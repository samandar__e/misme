from django.db.models import F

from components.models import Component
from projects.models import Projects
from restapp.models import TranslationTerm
from subproject.models import Subproject


class CustomReportManager:

    def get_app_models(self):
        return {
            "Component": {
                "app": "components",
                "model": "component",
                "fields": [
                    {"name": "component_name", "type": "string"},
                    {"name": "component_type", "type": "string"},
                    {"name": "component_variant", "type": "string"},
                    {"name": "component_order_number", "type": "number"},
                    {"name": "component_loan_grant_name", "type": "string"},
                    {"name": "component_responsible", "type": "string"},
                    {"name": "component_loan_grant_amount", "type": "number"},
                    {"name": "component_brief_description", "type": "string"},
                    {"name": "component_project", "type": "string"},
                    {"name": "component_sector", "type": "string"},
                    {"name": "component_field_of_activity", "type": "string"},
                    {"name": "component_pfi", "type": "string"},
                    {"name": "component_region", "type": "string"},
                    {"name": "component_district", "type": "string"},
                    {"name": "component_repayment_currency", "type": "string"},
                    {"name": "component_source_funding", "type": "number"},
                    {"name": "component_ifi_product", "type": "string"}
                ]
            },
            "Project": {
                "app": "projects",
                "model": "projects",
                "fields": [
                    {"name": "project_name", "type": "string"},
                    {"name": "project_projectNumber", "type": "string"},
                    {"name": "project_govResolution", "type": "string"},
                    {"name": "project_description", "type": "string"},
                    {"name": "project_overallProjectCost", "type": "number"},
                    {"name": "project_sdrValue", "type": "number"},
                    {"name": "project_currencyOne", "type": "number"},
                    {"name": "project_currencyTwo", "type": "number"},
                    {"name": "project_localGovernmentContribution", "type": "number"},
                    {"name": "project_beneficiaryFunds", "type": "number"},
                    {"name": "project_headOfPiu", "type": "string"},
                    {"name": "project_piuInTable", "type": "number"},
                    {"name": "project_piuEmployeesActual", "type": "number"},
                    {"name": "project_dateSigned", "type": "string"},
                    {"name": "project_dateClosing", "type": "string"},
                    {"name": "project_availableStudy", "type": "string"},
                    {"name": "project_overallCount", "type": "number"},
                    {"name": "project_menCount", "type": "number"},
                    {"name": "project_womenCount", "type": "number"},
                    {"name": "project_status", "type": "string"},
                    {"name": "project_ifi", "type": "string"},
                    {"name": "project_component", "type": "string"},
                    {"name": "project_region", "type": "string"},
                    {"name": "project_district", "type": "string"},
                    {"name": "project_pfi", "type": "string"}
                ]
            },
            "Subproject": {
                "app": "subproject",
                "model": "subproject",
                "fields": [
                    {"name": "subproject_name", "type": "string"},
                    {"name": "subproject_total_cost", "type": "number"},
                    {"name": "subproject_beneficiary_amount", "type": "number"},
                    {"name": "subproject_cofinanced_pfi", "type": "string"},
                    {"name": "subproject_loan_amount", "type": "number"},
                    {"name": "subproject_production_capacity", "type": "number"},
                    {"name": "subproject_start_activity", "type": "string"},
                    {"name": "subproject_start_financing", "type": "string"},
                    {"name": "subproject_status", "type": "string"},
                    {"name": "subproject_overall_jobs", "type": "number"},
                    {"name": "subproject_men", "type": "number"},
                    {"name": "subproject_women", "type": "number"},
                    {"name": "subproject_beneficiary", "type": "string"},
                    {"name": "subproject_component", "type": "string"},
                    {"name": "subproject_goods_imported_country", "type": "string"},
                    {"name": "subproject_region", "type": "string"},
                    {"name": "subproject_district", "type": "string"},
                    {"name": "subproject_production_capacity_unit", "type": "string"},
                    {"name": "subproject_project", "type": "string"},
                    {"name": "subproject_pfi", "type": "string"},
                    {"name": "subproject_sectors", "type": "string"},
                    {"name": "subproject_field_of_activities", "type": "string"},
                    {"name": "subproject_environment_requirements", "type": "string"}
                ]
            }
        }

    def get_fields_translations(self, model):
        models = self.get_app_models()
        item = models[model]
        fields = item.get('fields')

        terms = [field.get('name') for field in fields]
        translations = TranslationTerm.objects.filter(term_name__in=terms).values('term_name', 'name')

        field_translations = {}
        for field in fields:
            translation = next((item["name"] for item in translations if item["term_name"] == field.get("name")),
                               field.get("name"))
            field_translations[field.get("name")] = {"caption": translation, "type": field.get("type")}

        return field_translations

    def get_custom_report(self, model, start_date, end_date):
        class_method = getattr(self, model, self.subproject)
        data = class_method(start_date, end_date)
        return data

    def subproject(self, start_date, end_date):
        data = Subproject.objects\
            .values('id') \
            .annotate(
                subproject_name=F('name'),
                subproject_total_cost=F('total_cost'),
                subproject_beneficiary_amount=F('beneficiary_amount'),
                subproject_cofinanced_pfi=F('cofinanced_pfi'),
                subproject_loan_amount=F('loan_amount'),
                subproject_production_capacity=F('production_capacity'),
                subproject_start_activity=F('start_activity'),
                subproject_start_financing=F('start_financing'),
                subproject_status=F('status'),
                subproject_overall_jobs=F('overall_jobs'),
                subproject_men=F('men'),
                subproject_women=F('women'),
                subproject_beneficiary=F('beneficiary__name'),
                subproject_component=F('component__name'),
                subproject_goods_imported_country=F('goods_imported_countries__name'),
                subproject_region=F('region__name'),
                subproject_district=F('district__name'),
                subproject_production_capacity_unit=F('production_capacity_unit__name'),
                subproject_project=F('project__name'),
                subproject_pfi=F('pfi__name'),
                subproject_sectors=F('sectors__name'),
                subproject_field_of_activities=F('field_of_activities__name'),
                subproject_environment_requirements=F('environment_requirements__name'),
            )

        data = list(data)
        translated_fields = self.get_fields_translations("Subproject")
        data.insert(0, translated_fields)
        return data

    def component(self, start_date, end_date):
        data = Component.objects\
            .values('id')\
            .annotate(
                component_name=F('name'),
                component_type=F('type'),
                component_variant=F('variant'),
                component_order_number=F('order_number'),
                component_loan_grant_name=F('loan_grant_name'),
                component_responsible=F('responsible'),
                component_loan_grant_amount=F('loan_grant_amount'),
                component_brief_description=F('brief_description'),
                component_project=F('project__name'),
                component_sector=F('sectors__name'),
                component_field_of_activity=F('foas__name'),
                component_pfi=F('pfis__name'),
                component_region=F('regions__name'),
                component_district=F('districts__name'),
                component_repayment_currency=F('repayment_currency__name'),
                component_source_funding=F('source_fundings__name'),
                component_ifi_product=F('ifi_products__name'),
            )

        data = list(data)
        translated_fields = self.get_fields_translations("Component")
        data.insert(0, translated_fields)
        return data

    def project(self, start_date, end_date):
        data = Projects.objects \
            .values('id')\
            .annotate(
                project_name=F('name'),
                project_projectNumber=F('projectNumber'),
                project_govResolution=F('govResolution'),
                project_description=F('description'),
                project_overallProjectCost=F('overallProjectCost'),
                project_sdrValue=F('sdrValue'),
                project_currencyOne=F('currencyOne'),
                project_currencyTwo=F('currencyTwo'),
                project_localGovernmentContribution=F('localGovernmentContribution'),
                project_beneficiaryFunds=F('beneficiaryFunds'),
                project_headOfPiu=F('headOfPiu'),
                project_piuInTable=F('piuInTable'),
                project_piuEmployeesActual=F('piuEmployeesActual'),
                project_dateSigned=F('dateSigned'),
                project_dateClosing=F('dateClosing'),
                project_availableStudy=F('availableStudy'),
                project_overallCount=F('overallCount'),
                project_menCount=F('menCount'),
                project_womenCount=F('womenCount'),
                project_status=F('status'),
                project_ifi=F('ifi__name'),
                project_component=F('component__name'),
                project_region=F('regions__name'),
                project_district=F('districts__name'),
                project_pfi=F('pfis__name'),
            )

        data = list(data)
        translated_fields = self.get_fields_translations("Project")
        data.insert(0, translated_fields)
        return data
