from components.models import Component
from directory.models import Sector
from projects.models import Projects
from subproject.models import Subproject

from collections import Counter


class ReportMap:

    @staticmethod
    def get_overall_stats(year, region=None, district=None):
        # overall
        subprojects = Subproject.objects.get_by_region_overall_by_year(year, region, district)
        projects = Projects.objects.get_by_region_overall_by_year(year, region, district)
        components = Component.objects.get_by_region_overall_by_year(year, region, district)

        # leading_sectors
        leading_sector = {"sector": None, "subprojects_count": 0, "components_count": 0, "projects_count": 0}

        subproject_sectors = list(subprojects.filter(sectors__isnull=False).values_list("sectors", flat=True))
        components_sectors = list(components.filter(sectors__isnull=False).values_list("sectors", flat=True))
        sectors = subproject_sectors + components_sectors

        counter = Counter(sectors)
        if counter:
            max_id = counter.most_common(1)[0][0]
            sector = Sector.objects.get(pk=max_id)
            sector_subprojects = sector.subproject_set.all()
            sector_projects = [sp.project for sp in sector_subprojects]

            leading_sector["sector"] = sector.name
            leading_sector["subprojects_count"] = len(sector_subprojects)
            leading_sector["components_count"] = len(sector.component_set.all())
            leading_sector["projects_count"] = len(sector_projects)

        # Average production Indicators (Production value, Export value, New jobs)
        overall_jobs = sum(filter(None, subprojects.values_list("overall_jobs", flat=True)))
        subprojects_with_kpi = subprojects.filter(kpis__isnull=False)
        export_value_sum = 0.0
        production_value_sum = 0.0
        for sp in subprojects_with_kpi:
            kpis = sp.kpis.all()
            export_value_sum = export_value_sum + sum(kpis.values_list("export_value", flat=True))
            production_value_sum = production_value_sum + sum(kpis.values_list("production_value", flat=True))

        return {
            "overall": {
                "subprojects": len(subprojects),
                "projects": len(projects),
                "component": len(components)
            },
            "leading_sector": leading_sector,
            "average": {
                "production_value": production_value_sum,
                "export_value": export_value_sum,
                "new_jobs": overall_jobs
            }
        }

    @staticmethod
    def get_leading_sectors():
        subproject_sectors = Subproject.objects.all().values_list("sectors")
        return subproject_sectors

    def get_average_production_indicators(self):
        pass
