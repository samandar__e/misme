from django.urls import re_path, path

from reports.views.consolidated_project_forecasts import ConsolidatedProjectForecastsList
from reports.views.custom_report import CustomReportView, CustomReportDetailView, CustomReportData
from reports.views.map_report import MapOverallReport
from reports.views.network_schedule import NetworkScheduleList
from reports.views.report_to_arr import ReportToArrList
from reports.views.single_project_implementation import SingleProjectImplementationList
from reports.views.subproject_implementation_monitoring import SIMReportList
from reports.views.subproject_loan_repayments_monitoring import SLRMReportList

urlpatterns = [
    re_path(r'^network-schedule/$', NetworkScheduleList.as_view(), name='network_schedule_view'),
    re_path(r'^consolidated-project-forecasts/$', ConsolidatedProjectForecastsList.as_view(), name='consolidated_project_forecasts_view'),
    re_path(r'^report-to-arr/$', ReportToArrList.as_view(), name='report_to_arr_view'),
    re_path(r'^single-project-implementation/$', SingleProjectImplementationList.as_view(), name='single_project_implementation_view'),
    re_path(r'^subproject-implementation-monitoring/$', SIMReportList.as_view(), name='subproject_implementation_monitoring_view'),
    re_path(r'^subproject-loan-repayments-monitoring/$', SLRMReportList.as_view(), name='subproject_loan_repayments_monitoring_view'),

    re_path(r'^custom-report/$', CustomReportView.as_view(), name='custom_report'),
    path('custom-report/<int:pk>', CustomReportDetailView.as_view(), name='custom_report_detail'),

    re_path(r'^custom-report-data/$', CustomReportData.as_view(), name='custom_report_data'),
    re_path(r'^map-report/overall/$', MapOverallReport.as_view(), name='map_report_overall')
]