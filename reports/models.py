from restapp.models import BaseModel
from django.db import models
from django.utils.translation import ugettext_lazy as _


class CustomReport(BaseModel):
    name = models.CharField(_('Report name'), max_length=255, blank=False)
    start_date = models.DateField(_('Start interval'), null=True, blank=True)
    end_date = models.DateField(_('End interval'), null=True, blank=True)
    report_settings = models.TextField(_('Report settings'), null=True, blank=True)
    module = models.CharField(_('Module'), max_length=255, blank=True)

    def __str__(self):
        return self.name
