from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from components.models import Component, ProcurementPlan
from projects.models import ProjectImplementation
from subproject.models import Subproject


class ProcurementPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcurementPlan
        fields = ('id', 'component')


class SubprojectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subproject
        fields = ('id', 'name', 'total_cost', 'start_financing')


class ComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'project', 'type', 'variant', 'parent', 'order_number', 'loan_grant_amount')


class ReportsProjectImplementationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImplementation
        fields = ('id', 'project', 'date', 'activitiesImplemented', 'problems')


class SingleProjectImplementationSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()
    general_expenses = serializers.SerializerMethodField()
    remainder = serializers.SerializerMethodField()
    forecast_for_the_year = serializers.SerializerMethodField()
    forecast = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    percent = serializers.SerializerMethodField()
    component_parent = RecursiveField(many=True, read_only=True)

    class Meta:
        model = Component
        fields = ('id', 'parent', 'name', 'variant', 'total_amount', 'general_expenses', 'remainder', 'forecast_for_the_year',
                  'forecast', 'fact', 'percent', 'component_parent')

    def get_total_amount(self, blog_post):
        total_amount = 0
        return total_amount

    def get_general_expenses(self, blog_post):
        general_expenses = 0
        return general_expenses

    def get_remainder(self, blog_post):
        remainder = 0
        return remainder

    def get_forecast_for_the_year(self, blog_post):
        forecast_for_the_year = 0
        return forecast_for_the_year

    def get_forecast(self, blog_post):
        forecast = 0
        return forecast

    def get_fact(self, blog_post):
        fact = 0
        return fact

    def get_percent(self, blog_post):
        percent = 0
        return percent


