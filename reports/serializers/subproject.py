from rest_framework import serializers

from crm.models import ParticipatingFinancialInstitution
from directory.serializers import UOMSerializer, SectorSerializer, CountrySerializer, RelatedFOASerializer
from projects.models import Projects
from subproject.models import Subproject
from subproject.serializers import TotalCostField, LoanAmountField, BeneficiaryAmountField, CofinancedPfiField


class PFIDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = ('id', 'name')


class ProjectDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'name')


class SubprojectReportSerializer(serializers.ModelSerializer):
    pfi_detail = PFIDetailSerializer(read_only=True)
    project_detail = ProjectDetailSerializer(read_only=True)
    total_cost = TotalCostField(source='*', required=False)
    loan_amount = LoanAmountField(source='*', required=False)
    beneficiary_amount = BeneficiaryAmountField(source='*', required=False)
    cofinanced_pfi = CofinancedPfiField(source='*', required=False)
    sectors_detail = SectorSerializer(read_only=True, many=True)
    field_of_activities_detail = RelatedFOASerializer(read_only=True)
    production_capacity_unit_detail = UOMSerializer(read_only=True)
    goods_imported_country_detail = CountrySerializer(read_only=True)

    class Meta:
        model = Subproject
        fields = ('id', 'name', 'status', 'beneficiary', 'total_cost', 'loan_amount', 'beneficiary_amount',
                  'cofinanced_pfi', 'production_capacity', 'production_capacity_unit', 'production_capacity_unit_detail',
                  'start_activity', 'start_financing', 'sectors', 'sectors_detail',
                  'field_of_activities', 'field_of_activities_detail',
                  'environment_requirements', 'pfi', 'pfi_detail',
                  'goods_imported_country', 'goods_imported_country_detail',
                  'region', 'district', 'overall_jobs',
                  'men', 'women', 'project',
                  'project_detail', 'revolver_means', 'component')
