from rest_framework import serializers
from projects.models import Projects


class ConsolidatedProjectForecastsSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()  # Общая сумма  проектов
    general_expenses = serializers.SerializerMethodField()  # Обшие расходы
    remainder = serializers.SerializerMethodField()  # Остаток на 01.01.2020г.
    forecast = serializers.SerializerMethodField()  # Прогноз привлечения на 2020г.
    development_forecast = serializers.SerializerMethodField()  # Прогноз освоения на 2020г.
    balance = serializers.SerializerMethodField()  # Остаток средств на 01.05.2020г
    fact = serializers.SerializerMethodField()  # Факт январь-апрель
    january = serializers.SerializerMethodField()
    february = serializers.SerializerMethodField()
    march = serializers.SerializerMethodField()
    april = serializers.SerializerMethodField()
    may = serializers.SerializerMethodField()
    june = serializers.SerializerMethodField()
    july = serializers.SerializerMethodField()
    august = serializers.SerializerMethodField()
    september = serializers.SerializerMethodField()
    october = serializers.SerializerMethodField()
    november = serializers.SerializerMethodField()
    december = serializers.SerializerMethodField()
    expected_balance = serializers.SerializerMethodField()  # Ожидаемый остаток

    class Meta:
        model = Projects
        fields = ('id', 'name', 'total_amount', 'general_expenses', 'remainder', 'forecast',
                  'development_forecast', 'balance', 'fact', 'january', 'february', 'march', 'april', 'may', 'june',
                  'july', 'august', 'september', 'october', 'november', 'december', 'expected_balance')

    def get_total_amount(self, blog_post):
        total_amount = 0
        return total_amount

    def get_general_expenses(self, blog_post):
        general_expenses = 0
        return general_expenses

    def get_remainder(self, blog_post):
        remainder = 0
        return remainder

    def get_forecast(self, blog_post):
        forecast = 0
        return forecast

    def get_development_forecast(self, blog_post):
        development_forecast = 0
        return development_forecast

    def get_balance(self, blog_post):
        balance = 0
        return balance

    def get_fact(self, blog_post):
        fact = 0
        return fact

    def get_january(self, blog_post):
        january = 0
        return january

    def get_february(self, blog_post):
        february = 0
        return february

    def get_march(self, blog_post):
        march = 0
        return march

    def get_april(self, blog_post):
        april = 0
        return april

    def get_may(self, blog_post):
        may = 0
        return may

    def get_june(self, blog_post):
        june = 0
        return june

    def get_july(self, blog_post):
        july = 0
        return july

    def get_august(self, blog_post):
        august = 0
        return august

    def get_september(self, blog_post):
        september = 0
        return september

    def get_october(self, blog_post):
        october = 0
        return october

    def get_november(self, blog_post):
        november = 0
        return november

    def get_december(self, blog_post):
        december = 0
        return december

    def get_expected_balance(self, blog_post):
        expected_balance = 0
        return expected_balance
