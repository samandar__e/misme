from rest_framework import serializers

from crm.models import Beneficiary, ParticipatingFinancialInstitution
from directory.models import Region
from projects.models import Projects
from subproject.models import Subproject, Repayment


class ParticipatingFinancialInstitutionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = ('name',)


class BeneficiarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Beneficiary
        fields = ('name', 'taxId')


class RelatedPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('name',)


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'name',)


class SLRMReportSerializer(serializers.ModelSerializer):
    region = RelatedPositionSerializer()
    beneficiary = BeneficiarySerializer()
    project = ProjectSerializer()
    pfi = ParticipatingFinancialInstitutionSerializer()

    principal_paid = serializers.SerializerMethodField()
    principal_overdue_less_then30 = serializers.SerializerMethodField()
    principal_overdue_from30to90 = serializers.SerializerMethodField()
    principal_overdue_more_then90 = serializers.SerializerMethodField()
    total_principal = serializers.SerializerMethodField()
    interest_paid = serializers.SerializerMethodField()
    interest_overdue_less_then30 = serializers.SerializerMethodField()
    interest_overdue_from30to90 = serializers.SerializerMethodField()
    interest_overdue_more_then90 = serializers.SerializerMethodField()
    total_interest = serializers.SerializerMethodField()
    current_balance = serializers.SerializerMethodField()

    class Meta:
        model = Subproject
        fields = ('id', 'pfi', 'region', 'project', 'beneficiary', 'loan_amount', 'principal_paid',
                  'principal_overdue_less_then30', 'principal_overdue_from30to90', 'principal_overdue_more_then90',
                  'total_principal', 'interest_paid', 'interest_overdue_less_then30',
                  'interest_overdue_from30to90', 'interest_overdue_more_then90', 'total_interest', 'current_balance')

    def get_principal_paid(self, blog_post):
        principal_paid = 0
        return principal_paid

    def get_principal_overdue_less_then30(self, blog_post):
        principal_overdue_less_then30 = 0
        return principal_overdue_less_then30

    def get_principal_overdue_from30to90(self, blog_post):
        principal_overdue_from30to90 = 0
        return principal_overdue_from30to90

    def get_principal_overdue_more_then90(self, blog_post):
        principal_overdue_more_then90 = 0
        return principal_overdue_more_then90

    def get_total_principal(self, blog_post):
        total_principal = 0
        return total_principal

    def get_interest_paid(self, blog_post):
        interest_paid = 0
        return interest_paid

    def get_interest_overdue_less_then30(self, blog_post):
        interest_overdue_less_then30 = 0
        return interest_overdue_less_then30

    def get_interest_overdue_from30to90(self, blog_post):
        interest_overdue_from30to90 = 0
        return interest_overdue_from30to90

    def get_interest_overdue_more_then90(self, blog_post):
        interest_overdue_more_then90 = 0
        return interest_overdue_more_then90

    def get_total_interest(self, blog_post):
        total_interest = 0
        return total_interest

    def get_current_balance(self, blog_post):
        current_balance = 0
        return current_balance


class RepaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Repayment
        fields = ('id', 'principal_paid', 'principal_overdue_less_then30', 'principal_overdue_from30to90',
                  'principal_overdue_more_then90', 'interest_paid', 'interest_overdue_less_then30',
                  'interest_overdue_from30to90', 'interest_overdue_more_then90')

