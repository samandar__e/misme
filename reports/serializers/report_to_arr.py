from rest_framework import serializers

from crm.models import InternationalFinancialInstitute
from projects.models import Projects, LoanFacility


class NamedIFISerializer(serializers.ModelSerializer):
    class Meta:
        model = InternationalFinancialInstitute
        fields = ('id', 'name')


class LoanFacilitySerializer(serializers.ModelSerializer):
    ifi = NamedIFISerializer()

    class Meta:
        model = LoanFacility
        fields = ('id', 'project', 'ifi')


class ProjectsReportSerializer(serializers.ModelSerializer):
    ifi = serializers.SerializerMethodField()

    class Meta:
        model = Projects
        fields = ('id', 'name', 'ifi', 'implementationStartDate', 'implementationEndDate', 'overallProjectCost')

    def get_ifi(self, blog_post):
        ifi = ''
        return ifi
