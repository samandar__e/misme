from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from components.models import Component, ProcurementPlan
from subproject.models import Subproject


class ProcurementPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcurementPlan
        fields = ('id', 'component')


class SubprojectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subproject
        fields = ('id', 'name', 'total_cost', 'start_financing')


class ComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'project', 'type', 'variant', 'parent', 'order_number', 'loan_grant_amount')


class NetworkScheduleSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()
    general_expenses = serializers.SerializerMethodField()
    remainder = serializers.SerializerMethodField()
    forecast = serializers.SerializerMethodField()
    development_forecast = serializers.SerializerMethodField()
    fact = serializers.SerializerMethodField()
    january = serializers.SerializerMethodField()
    february = serializers.SerializerMethodField()
    march = serializers.SerializerMethodField()
    april = serializers.SerializerMethodField()
    may = serializers.SerializerMethodField()
    june = serializers.SerializerMethodField()
    july = serializers.SerializerMethodField()
    august = serializers.SerializerMethodField()
    september = serializers.SerializerMethodField()
    october = serializers.SerializerMethodField()
    november = serializers.SerializerMethodField()
    december = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    year_to_year = serializers.SerializerMethodField()
    component_parent = RecursiveField(many=True, read_only=True)

    class Meta:
        model = Component
        fields = ('id', 'project', 'parent', 'name', 'variant', 'total_amount', 'general_expenses', 'remainder', 'forecast',
                  'development_forecast', 'fact', 'january', 'february', 'march', 'april', 'may', 'june', 'july',
                  'august', 'september', 'october', 'november', 'december', 'year', 'year_to_year', 'component_parent')

    def get_total_amount(self, blog_post):
        total_amount = 0
        return total_amount

    def get_general_expenses(self, blog_post):
        general_expenses = 0
        return general_expenses

    def get_remainder(self, blog_post):
        remainder = 0
        return remainder

    def get_forecast(self, blog_post):
        forecast = 0
        return forecast

    def get_development_forecast(self, blog_post):
        development_forecast = 0
        return development_forecast

    def get_fact(self, blog_post):
        fact = 0
        return fact

    def get_january(self, blog_post):
        january = 0
        return january

    def get_february(self, blog_post):
        february = 0
        return february

    def get_march(self, blog_post):
        march = 0
        return march

    def get_april(self, blog_post):
        april = 0
        return april

    def get_may(self, blog_post):
        may = 0
        return may

    def get_june(self, blog_post):
        june = 0
        return june

    def get_july(self, blog_post):
        july = 0
        return july

    def get_august(self, blog_post):
        august = 0
        return august

    def get_september(self, blog_post):
        september = 0
        return september

    def get_october(self, blog_post):
        october = 0
        return october

    def get_november(self, blog_post):
        november = 0
        return november

    def get_december(self, blog_post):
        december = 0
        return december

    def get_year(self, blog_post):
        year = 0
        return year

    def get_year_to_year(self, blog_post):
        year_to_year = 0
        return year_to_year
