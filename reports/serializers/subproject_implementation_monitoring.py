from rest_framework import serializers

from crm.models import Beneficiary, ParticipatingFinancialInstitution
from directory.models import Region, FOA
from subproject.models import Subproject, SubprojectKPI


class ParticipatingFinancialInstitutionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = ('name',)


class FOASerializer(serializers.ModelSerializer):
    class Meta:
        model = FOA
        fields = ('name',)


class BeneficiarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Beneficiary
        fields = ('name', 'taxId')


class RelatedPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('name',)


class SIMReportSerializer(serializers.ModelSerializer):
    region = RelatedPositionSerializer()
    beneficiary = BeneficiarySerializer()
    field_of_activities = FOASerializer()
    pfi = ParticipatingFinancialInstitutionSerializer()
    note = serializers.SerializerMethodField()
    total = serializers.SerializerMethodField()
    production_value_sum = serializers.SerializerMethodField()
    production_quantity_sum = serializers.SerializerMethodField()

    class Meta:
        model = Subproject
        fields = ('id', 'region', 'beneficiary', 'field_of_activities', 'pfi', 'loan_amount',
                  'start_financing', 'start_activity', 'total', 'men', 'women', 'production_quantity_sum',
                  'production_value_sum', 'note')

    def get_total(self, blog_post):
        total = 0
        return total

    def get_note(self, blog_post):
        note = ""
        return note

    def get_production_value_sum(self, blog_post):
        production_value_sum = 0
        return production_value_sum

    def get_production_quantity_sum(self, blog_post):
        production_quantity_sum = ""
        return production_quantity_sum


class SubprojectKPISerializer(serializers.ModelSerializer):

    class Meta:
        model = SubprojectKPI
        fields = ('id', 'production_quantity', 'production_value')

