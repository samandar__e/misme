from rest_framework import serializers

from components.models import Component
from projects.models import Projects
from reports.models import CustomReport
from subproject.models import Subproject


class CustomReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomReport
        fields = ('id', 'name', 'start_date', 'end_date', 'report_settings', 'module')


class ComponentCustomReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Component


class ProjectCustomReportSerializer(serializers. ModelSerializer):

    class Meta:
        model = Projects


class SubprojectCustomReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subproject
        fields = ('id', 'name', 'status', 'beneficiary', 'total_cost', 'loan_amount', 'beneficiary_amount',
                  'cofinanced_pfi', 'production_capacity', 'uom', 'start_activity', 'start_financing', 'sectors',
                  'field_of_activities', 'environment_requirements', 'pfi', 'country', 'region', 'district', 'overall',
                  'men', 'women', 'project', 'revolver_means', 'component')
