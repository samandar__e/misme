from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import datetime

from components.models import Component, PmCosts, Contracts, ProcurementPlan, ComponentImplementation
from components.serializers import PmCostsSerializer, ContractsSerializer, ComponentImplementationSerializer
from reports.serializers.network_schedule import NetworkScheduleSerializer, ComponentSerializer, \
    ProcurementPlanSerializer, SubprojectSerializer
from subproject.models import Subproject


class NetworkScheduleList(ListAPIView):
    http_method_names = ['get']

    # Общая сумма проекта
    def total_amount_component(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            components = Component.objects.all().filter(id=component['id'])
            serializer = ComponentSerializer(components, many=True)
            components_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(components_dict) != null:
                for j in range(0, len(components_dict)):
                    # date_cr = datetime.strptime(components_dict[j]['date_cr'], '%Y-%m-%d').year
                    # if date_cr == date_year:
                    if components_dict[j]['loan_grant_amount'] is not None:
                        sum_value = sum_value + int(components_dict[j]['loan_grant_amount'])
                        component['total_amount'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.total_amount_component(component, summ, date_year)
        return summ

    # Сумма в следующем году
    def next_year_amount(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            implementations = ComponentImplementation.objects.all().filter(component=component['id'])
            serializer = ComponentImplementationSerializer(implementations, many=True)
            implementations_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(implementations_dict) != null:
                for j in range(0, len(implementations_dict)):
                    date_str = implementations_dict[j]['month_year']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year+1:
                        if implementations_dict[j]['forecast_value'] is not None:
                            sum_value = sum_value + int(implementations_dict[j]['forecast_value'])
                            component['year'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.next_year_amount(component, summ, date_year)
        return summ

    # remainder - development_forecast - year
    def remainder_development_year(self, component, summ, date_year):
        if len(component['component_parent']) == 0:
            remainder_development = int(component['remainder']) - float(component['development_forecast']) - float(component['year'])
            component['year_to_year'] = remainder_development
            summ.append(remainder_development)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.remainder_development_year(component, summ, date_year)
        return summ

    # Остаток
    def remainder(self, component, summ, date_year):
        if len(component['component_parent']) == 0:
            remainder = int(component['total_amount']) - float(component['general_expenses'])
            component['remainder'] = remainder
            summ.append(remainder)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.remainder(component, summ, date_year)
        return summ

    # forecast and development_forecast
    def sum_forecast_and_development_forecast(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            implementations = ComponentImplementation.objects.all().filter(component=component['id'])
            serializer = ComponentImplementationSerializer(implementations, many=True)
            implementations_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(implementations_dict) != null:
                for j in range(0, len(implementations_dict)):
                    date_str = implementations_dict[j]['month_year']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if implementations_dict[j]['forecast_value'] is not None:
                            sum_value = sum_value + int(implementations_dict[j]['forecast_value'])
                            component['forecast'] = sum_value
                            component['development_forecast'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.sum_forecast_and_development_forecast(component, summ, date_year)
        return summ

    # Months and Fact
    def months_fact(self, component, date_year, number, sum_january, sum_february, sum_march, sum_april, sum_may,
                    sum_june, sum_july, sum_august, sum_september, sum_october, sum_november, sum_december, sum_fact, null=None):
        if len(component['component_parent']) == 0:
            if number == 1:
                procurement_plan = ProcurementPlan.objects.all().filter(component=component['id'])
                serializer = ProcurementPlanSerializer(procurement_plan, many=True)
                procurement_plan_dict = json.loads(json.dumps(serializer.data))
                data_dict = []
                for i in range(0, len(procurement_plan_dict)):
                    contracts = Contracts.objects.all().filter(procurement_plan=procurement_plan_dict[i]['id'],
                                                               status='PUBLISHED')
                    serializer = ContractsSerializer(contracts, many=True)
                    data_dict = json.loads(json.dumps(serializer.data))
            elif number == 2:
                subprojects = Subproject.objects.all().filter(component=component['id'])
                serializer = SubprojectSerializer(subprojects, many=True)
                data_dict = json.loads(json.dumps(serializer.data))
            elif number == 3:
                pm_costs = PmCosts.objects.all().filter(component=component['id'])
                serializer_pm = PmCostsSerializer(pm_costs, many=True)
                data_dict = json.loads(json.dumps(serializer_pm.data))

            if len(data_dict) != 0:
                for j in range(0, len(data_dict)):
                    if number == 1:
                        date_str = data_dict[j]['date_of_signature']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        amount_sum = float(0 if data_dict[j]['amount_in_usd'] is None else data_dict[j]['amount_in_usd'])
                    elif number == 2:
                        date_str = data_dict[j]['start_financing']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        amount_sum = float(0 if data_dict[j]['total_cost'] is None else data_dict[j]['total_cost'])
                    elif number == 3:
                        date_str = data_dict[j]['date_cr']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        amount_sum = float(data_dict[j]['amount_in_usd'])

                    if date_cr == date_year:
                        if date_month == 1:
                            component['january'] = component['january'] + amount_sum
                        if date_month == 2:
                            component['february'] = component['february'] + amount_sum
                        if date_month == 3:
                            component['march'] = component['march'] + amount_sum
                        if date_month == 4:
                            component['april'] = component['april'] + amount_sum
                        if date_month == 5:
                            component['may'] = component['may'] + amount_sum
                        if date_month == 6:
                            component['june'] = component['june'] + amount_sum
                        if date_month == 7:
                            component['july'] = component['july'] + amount_sum
                        if date_month == 8:
                            component['august'] = component['august'] + amount_sum
                        if date_month == 9:
                            component['september'] = component['september'] + amount_sum
                        if date_month == 10:
                            component['october'] = component['october'] + amount_sum
                        if date_month == 11:
                            component['november'] = component['november'] + amount_sum
                        if date_month == 12:
                            component['december'] = component['december'] + amount_sum
                        component['fact'] = component['january'] + component['february'] + component['march'] +component['april'] + \
                            component['may'] + component['june'] + component['july'] + component['august'] + \
                            component['september'] + component['october'] + component['november'] + component['december']

                        if component['parent'] == null:
                            sum_january.append(component['january']), sum_february.append(component['february'])
                            sum_march.append(component['march']), sum_april.append(component['april'])
                            sum_may.append(component['may']), sum_june.append(component['june'])
                            sum_july.append(component['july']), sum_august.append(component['august'])
                            sum_september.append(component['september']), sum_october.append(component['october'])
                            sum_november.append(component['november']), sum_december.append(component['december'])
                            sum_fact_all = component['january'] + component['february'] + component['march'] + \
                                component['april'] + component['may'] + component['june'] + component['july'] +\
                                component['august'] + component['september'] + component['october'] + \
                                component['november'] + component['december']
                            sum_fact.append(sum_fact_all)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.months_fact(component, date_year, number, sum_january, sum_february, sum_march, sum_april, sum_may,
                     sum_june, sum_july, sum_august, sum_september, sum_october, sum_november, sum_december, sum_fact)
                sum_january.append(component['january']), sum_february.append(component['february'])
                sum_march.append(component['march']), sum_april.append(component['april'])
                sum_may.append(component['may']), sum_june.append(component['june'])
                sum_july.append(component['july']), sum_august.append(component['august'])
                sum_september.append(component['september']), sum_october.append(component['october'])
                sum_november.append(component['november']), sum_december.append(component['december'])
                sum_fact_all = component['january'] + component['february'] + component['march'] + component['april'] + \
                    component['may'] + component['june'] + component['july'] + component['august'] + \
                    component['september'] + component['october'] + component['november'] + component['december']
                sum_fact.append(sum_fact_all)
        return sum_january, sum_february, sum_march, sum_april, sum_may, sum_june, sum_july, sum_august, sum_september, \
            sum_october, sum_november, sum_december, sum_fact

    def sum_months_and_fact(self, component, date_year, number, null=None):
        # ------------------ Months and Fact --------------------
        sum_january, sum_february, sum_march, sum_april, sum_may, sum_june, sum_july, sum_august, sum_september, \
            sum_october, sum_november, sum_december, sum_fact = [], [], [], [], [], [], [], [], [], [], [], [], []

        self.months_fact(component, date_year, number, sum_january, sum_february, sum_march, sum_april, sum_may,
                         sum_june, sum_july, sum_august, sum_september, sum_october, sum_november, sum_december, sum_fact)
        component['january'] = sum_january[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_january)
        component['february'] = sum_february[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_february)
        component['march'] = sum_march[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_march)
        component['april'] = sum_april[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_april)
        component['may'] = sum_may[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_may)
        component['june'] = sum_june[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_june)
        component['july'] = sum_july[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_july)
        component['august'] = sum_august[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_august)
        component['september'] = sum_september[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_september)
        component['october'] = sum_october[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_october)
        component['november'] = sum_november[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_november)
        component['december'] = sum_december[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_december)
        component['fact'] = sum_fact[-1] if component['parent'] == null and component['component_parent'] == null else sum(sum_fact)
        return component

    # ========================================== Variant Project management ============================================
    # Variant: Project management Обшие расходы
    def general_expenses_pm_cost(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            pm_costs = PmCosts.objects.all().filter(id=component['id'])
            serializer = PmCostsSerializer(pm_costs, many=True)
            pm_costs_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(pm_costs_dict) != null:
                for j in range(0, len(pm_costs_dict)):
                    date_str = pm_costs_dict[j]['date_cr']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if pm_costs_dict[j]['amount_in_usd'] is not None:
                            sum_value = sum_value + float(pm_costs_dict[j]['amount_in_usd'])
                            component['general_expenses'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_pm_cost(component, summ, date_year)
        return summ

    # Variant: Project management
    def variant_project_management(self, component, date_year):
        # ------------------- Общая сумма проекта ---------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  -----------------------
        summ_general = []
        sum_list_general = self.general_expenses_pm_cost(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ---------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------------
        number = 3
        self.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        # -------------------------- 2021г. ---------------------------
        summ_next_year = []
        sum_list_next_year = self.next_year_amount(component, summ_next_year, date_year)
        component['year'] = sum(sum_list_next_year)
        # ------------remainder - development_forecast - year----------
        summ_year_to_year= []
        sum_list_year_to_year = self.remainder_development_year(component, summ_year_to_year, date_year)
        component['year_to_year'] = sum(sum_list_year_to_year)
        return component

    # ============================================= Variant Procurement ================================================
    # Variant Procurement Обшие расходы
    def general_expenses_contract(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            procurement_plan = ProcurementPlan.objects.all().filter(component=component['id'])
            serializer = ProcurementPlanSerializer(procurement_plan, many=True)
            procurement_plan_dict = json.loads(json.dumps(serializer.data))
            for i in range(0, len(procurement_plan_dict)):
                contracts = Contracts.objects.all().filter(procurement_plan=procurement_plan_dict[i]['id'])
                serializer = ContractsSerializer(contracts, many=True)
                contracts_dict = json.loads(json.dumps(serializer.data))
                sum_value = 0
                if len(contracts_dict) != null:
                    for j in range(0, len(contracts_dict)):
                        date_str = contracts_dict[j]['date_of_signature']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                        if date_cr == date_year:
                            if contracts_dict[j]['amount_in_usd'] is not None:
                                sum_value = sum_value + float(contracts_dict[j]['amount_in_usd'])
                                component['general_expenses'] = sum_value
                summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_contract(component, summ, date_year)
        return summ

    # Variant: Procurement
    def variant_procurement(self, component, date_year):
        # ------------------- Общая сумма проекта -----------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ----------------------- Обшие расходы -------------------------
        summ_general = []
        sum_list_general = self.general_expenses_contract(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # -------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 1
        self.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        # -------------------------- 2021г. ---------------------------
        summ_next_year = []
        sum_list_next_year = self.next_year_amount(component, summ_next_year, date_year)
        component['year'] = sum(sum_list_next_year)
        # ------------remainder - development_forecast - year----------
        summ_year_to_year = []
        sum_list_year_to_year = self.remainder_development_year(component, summ_year_to_year, date_year)
        component['year_to_year'] = sum(sum_list_year_to_year)
        return component

    # ============================================== Access to loans ===================================================
    # Access to loans Обшие расходы
    def general_expenses_subproject(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            subprojects = Subproject.objects.all().filter(component=component['id'])
            serializer = SubprojectSerializer(subprojects, many=True)
            subprojects_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(subprojects_dict) != null:
                for j in range(0, len(subprojects_dict)):
                    date_str = subprojects_dict[j]['start_financing']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if subprojects_dict[j]['total_cost'] is not None:
                            sum_value = sum_value + float(subprojects_dict[j]['total_cost'])
                            component['general_expenses'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_subproject(component, summ, date_year)
        return summ

    # Variant: Access to loans
    def variant_access_to_loans(self, component, date_year):
        # ------------------- Общая сумма  проекта --------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  ------------------------
        summ_general = []
        sum_list_general = self.general_expenses_subproject(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 2
        self.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        # -------------------------- 2021г. ---------------------------
        summ_next_year = []
        sum_list_next_year = self.next_year_amount(component, summ_next_year, date_year)
        component['year'] = sum(sum_list_next_year)
        # ------------remainder - development_forecast - year----------
        summ_year_to_year = []
        sum_list_year_to_year = self.remainder_development_year(component, summ_year_to_year, date_year)
        component['year_to_year'] = sum(sum_list_year_to_year)
        return component

    def get(self, request, include_self=True):
        date_comp = request.GET['date']
        date_year = datetime.strptime(date_comp, '%Y-%m-%d').year
        queryset = Component.objects.all().filter(project=request.GET['project'], parent=None).order_by('id')
        serializer = NetworkScheduleSerializer(queryset, many=True)
        component_dict = json.loads(json.dumps(serializer.data))

        for i in range(0, len(component_dict)):
            if component_dict[i]['variant'] == 'PR':
                component = component_dict[i]
                self.variant_procurement(component, date_year)
            elif component_dict[i]['variant'] == 'AL':
                component = component_dict[i]
                self.variant_access_to_loans(component, date_year)
            elif component_dict[i]['variant'] == 'PM':
                component = component_dict[i]
                self.variant_project_management(component, date_year)
        return Response(component_dict)
