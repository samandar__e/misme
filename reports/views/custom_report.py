from datetime import date

from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from reports.managers.report_manager import CustomReportManager
from reports.models import CustomReport
from reports.serializers.custom_report import CustomReportSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class CustomReportView(ListCreateAPIView):
    serializer_class = CustomReportSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name',)
    ordering = ['pk']

    def get_queryset(self):
        return CustomReport.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class CustomReportDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = CustomReportSerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        report = get_object_or_404(CustomReport, id=pk)
        serializer = self.serializer_class(report)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        report = get_object_or_404(CustomReport, id=pk)
        serializer = self.serializer_class(report, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        report = get_object_or_404(CustomReport, id=pk)
        report.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class CustomReportData(APIView):

    def get(self, request):
        manager = CustomReportManager()
        start_date = request.query_params.get("start_date", date.today())
        end_date = request.query_params.get("end_date", date.today())
        model = request.query_params.get("model", "subproject")
        results = manager.get_custom_report(model, start_date, end_date)
        return Response(results, 200)
