from rest_framework.response import Response
from rest_framework.views import APIView

from reports.managers.report_map_manager import ReportMap


class MapOverallReport(APIView):

    def get(self, request):
        region = request.query_params.get("region")
        district = request.query_params.get("district")
        year = request.query_params.get("year")
        results = ReportMap.get_overall_stats(year, region, district)
        return Response(results, 200)
