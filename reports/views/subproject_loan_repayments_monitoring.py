from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import datetime

from reports.serializers.subproject_loan_repayments_monitoring import SLRMReportSerializer, RepaymentSerializer
from subproject.models import Subproject, Repayment


class SLRMReportList(ListAPIView):
    http_method_names = ['get']

    def get(self, request, include_self=True, null=None):
        date_sub = request.GET['date']
        pfi_id = request.GET['pfi']
        region_id = request.GET['region']
        date_year = datetime.strptime(date_sub, '%Y-%m-%d').year
        if region_id == "":
            queryset = Subproject.objects.all().filter(project=request.GET['project'], start_financing__year=date_year,
                                                       pfi=pfi_id).order_by('id')
        else:
            queryset = Subproject.objects.all().filter(project=request.GET['project'], start_financing__year=date_year,
                                                       pfi=pfi_id, region=region_id).order_by('id')
        serializer = SLRMReportSerializer(queryset, many=True)
        subproject_dict = json.loads(json.dumps(serializer.data))
        total_list = []
        total_loan_amount = 0
        total_current_balance = 0
        for i in range(0, len(subproject_dict)):
            if subproject_dict[i]['pfi'] is None:
                subproject_dict[i]['pfi'] = ""
            if subproject_dict[i]['beneficiary'] is None:
                subproject_dict[i]['beneficiary'] = ""
            if subproject_dict[i]['region'] is None:
                subproject_dict[i]['region'] = ""
            if subproject_dict[i]['loan_amount'] is None:
                subproject_dict[i]['loan_amount'] = 0

            queryset_repayment = Repayment.objects.all().filter(subproject=subproject_dict[i]['id']).order_by('id')
            serializer_repayment = RepaymentSerializer(queryset_repayment, many=True)
            repayment_dict = json.loads(json.dumps(serializer_repayment.data))

            principal_paid_sum, principal_overdue_less_then30_sum, principal_overdue_from30to90_sum, principal_overdue_more_then90_sum, \
                interest_paid_sum, interest_overdue_less_then30_sum, interest_overdue_from30to90_sum, interest_overdue_more_then90_sum = \
                [], [], [], [], [], [], [], []
            for j in range(0, len(repayment_dict)):
                if repayment_dict[j]['principal_paid'] is None:
                    repayment_dict[j]['principal_paid'] = 0
                if repayment_dict[j]['principal_overdue_less_then30'] is None:
                    repayment_dict[j]['principal_overdue_less_then30'] = 0
                if repayment_dict[j]['principal_overdue_from30to90'] is None:
                    repayment_dict[j]['principal_overdue_from30to90'] = 0
                if repayment_dict[j]['principal_overdue_more_then90'] is None:
                    repayment_dict[j]['principal_overdue_more_then90'] = 0
                if repayment_dict[j]['interest_paid'] is None:
                    repayment_dict[j]['interest_paid'] = 0
                if repayment_dict[j]['interest_overdue_less_then30'] is None:
                    repayment_dict[j]['interest_overdue_less_then30'] = 0
                if repayment_dict[j]['interest_overdue_from30to90'] is None:
                    repayment_dict[j]['interest_overdue_from30to90'] = 0
                if repayment_dict[j]['interest_overdue_more_then90'] is None:
                    repayment_dict[j]['interest_overdue_more_then90'] = 0

                principal_paid_sum.append(repayment_dict[j]['principal_paid'])
                principal_overdue_less_then30_sum.append(repayment_dict[j]['principal_overdue_less_then30'])
                principal_overdue_from30to90_sum.append(repayment_dict[j]['principal_overdue_from30to90'])
                principal_overdue_more_then90_sum.append(repayment_dict[j]['principal_overdue_more_then90'])
                interest_paid_sum.append(repayment_dict[j]['interest_paid'])
                interest_overdue_less_then30_sum.append(repayment_dict[j]['interest_overdue_less_then30'])
                interest_overdue_from30to90_sum.append(repayment_dict[j]['interest_overdue_from30to90'])
                interest_overdue_more_then90_sum.append(repayment_dict[j]['interest_overdue_more_then90'])
                
            subproject_dict[i]['principal_paid'] = sum(principal_paid_sum)
            subproject_dict[i]['principal_overdue_less_then30'] = sum(principal_overdue_less_then30_sum)
            subproject_dict[i]['principal_overdue_from30to90'] = sum(principal_overdue_from30to90_sum)
            subproject_dict[i]['principal_overdue_more_then90'] = sum(principal_overdue_more_then90_sum)
            subproject_dict[i]['total_principal'] = subproject_dict[i]['principal_overdue_less_then30'] + \
                subproject_dict[i]['principal_overdue_from30to90'] + subproject_dict[i]['principal_overdue_more_then90']
            subproject_dict[i]['interest_paid'] = sum(interest_paid_sum)
            subproject_dict[i]['interest_overdue_less_then30'] = sum(interest_overdue_less_then30_sum)
            subproject_dict[i]['interest_overdue_from30to90'] = sum(interest_overdue_from30to90_sum)
            subproject_dict[i]['interest_overdue_more_then90'] = sum(interest_overdue_more_then90_sum)
            subproject_dict[i]['total_interest'] = subproject_dict[i]['interest_overdue_less_then30'] + \
                subproject_dict[i]['interest_overdue_from30to90'] + subproject_dict[i]['interest_overdue_more_then90']
            subproject_dict[i]['current_balance'] = \
                subproject_dict[i]['loan_amount'] - subproject_dict[i]['principal_paid'] + subproject_dict[i]['total_interest']

            total_loan_amount = total_loan_amount + subproject_dict[i]['loan_amount']
            total_current_balance = total_current_balance + subproject_dict[i]['current_balance']
        total_dict = {'total_loan_amount': total_loan_amount, 'total_current_balance': total_current_balance}
        total_list.append(total_dict)
        return Response({'subproject': subproject_dict, 'loan_amount_and_current_balance': total_list},
                        status=status.HTTP_200_OK)
