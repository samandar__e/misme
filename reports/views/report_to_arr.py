from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json

from projects.models import Projects, LoanFacility
from reports.serializers.report_to_arr import ProjectsReportSerializer, LoanFacilitySerializer


class ReportToArrList(ListAPIView):
    http_method_names = ['get']

    def get(self, request, include_self=True):
        queryset = Projects.objects.all().order_by('id')
        serializer = ProjectsReportSerializer(queryset, many=True)
        projects_dict = json.loads(json.dumps(serializer.data))
        amount = 0
        for i in range(0, len(projects_dict)):
            loan_facility = LoanFacility.objects.all().filter(project=projects_dict[i]['id'])
            loan_facility_serializer = LoanFacilitySerializer(loan_facility, many=True)
            loan_facility_dict = json.loads(json.dumps(loan_facility_serializer.data))
            loan_facility_ifi = []
            for j in range(0, len(loan_facility_dict)):
                loan_facility_ifi.append(loan_facility_dict[j]['ifi'])
            projects_dict[i]['ifi'] = loan_facility_ifi
            if projects_dict[i]['overallProjectCost'] is not None:
                amount = amount + int(projects_dict[i]['overallProjectCost'])
                if projects_dict[i]['implementationStartDate'] is None:
                    projects_dict[i]['implementationStartDate'] = ""
                if projects_dict[i]['implementationEndDate'] is None:
                    projects_dict[i]['implementationEndDate'] = ""
        print('projects', amount)
        return Response({'projects': projects_dict, 'amount': amount}, status=status.HTTP_200_OK)
