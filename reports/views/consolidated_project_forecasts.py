from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import datetime

from components.models import Component
from projects.models import Projects
from reports.serializers.consolidated_project_forecasts import ConsolidatedProjectForecastsSerializer
from reports.serializers.network_schedule import NetworkScheduleSerializer
from reports.views.network_schedule import NetworkScheduleList


class ConsolidatedProjectForecastsList(ListAPIView):
    http_method_names = ['get']

    # Variant: Procurement
    def variant_procurement(self, component, date_year):
        network = NetworkScheduleList()
        # ------------------- Общая сумма проекта -----------------------
        summ_total = []
        sum_list_total = network.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ----------------------- Обшие расходы -------------------------
        summ_general = []
        sum_list_general = network.general_expenses_contract(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # -------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = network.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 1
        network.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = network.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        return component

    # Variant: Access to loans
    def variant_access_to_loans(self, component, date_year):
        network = NetworkScheduleList()
        # ------------------- Общая сумма  проекта --------------------
        summ_total = []
        sum_list_total = network.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  ------------------------
        summ_general = []
        sum_list_general = network.general_expenses_subproject(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = network.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 2
        network.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = network.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        # -------------------------- 2021г. ---------------------------
        return component

    # Variant: Project management
    def variant_project_management(self, component, date_year):
        network = NetworkScheduleList()
        # ------------------- Общая сумма проекта ---------------------
        summ_total = []
        sum_list_total = network.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  -----------------------
        summ_general = []
        sum_list_general = network.general_expenses_pm_cost(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ---------------------------
        summ_remainder = []
        sum_list_remainder = network.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------------
        number = 3
        network.sum_months_and_fact(component, date_year, number)
        # ------------- Прогноз прив-я и Прогноз освоения -------------
        sum_forecast = []
        sum_list_forecast = network.sum_forecast_and_development_forecast(component, sum_forecast, date_year)
        component['forecast'] = sum(sum_list_forecast)
        component['development_forecast'] = sum(sum_list_forecast)
        return component

    def get(self, request, include_self=True):
        date_comp = request.GET['date']
        date_year = datetime.strptime(date_comp, '%Y-%m-%d').year
        queryset = Projects.objects.all().order_by('id')
        serializer_projects = ConsolidatedProjectForecastsSerializer(queryset, many=True)
        projects_dict = json.loads(json.dumps(serializer_projects.data))

        for i in range(0, len(projects_dict)):
            sum_total_amount, sum_general_expenses, sum_remainder, sum_forecast, sum_development_forecast, \
                sum_fact, sum_january, sum_february, sum_march, sum_april, sum_may, sum_june, sum_july, sum_august, \
                sum_september, sum_october, sum_november, sum_december = [], [], [], [], [], [], [], [], [], [], [], \
                [], [], [], [], [], [], []
            # print(projects_dict[i]['id'])
            queryset = Component.objects.all().filter(project=projects_dict[i]['id'], parent=None).order_by('id')
            serializer = NetworkScheduleSerializer(queryset, many=True)
            component_dict = json.loads(json.dumps(serializer.data))

            for j in range(0, len(component_dict)):
                if component_dict[j]['variant'] == 'PR':
                    component = component_dict[j]
                    self.variant_procurement(component, date_year)
                elif component_dict[j]['variant'] == 'AL':
                    component = component_dict[j]
                    self.variant_access_to_loans(component, date_year)
                elif component_dict[j]['variant'] == 'PM':
                    component = component_dict[j]
                    self.variant_project_management(component, date_year)
                sum_total_amount.append(component_dict[j]['total_amount'])
                sum_general_expenses.append(component_dict[j]['general_expenses'])
                sum_remainder.append(component_dict[j]['remainder'])
                sum_forecast.append(component_dict[j]['forecast'])
                sum_development_forecast.append(component_dict[j]['development_forecast'])
                sum_fact.append(component_dict[j]['fact'])
                sum_january.append(component_dict[j]['january'])
                sum_february.append(component_dict[j]['february'])
                sum_march.append(component_dict[j]['march'])
                sum_april.append(component_dict[j]['april'])
                sum_may.append(component_dict[j]['may'])
                sum_june.append(component_dict[j]['june'])
                sum_july.append(component_dict[j]['july'])
                sum_august.append(component_dict[j]['august'])
                sum_september.append(component_dict[j]['september'])
                sum_october.append(component_dict[j]['october'])
                sum_november.append(component_dict[j]['november'])
                sum_december.append(component_dict[j]['december'])
            projects_dict[i]['total_amount'] = float("{:.3f}".format(sum(sum_total_amount)))
            projects_dict[i]['general_expenses'] = float("{:.3f}".format(sum(sum_general_expenses)))
            projects_dict[i]['remainder'] = float("{:.3f}".format(sum(sum_remainder)))
            projects_dict[i]['forecast'] = float("{:.3f}".format(sum(sum_forecast)))
            projects_dict[i]['development_forecast'] = float("{:.3f}".format(sum(sum_development_forecast)))
            projects_dict[i]['fact'] = float("{:.3f}".format(sum(sum_fact)))
            projects_dict[i]['january'] = float("{:.3f}".format(sum(sum_january)))
            projects_dict[i]['february'] = float("{:.3f}".format(sum(sum_february)))
            projects_dict[i]['march'] = float("{:.3f}".format(sum(sum_march)))
            projects_dict[i]['april'] = float("{:.3f}".format(sum(sum_april)))
            projects_dict[i]['may'] = float("{:.3f}".format(sum(sum_may)))
            projects_dict[i]['june'] = float("{:.3f}".format(sum(sum_june)))
            projects_dict[i]['july'] = float("{:.3f}".format(sum(sum_july)))
            projects_dict[i]['august'] = float("{:.3f}".format(sum(sum_august)))
            projects_dict[i]['september'] = float("{:.3f}".format(sum(sum_september)))
            projects_dict[i]['october'] = float("{:.3f}".format(sum(sum_october)))
            projects_dict[i]['november'] = float("{:.3f}".format(sum(sum_november)))
            projects_dict[i]['december'] = float("{:.3f}".format(sum(sum_december)))
            projects_dict[i]['balance'] = float("{:.3f}".format(sum(sum_fact) - sum(sum_remainder)))
            projects_dict[i]['expected_balance'] = float("{:.3f}".format(sum(sum_remainder) - sum(sum_forecast)))

        return Response(projects_dict)
