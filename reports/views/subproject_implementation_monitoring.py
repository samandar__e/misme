from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import datetime

from reports.serializers.subproject_implementation_monitoring import SIMReportSerializer, SubprojectKPISerializer
from subproject.models import Subproject, SubprojectKPI


class SIMReportList(ListAPIView):
    http_method_names = ['get']

    def get(self, request, include_self=True, null=None):
        date_sub = request.GET['date']
        data_pfi = request.GET['pfi']
        date_year = datetime.strptime(date_sub, '%Y-%m-%d').year
        queryset = Subproject.objects.all().filter(project=request.GET['project'],
                                                   start_financing__year=date_year, pfi=data_pfi).order_by('id')
        serializer = SIMReportSerializer(queryset, many=True)
        subproject_dict = json.loads(json.dumps(serializer.data))

        for i in range(0, len(subproject_dict)):
            # print("salommm", subproject_dict[i]['pfi']['id'])
            subproject_dict[i]['men'] = 0 if subproject_dict[i]['men'] == null else subproject_dict[i]['men']
            subproject_dict[i]['women'] = 0 if subproject_dict[i]['women'] == null else subproject_dict[i]['women']
            subproject_dict[i]['total'] = subproject_dict[i]['men'] + subproject_dict[i]['women']

            if subproject_dict[i]['field_of_activities'] == []:
                subproject_dict[i]['field_of_activities'] = ""
            if subproject_dict[i]['pfi'] is None:
                subproject_dict[i]['pfi'] = ""
            if subproject_dict[i]['beneficiary'] is None:
                subproject_dict[i]['beneficiary'] = ""
            if subproject_dict[i]['region'] is None:
                subproject_dict[i]['region'] = ""
            if subproject_dict[i]['loan_amount'] is None:
                subproject_dict[i]['loan_amount'] = 0
            if subproject_dict[i]['start_financing'] is None:
                subproject_dict[i]['start_financing'] = ""
            if subproject_dict[i]['start_activity'] is None:
                subproject_dict[i]['start_activity'] = ""

            queryset_kpi = SubprojectKPI.objects.all().filter(subproject=subproject_dict[i]['id'], data_type='ACTUAL').order_by('id')
            serializer_kpi = SubprojectKPISerializer(queryset_kpi, many=True)
            subproject_kpi_dict = json.loads(json.dumps(serializer_kpi.data))
            value_sum = []
            quantity_sum = []
            for j in range(0, len(subproject_kpi_dict)):
                if subproject_kpi_dict[j]['production_value'] is None:
                    subproject_kpi_dict[j]['production_value'] = 0
                if subproject_kpi_dict[j]['production_quantity'] is None:
                    subproject_kpi_dict[j]['production_quantity'] = 0
                value_sum.append(subproject_kpi_dict[j]['production_value'])
                quantity_sum.append(subproject_kpi_dict[j]['production_quantity'])
            subproject_dict[i]['production_value_sum'] = sum(value_sum)
            subproject_dict[i]['production_quantity_sum'] = sum(quantity_sum)
        return Response({'subproject': subproject_dict}, status=status.HTTP_200_OK)
