from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import datetime

from components.models import Component, PmCosts, Contracts, ProcurementPlan, ComponentImplementation
from components.serializers import PmCostsSerializer, ContractsSerializer, ComponentImplementationSerializer
from projects.models import ProjectImplementation
from reports.serializers.network_schedule import ComponentSerializer, ProcurementPlanSerializer, SubprojectSerializer
from reports.serializers.single_project_implementation import SingleProjectImplementationSerializer, \
    ReportsProjectImplementationSerializer
from subproject.models import Subproject


class SingleProjectImplementationList(ListAPIView):
    http_method_names = ['get']

    # Общая сумма проекта
    def total_amount_component(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            components = Component.objects.all().filter(id=component['id'])
            serializer = ComponentSerializer(components, many=True)
            components_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(components_dict) != null:
                for j in range(0, len(components_dict)):
                    # date_cr = datetime.strptime(components_dict[j]['date_cr'], '%Y-%m-%d').year
                    # if date_cr == date_year:
                    if components_dict[j]['loan_grant_amount'] is not None:
                        sum_value = sum_value + int(components_dict[j]['loan_grant_amount'])
                        component['total_amount'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.total_amount_component(component, summ, date_year)
        return summ

    # Сумма в следующем году
    def next_year_amount(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            implementations = ComponentImplementation.objects.all().filter(component=component['id'])
            serializer = ComponentImplementationSerializer(implementations, many=True)
            implementations_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(implementations_dict) != null:
                for j in range(0, len(implementations_dict)):
                    date_str = implementations_dict[j]['month_year']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year+1:
                        if implementations_dict[j]['forecast_value'] is not None:
                            sum_value = sum_value + int(implementations_dict[j]['forecast_value'])
                            component['year'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.next_year_amount(component, summ, date_year)
        return summ

    # Остаток
    def remainder(self, component, summ, date_year):
        if len(component['component_parent']) == 0:
            remainder = int(component['total_amount']) - float(component['general_expenses'])
            component['remainder'] = float("{:.3f}".format(remainder))
            summ.append(remainder)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.remainder(component, summ, date_year)
        return summ

    # forecast_for_the_year and development_forecast
    def sum_forecast_for_the_year(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            implementations = ComponentImplementation.objects.all().filter(component=component['id'])
            serializer = ComponentImplementationSerializer(implementations, many=True)
            implementations_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(implementations_dict) != null:
                for j in range(0, len(implementations_dict)):
                    date_str = implementations_dict[j]['month_year']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if implementations_dict[j]['forecast_value'] is not None:
                            sum_value = sum_value + int(implementations_dict[j]['forecast_value'])
                            component['forecast_for_the_year'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.sum_forecast_for_the_year(component, summ, date_year)
        return summ

    # Months and Fact
    def months_fact(self, component, date_year, number, date_month, sum_fact, null=None):
        if len(component['component_parent']) == 0:
            january_0, february_0, march_0, april_0, may_0, june_0, july_0, august_0, september_0, october_0, november_0,\
                december_0 = [], [], [], [], [], [], [], [], [], [], [], []
            if number == 1:
                procurement_plan = ProcurementPlan.objects.all().filter(component=component['id'])
                serializer = ProcurementPlanSerializer(procurement_plan, many=True)
                procurement_plan_dict = json.loads(json.dumps(serializer.data))
                data_dict = []
                for i in range(0, len(procurement_plan_dict)):
                    contracts = Contracts.objects.all().filter(procurement_plan=procurement_plan_dict[i]['id'],
                                                               status='PUBLISHED')
                    serializer = ContractsSerializer(contracts, many=True)
                    data_dict = json.loads(json.dumps(serializer.data))
            elif number == 2:
                subprojects = Subproject.objects.all().filter(component=component['id'])
                serializer = SubprojectSerializer(subprojects, many=True)
                data_dict = json.loads(json.dumps(serializer.data))
            elif number == 3:
                pm_costs = PmCosts.objects.all().filter(component=component['id'])
                serializer_pm = PmCostsSerializer(pm_costs, many=True)
                data_dict = json.loads(json.dumps(serializer_pm.data))

            if len(data_dict) != 0:
                for j in range(0, len(data_dict)):
                    if number == 1:
                        date_str = data_dict[j]['date_of_signature']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month

                        amount_sum = float(0 if data_dict[j]['amount_in_usd'] is None else data_dict[j]['amount_in_usd'])
                    elif number == 2:
                        date_str = data_dict[j]['start_financing']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        amount_sum = float(0 if data_dict[j]['total_cost'] is None else data_dict[j]['total_cost'])
                    elif number == 3:
                        date_str = data_dict[j]['date_cr']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                            date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                        amount_sum = float(data_dict[j]['amount_in_usd'])

                    if date_cr == date_year:
                        if date_cr_month <= date_month:
                            if date_cr_month == 1:
                                january_0.append(amount_sum)
                            if date_cr_month == 2:
                                february_0.append(amount_sum)
                            if date_cr_month == 3:
                                march_0.append(amount_sum)
                            if date_cr_month == 4:
                                april_0.append(amount_sum)
                            if date_cr_month == 5:
                                may_0.append(amount_sum)
                            if date_cr_month == 6:
                                june_0.append(amount_sum)
                            if date_cr_month == 7:
                                july_0.append(amount_sum)
                            if date_cr_month == 8:
                                august_0.append(amount_sum)
                            if date_cr_month == 9:
                                september_0.append(amount_sum)
                            if date_cr_month == 10:
                                october_0.append(amount_sum)
                            if date_cr_month == 11:
                                november_0.append(amount_sum)
                            if date_cr_month == 12:
                                december_0.append(amount_sum)
                            component['fact'] = sum(january_0) + sum(february_0) + sum(march_0) + sum(april_0) + sum(may_0) + \
                                sum(june_0) + sum(july_0) + sum(august_0) + sum(september_0) + sum(october_0) + sum(november_0) + \
                                sum(december_0)
                            if component['parent'] == null:
                                sum_fact.append(component['fact'])
        else:
            component_is_not = component['component_parent']
            for i in range(0, len(component_is_not)):
                component = component_is_not[i]
                self.months_fact(component, date_year, number, date_month, sum_fact)
        return component

    # Сумма Факты
    def sum_fact_all(self, component, summ, null=None):
        if len(component['component_parent']) == 0 and component['parent'] != null:
            summ.append(component['fact'])
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.sum_fact_all(component, summ)
        return summ

    def sum_months_and_fact(self, component, date_year, number, date_month, null=None):
        # ------------------ Months and Fact --------------------
        sum_fact = []
        self.months_fact(component, date_year, number, date_month, sum_fact)
        summ = []
        self.sum_fact_all(component, summ)
        component['fact'] = sum(sum_fact) if component['parent'] == null and component['component_parent'] == [] else sum(summ)
        return component

    # forecast
    def sum_forecast(self, component, summ, date_year, date_month, null=None):
        if len(component['component_parent']) == 0:
            implementations = ComponentImplementation.objects.all().filter(component=component['id'])
            serializer = ComponentImplementationSerializer(implementations, many=True)
            implementations_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(implementations_dict) != null:
                for j in range(0, len(implementations_dict)):
                    date_str = implementations_dict[j]['month_year']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                        date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                        date_cr_month = datetime.strptime(date_str, '%Y-%m-%d').month

                    if date_cr == date_year:
                        if date_cr_month <= date_month:
                            if implementations_dict[j]['forecast_value'] is not None:
                                sum_value = sum_value + int(implementations_dict[j]['forecast_value'])
                                component['forecast'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.sum_forecast(component, summ, date_year, date_month, null=None)
        return summ

    # percent
    def percent(self, component, summ):
        if len(component['component_parent']) == 0:
            if component['forecast'] != 0:
                percent = (int(component['fact']) / float(component['forecast'])) * 100
                component['percent'] = percent
                summ.append(percent)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.percent(component, summ)
        return component

    # ========================================== Variant Project management ============================================
    # Variant: Project management Обшие расходы
    def general_expenses_pm_cost(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            pm_costs = PmCosts.objects.all().filter(id=component['id'])
            serializer = PmCostsSerializer(pm_costs, many=True)
            pm_costs_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(pm_costs_dict) != null:
                for j in range(0, len(pm_costs_dict)):
                    date_str = pm_costs_dict[j]['date_cr']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if pm_costs_dict[j]['amount_in_usd'] is not None:
                            sum_value = sum_value + float(pm_costs_dict[j]['amount_in_usd'])
                            component['general_expenses'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_pm_cost(component, summ, date_year)
        return summ

    # Variant: Project management
    def variant_project_management(self, component, date_year, date_month):
        # ------------------- Общая сумма проекта ---------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  -----------------------
        summ_general = []
        sum_list_general = self.general_expenses_pm_cost(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ---------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------------
        number = 3
        self.sum_months_and_fact(component, date_year, number, date_month)
        # ------------------- Прогноз прив-я --------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_for_the_year(component, sum_forecast, date_year)
        component['forecast_for_the_year'] = sum(sum_list_forecast)
        # ---------------------- Прогноз ------------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast(component, sum_forecast, date_year, date_month)
        component['forecast'] = sum(sum_list_forecast)
        # ------------------------- percent ---------------------------
        summ_percent = []
        self.percent(component, summ_percent)
        if component['forecast'] != 0:
            percent = (int(component['fact']) / float(component['forecast'])) * 100
            component['percent'] = float("{:.0f}".format(percent))
        return component

    # ============================================= Variant Procurement ================================================
    # Variant Procurement Обшие расходы
    def general_expenses_contract(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            procurement_plan = ProcurementPlan.objects.all().filter(component=component['id'])
            serializer = ProcurementPlanSerializer(procurement_plan, many=True)
            procurement_plan_dict = json.loads(json.dumps(serializer.data))
            for i in range(0, len(procurement_plan_dict)):
                contracts = Contracts.objects.all().filter(procurement_plan=procurement_plan_dict[i]['id'])
                serializer = ContractsSerializer(contracts, many=True)
                contracts_dict = json.loads(json.dumps(serializer.data))
                sum_value = 0
                if len(contracts_dict) != null:
                    for j in range(0, len(contracts_dict)):
                        date_str = contracts_dict[j]['date_of_signature']
                        if date_str is None:
                            date_str = '1001-01-01'
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                        else:
                            date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                        if date_cr == date_year:
                            if contracts_dict[j]['amount_in_usd'] is not None:
                                sum_value = sum_value + float(contracts_dict[j]['amount_in_usd'])
                                component['general_expenses'] = sum_value
                summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_contract(component, summ, date_year)
        return summ

    # Variant: Procurement
    def variant_procurement(self, component, date_year, date_month):
        # ------------------- Общая сумма проекта -----------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ----------------------- Обшие расходы -------------------------
        summ_general = []
        sum_list_general = self.general_expenses_contract(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # -------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 1
        self.sum_months_and_fact(component, date_year, number, date_month)
        # ------------------- Прогноз прив-я --------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_for_the_year(component, sum_forecast, date_year)
        component['forecast_for_the_year'] = sum(sum_list_forecast)
        # ---------------------- Прогноз ------------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast(component, sum_forecast, date_year, date_month)
        component['forecast'] = sum(sum_list_forecast)
        # ------------------------- percent ---------------------------
        summ_percent = []
        self.percent(component, summ_percent)
        if component['forecast'] != 0:
            percent = (int(component['fact']) / float(component['forecast'])) * 100
            component['percent'] = float("{:.0f}".format(percent))
        return component

    # ============================================== Access to loans ===================================================
    # Access to loans Обшие расходы
    def general_expenses_subproject(self, component, summ, date_year, null=None):
        if len(component['component_parent']) == 0:
            subprojects = Subproject.objects.all().filter(component=component['id'])
            serializer = SubprojectSerializer(subprojects, many=True)
            subprojects_dict = json.loads(json.dumps(serializer.data))
            sum_value = 0
            if len(subprojects_dict) != null:
                for j in range(0, len(subprojects_dict)):
                    date_str = subprojects_dict[j]['start_financing']
                    if date_str is None:
                        date_str = '1001-01-01'
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year
                    else:
                        date_cr = datetime.strptime(date_str, '%Y-%m-%d').year

                    if date_cr == date_year:
                        if subprojects_dict[j]['total_cost'] is not None:
                            sum_value = sum_value + float(subprojects_dict[j]['total_cost'])
                            component['general_expenses'] = sum_value
            summ.append(sum_value)
        else:
            component_isnot = component['component_parent']
            for i in range(0, len(component_isnot)):
                component = component_isnot[i]
                self.general_expenses_subproject(component, summ, date_year)
        return summ

    # Variant: Access to loans
    def variant_access_to_loans(self, component, date_year, date_month):
        # ------------------- Общая сумма  проекта --------------------
        summ_total = []
        sum_list_total = self.total_amount_component(component, summ_total, date_year)
        component['total_amount'] = sum(sum_list_total)
        # ---------------------- Обшие расходы  ------------------------
        summ_general = []
        sum_list_general = self.general_expenses_subproject(component, summ_general, date_year)
        component['general_expenses'] = sum(sum_list_general)
        # ------------------------- Остаток ----------------------------
        summ_remainder = []
        sum_list_remainder = self.remainder(component, summ_remainder, date_year)
        component['remainder'] = float("{:.3f}".format(sum(sum_list_remainder)))
        # ------------------ Months and Fact --------------------
        number = 2
        self.sum_months_and_fact(component, date_year, number, date_month)
        # ------------------- Прогноз прив-я --------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast_for_the_year(component, sum_forecast, date_year)
        component['forecast_for_the_year'] = sum(sum_list_forecast)
        # ---------------------- Прогноз ------------------------
        sum_forecast = []
        sum_list_forecast = self.sum_forecast(component, sum_forecast, date_year, date_month)
        component['forecast'] = sum(sum_list_forecast)
        # ------------------------- percent ---------------------------
        summ_percent = []
        self.percent(component, summ_percent)
        if component['forecast'] != 0:
            percent = (int(component['fact']) / float(component['forecast'])) * 100
            component['percent'] = float("{:.0f}".format(percent))
        return component

    def problems_and_activities_implemented(self, request, date_year, problems_and_activities):
        queryset = ProjectImplementation.objects.all().filter(project=request.GET['project']).order_by('id')
        serializer = ReportsProjectImplementationSerializer(queryset, many=True)
        implementation_dict = json.loads(json.dumps(serializer.data))
        for i in range(0, len(implementation_dict)):
            date_cr_year = datetime.strptime(implementation_dict[i]['date'], '%Y-%m-%d').year
            if date_cr_year == date_year:
                problem_list = {'activitiesImplemented': implementation_dict[i]['activitiesImplemented'],
                                'problems': implementation_dict[i]['problems']}
                problems_and_activities.append(problem_list)
        return problems_and_activities

    def get(self, request, include_self=True):
        date_comp = request.GET['date']
        date_year = datetime.strptime(date_comp, '%Y-%m-%d').year
        date_month = datetime.strptime(date_comp, '%Y-%m-%d').month
        queryset = Component.objects.all().filter(project=request.GET['project'], parent=None).order_by('id')
        serializer = SingleProjectImplementationSerializer(queryset, many=True)
        component_dict = json.loads(json.dumps(serializer.data))
        problems_and_activities = []
        for i in range(0, len(component_dict)):
            if component_dict[i]['variant'] == 'PR':
                component = component_dict[i]
                self.variant_procurement(component, date_year, date_month)
            elif component_dict[i]['variant'] == 'AL':
                component = component_dict[i]
                self.variant_access_to_loans(component, date_year, date_month)
            elif component_dict[i]['variant'] == 'PM':
                component = component_dict[i]
                self.variant_project_management(component, date_year, date_month)
        self.problems_and_activities_implemented(request, date_year, problems_and_activities)
        return Response({'components': component_dict, 'problems_and_activitiesImplemented': problems_and_activities})
