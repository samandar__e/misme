from django.urls import re_path, path
from .views import UserView, RoleView, RolePermissionGridView, RoleDetailView

urlpatterns = [
    re_path(r'^user/$', UserView.as_view(), name='current_user'),

    re_path(r'^role/$', RoleView.as_view(), name='roles_view'),
    path('role/<int:pk>', RoleDetailView.as_view(), name='roles_details_view'),
    re_path(r'^role/grid/$', RolePermissionGridView.as_view(), name='roles_grid')
]