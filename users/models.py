from django.conf import settings
from django.contrib.contenttypes.models import ContentType, ContentTypeManager
from django.db import models
from django.contrib.auth.models import AbstractUser, Group, GroupManager
from django.utils.translation import ugettext_lazy as _


GENDERS = (
    ('male', _('Male')),
    ('female', _('Female')),
)

WEEK_DAYS = (
    ('monday', _('Monday'),),
    ('tuesday', _('Tuesday'),),
    ('wednesday', _('Wednesday'),),
    ('thursday', _('Thursday'),),
    ('friday', _('Friday'),),
    ('saturday', _('Saturday'),),
    ('sunday', _('Sunday'),),
)


class CommonInfo(models.Model):
    created_time = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_time = models.DateTimeField(auto_now_add=False, auto_now=True)


class User(AbstractUser):
    username = models.CharField(max_length=255, null=False, blank=False, unique=True)
    first_name = models.CharField(_('First name'), max_length=30, blank=True)
    last_name = models.CharField(_('Last name'), max_length=30, blank=True)
    is_active = models.BooleanField(_('Active'), default=True)
    date_of_birthday = models.DateField(_('date of birthday'), null=True, blank=True, )
    gender = models.CharField(choices=GENDERS, max_length=6, null=True, blank=True, )
    avatar = models.ImageField(null=True)
    date_joined = models.DateTimeField(_('Date joined'), auto_now_add=True)
    created_time = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated_time = models.DateTimeField(auto_now_add=False, auto_now=True)
    created_by = models.ForeignKey('self', null=True, blank=True, on_delete=models.SET_NULL)
    updated_by = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='created_by_user', null=True, on_delete=models.SET_NULL)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='updated_by_user', null=True, on_delete=models.SET_NULL)

    # USERNAME_FIELD = 'phone'
    # objects = UserManager()
    # REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Role(Group):
    objects = GroupManager()

    description = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('role')
        verbose_name_plural = _('roles')


class AppModule(models.Model):
    name = models.CharField(_('Module name'), max_length=125, blank=True)
    on_dashboard = models.BooleanField(default=False)
    content_types = models.ManyToManyField(ContentType)

    class Meta:
        verbose_name = _('module')
        verbose_name_plural = _('modules')
