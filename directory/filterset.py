from django_filters.rest_framework import FilterSet

from directory.models import District, ExchangeRate


class DistrictFilter(FilterSet):

    class Meta:
        model = District
        fields = ['region']


class ExchangeRateFilter(FilterSet):

    class Meta:
        model = ExchangeRate
        fields = ['date']
