from django.urls import re_path, path

from .views.country import CountryView, CountryDetailView
from .views.department import DepartmentView, DepartmentDetailView
from .views.district import DistrictView, DistrictDetailView, DistrictGeoJsonView
from .views.employee import EmployeeView, EmployeeDetailView
from .views.entity_type import EntityTypeView, EntityTypeDetailView
from .views.enums import MembershipGroupsList, SERCategoriesList, IndicatorLevelsList, IndicatorTypesList, \
    IndicatorPhasesList, IndicatorFrequenciesList
from .views.exchange_rate import ExchangeRatesView
from .views.foa import FOAView, FOADetailView
from .views.ififp import IFIFPView, IFIFPDetailView
from .views.indicator import IndicatorDetailView, IndicatorView
from .views.position import PositionView, PositionDetailView
from .views.region import RegionView, RegionDetailView, RegionGeoJsonView, RegionDistrictsGeoJsonView, \
    RegionGeoJsonListView
from .views.currency import CurrencyView, CurrencyDetailView
from .views.sector import SectorView, SectorDetailView
from .views.ser import SERView, SERDetailView
from .views.uom import UOMView, UOMDetailView


urlpatterns = [
    re_path(r'^region/$', RegionView.as_view(), name='regions_view'),
    path('region/<int:pk>', RegionDetailView.as_view(), name='region_detail_view'),
    path('region/<int:pk>/geo', RegionGeoJsonView.as_view(), name='region_geo_json_view'),
    path('region/geo/all', RegionGeoJsonListView.as_view(), name='region_geo_json_list_view'),
    path('region/<int:pk>/district/geo', RegionDistrictsGeoJsonView.as_view(), name='region_district_geo_json_view'),

    re_path(r'^district/$', DistrictView.as_view(), name='districts_view'),
    path('district/<int:pk>', DistrictDetailView.as_view(), name='districts_detail_view'),
    path('district/<int:pk>/geo/', DistrictGeoJsonView.as_view(), name='district_geo_json_view'),

    re_path(r'^department/$', DepartmentView.as_view(), name='department_view'),
    path('department/<int:pk>', DepartmentDetailView.as_view(), name='department_detail_view'),

    re_path(r'^position/$', PositionView.as_view(), name='position_view'),
    path('position/<int:pk>', PositionDetailView.as_view(), name='position_detail_view'),

    re_path(r'^currency/$', CurrencyView.as_view(), name='currency_view'),
    path('currency/<int:pk>', CurrencyDetailView.as_view(), name='currency_detail_view'),

    re_path(r'^uom/$', UOMView.as_view(), name='uom_view'),
    path('uom/<int:pk>', UOMDetailView.as_view(), name='uom_detail_view'),

    re_path(r'^ififp/$', IFIFPView.as_view(), name='ififp_view'),
    path('ififp/<int:pk>', IFIFPDetailView.as_view(), name='ififp_detail_view'),

    re_path(r'^ser/$', SERView.as_view(), name='ser_view'),
    path('ser/<int:pk>', SERDetailView.as_view(), name='ser_detail_view'),

    re_path(r'^sector/$', SectorView.as_view(), name='sector_view'),
    path('sector/<int:pk>', SectorDetailView.as_view(), name='sector_detail_view'),

    re_path(r'^foa/$', FOAView.as_view(), name='foa_view'),
    path('foa/<int:pk>', FOADetailView.as_view(), name='foa_detail_view'),

    re_path(r'^indicator/$', IndicatorView.as_view(), name='indicator_view'),
    path('indicator/<int:pk>', IndicatorDetailView.as_view(), name='indicator_detail_view'),

    re_path(r'^employee/$', EmployeeView.as_view(), name='employee_view'),
    path('employee/<int:pk>', EmployeeDetailView.as_view(), name='employee_detail_view'),

    re_path(r'^country/$', CountryView.as_view(), name='country_view'),
    path('country/<int:pk>', CountryDetailView.as_view(), name='country_detail_view'),

    re_path(r'^entity-type/$', EntityTypeView.as_view(), name='entity_type_view'),
    path('entity-type/<int:pk>', EntityTypeDetailView.as_view(), name='entity_type_detail_view'),

    re_path(r'^exchange-rates/$', ExchangeRatesView.as_view(), name='exchange_rates_view'),

    # enums urls
    re_path(r'^enum-membership-groups/$', MembershipGroupsList.as_view(), name='membership_group_view'),
    re_path(r'^enum-ser-categories/$', SERCategoriesList.as_view(), name='ser_categories_view'),
    re_path(r'^enum-indicator-levels/$', IndicatorLevelsList.as_view(), name='indicator_levels_view'),
    re_path(r'^enum-indicator-types/$', IndicatorTypesList.as_view(), name='indicator_types_view'),
    re_path(r'^enum-indicator-phases/$', IndicatorPhasesList.as_view(), name='indicator_phases_view'),
    re_path(r'^enum-indicator-frequencies/$', IndicatorFrequenciesList.as_view(), name='indicator_frequencies_view'),

]
