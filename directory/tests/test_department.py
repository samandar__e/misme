from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class DepartmentTest(BaseTestCase):
    fixtures = ['user.yaml', 'department.yaml']

    def test_list(self):
        response = self.client.get(reverse('department_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        response = self.client.post(reverse('department_view'), {
            'name': 'Test',
            'name_en': 'Test name EN',
            'name_uz': 'Test name UZ',
            'name_ru': 'Test name RU',
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(reverse('department_view'), {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('department_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

    def test_update(self):
        url = reverse('department_detail_view', kwargs={"pk": 1})
        response = self.client.put(url, {
            'name': 'Test (UPD)',
            'name_en': 'Test name EN',
            'name_uz': 'Test name UZ',
            'name_ru': 'Test name RU',
        })
        self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('department_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
