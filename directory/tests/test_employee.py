from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class EmployeeTest(BaseTestCase):
    pass
    fixtures = ['user.yaml', 'department.yaml', 'position.yaml', 'region.yaml', 'employee.yaml']

    def test_list(self):
        response = self.client.get(reverse('employee_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        url = reverse('employee_view')
        # response = self.client.post(url, {
        #     'fullname': 'James Braun',
        #     'membership_group': 'BENEFICIARY',
        #     'organization': 'NBU',
        #     'department': 1,
        #     'position': 1,
        #     'qualification': 'Qualification',
        #     'region': 1,
        #     'phone': '998991112233',
        #     'email': 'test@test.te',
        #     'user': 1,
        #     'role': 1,
        #     'IFI': 1,
        #     'PFI': 1,
        #     'beneficiary': 1,
        #     'agency': 1,
        #     'consultant': 1
        # })
        # self.assertEqual(response.status_code, 201)

        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('employee_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

    def test_update(self):
        url = reverse('employee_detail_view', kwargs={"pk": 1})
        # response = self.client.put(url, {
        #     'name': 'Test name (UPD)',
        #     'name_en': 'Test name EN',
        #     'name_uz': 'Test name UZ',
        #     'name_ru': 'Test name RU',
        #     'department': 1
        # })
        # self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('employee_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
