from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class UOMTest(BaseTestCase):
    fixtures = ['user.yaml', 'UOM.yaml']

    def test_list(self):
        response = self.client.get(reverse('uom_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        response = self.client.post(reverse('uom_view'), {
            'name': 'Test name',
            'symbol': 'kg',
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(reverse('uom_view'), {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('uom_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

        response = self.client.delete(reverse('uom_detail_view', kwargs={"pk": 0}))
        self.assertEqual(response.status_code, 404)

    def test_update(self):
        url = reverse('uom_detail_view', kwargs={"pk": 1})
        response = self.client.put(url, {
            'name': 'Test name (UPD)',
            'symbol': 'kg',
        })
        self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('uom_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
