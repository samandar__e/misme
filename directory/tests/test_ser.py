from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class SERTest(BaseTestCase):
    fixtures = ['user.yaml', 'ser.yaml']

    def test_list(self):
        response = self.client.get(reverse('ser_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        response = self.client.post(reverse('ser_view'), {
            'name': 'Requirement name 1',
            'category': 'GGE',
            'description': 'Description text'
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(reverse('ser_view'), {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('ser_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

    def test_update(self):
        url = reverse('ser_detail_view', kwargs={"pk": 1})
        response = self.client.put(url, {
            'name': 'Requirement name 1 (UPD)',
            'category': 'GGE',
            'description': 'Description text'
        })
        self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('ser_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
