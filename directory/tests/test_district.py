from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class DistrictTest(BaseTestCase):
    fixtures = ['user.yaml', 'region.yaml', 'district.json']

    def test_list(self):
        response = self.client.get(reverse('districts_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        response = self.client.post(reverse('districts_view'), {
            'code': 1703202,
            'name': 'Test',
            'name_en': 'Test name EN',
            'name_uz': 'Test name UZ',
            'name_ru': 'Test name RU',
            'region_id': 1,
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(reverse('districts_view'), {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('districts_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

    def test_update(self):
        url = reverse('districts_detail_view', kwargs={"pk": 1})
        response = self.client.put(url, {
            'code': 1703202,
            'name': 'Test',
            'name_en': 'Test name EN',
            'name_uz': 'Test name UZ',
            'name_ru': 'Test name RU',
            'region_id': 2,
        })
        self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('districts_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
