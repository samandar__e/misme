from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class ExchangeRateViewTest(BaseTestCase):
    fixtures = ['user.yaml']

    def test_list(self):
        response = self.client.get(reverse('exchange_rates_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")