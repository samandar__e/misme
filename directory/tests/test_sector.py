from django.urls import reverse
from restapp.tests.base_test import BaseTestCase


class SectorTest(BaseTestCase):
    fixtures = ['user.yaml', 'sector.yaml']

    def test_list(self):
        response = self.client.get(reverse('sector_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "pagination")
        self.assertContains(response, "results")

    def test_create(self):
        response = self.client.post(reverse('sector_view'), {
            'name': 'Test name',
        })
        self.assertEqual(response.status_code, 201)

        response = self.client.post(reverse('sector_view'), {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_delete(self):
        response = self.client.delete(reverse('sector_detail_view', kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)

    def test_update(self):
        url = reverse('sector_detail_view', kwargs={"pk": 1})
        response = self.client.put(url, {
            'name': 'Test name (UPD)',
        })
        self.assertEqual(response.status_code, 202)

        response = self.client.put(url, {})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data["errorCode"], 400)
        self.assertEqual(response.data["errorMessage"], "Bad Request")

    def test_detail(self):
        url = reverse('sector_detail_view', kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.data["id"], 1)
