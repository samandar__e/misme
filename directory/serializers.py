from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from users.models import User
from users.serializers import RelatedUserSerializer, GroupSerializer, RelatedUserPutSerializer
from .models import Region, District, Department, Position, Currency, UOM, IFIFP, SER, Sector, FOA, Indicator, \
    Employee, Qualification, ExchangeRate, GeoJsonObject, EntityType


class LocaleSerializer(serializers.ModelSerializer):
    name_uz = serializers.CharField(allow_blank=False)
    name_ru = serializers.CharField(allow_blank=False)
    name_en = serializers.CharField(allow_blank=False)


class RelatedRegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('id', 'name')


class RelatedDistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('id', 'name')


class RelatedDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name')


class RelatedPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ('id', 'name')


class RegionSerializer(LocaleSerializer):

    class Meta:
        model = Region
        fields = ('id', 'code', 'name', 'name_uz', 'name_ru', 'name_en')


class RegionDetailSerializer(LocaleSerializer):

    class Meta:
        model = Region
        fields = ('id', 'code', 'name_uz', 'name_ru', 'name_en')


class DistrictListSerializer(LocaleSerializer):
    region = RelatedRegionSerializer()

    class Meta:
        model = District
        fields = ('id', 'code', 'name', 'name_uz', 'name_ru', 'name_en', 'region')


class DistrictDetailSerializer(LocaleSerializer):

    class Meta:
        model = District
        fields = ('id', 'code', 'name_uz', 'name_ru', 'name_en', 'region')


class DepartmentSerializer(LocaleSerializer):

    class Meta:
        model = Department
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en')


class DepartmentDetailSerializer(LocaleSerializer):

    class Meta:
        model = Department
        fields = ('id', 'name_uz', 'name_ru', 'name_en')


class PositionSerializer(LocaleSerializer):

    class Meta:
        model = Position
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en', 'department')


class PositionDetailSerializer(LocaleSerializer):

    class Meta:
        model = Position
        fields = ('id', 'name_uz', 'name_ru', 'name_en', 'department')


class CurrencySerializer(LocaleSerializer):

    class Meta:
        model = Currency
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en', 'symbol')


class CurrencyDetailSerializer(LocaleSerializer):

    class Meta:
        model = Currency
        fields = ('id', 'name_uz', 'name_ru', 'name_en', 'symbol')


class UOMSerializer(serializers.ModelSerializer):
    class Meta:
        model = UOM
        fields = ('id', 'name', 'symbol')


class IFIFPSerializer(serializers.ModelSerializer):
    class Meta:
        model = IFIFP
        fields = ('id', 'name', 'amount', 'term', 'loan_interest', 'grace_period', 'requirement')


class SERSerializer(serializers.ModelSerializer):
    class Meta:
        model = SER
        fields = ('id', 'name', 'category', 'description')


class SectorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sector
        fields = ('id', 'name')


class SectorListSerializer(LocaleSerializer):
    class Meta:
        model = Sector
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en')


class SectorDetailSerializer(LocaleSerializer):
    class Meta:
        model = Sector
        fields = ('id', 'name_uz', 'name_ru', 'name_en')


class FOASerializer(LocaleSerializer):
    class Meta:
        model = FOA
        fields = ('id', 'name_uz', 'name_ru', 'name_en', 'sector')


class RelatedFOASerializer(serializers.ModelSerializer):
    class Meta:
        model = FOA
        fields = ('id', 'name')


class FOAListSerializer(serializers.ModelSerializer):
    sector = SectorSerializer()

    class Meta:
        model = FOA
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en', 'sector')


class IndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Indicator
        fields = ('id', 'name', 'level', 'type', 'phase', 'description', 'uom',
                  'collection_methods', 'data_source', 'frequency', 'responsible_person', 'reporting_format',
                  'control_tool')


class IndicatorListSerializer(serializers.ModelSerializer):
    uom = UOMSerializer()

    class Meta:
        model = Indicator
        fields = ('id', 'name', 'level', 'type', 'phase', 'description', 'uom',
                  'collection_methods', 'data_source', 'frequency', 'responsible_person', 'reporting_format',
                  'control_tool')


class QualificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Qualification
        fields = ('id', 'name')


class EmployeeSerializer(serializers.ModelSerializer):
    user = RelatedUserSerializer(required=False)

    class Meta:
        model = Employee
        fields = ('id', 'firstname', 'lastname', 'membership_group', 'organization', 'department', 'position',
                  'qualification', 'region', 'phone', 'user', 'role', 'IFI', 'PFI', 'beneficiary', 'agency',
                  'consultant', 'projects')

    def create(self, validated_data):

        user_data = validated_data.pop('user')
        projects = validated_data.pop('projects')
        employee = Employee.objects.create(**validated_data)
        user = User.objects.create_user(
            username=user_data['username'],
            password=user_data['password'],
            first_name=validated_data['firstname'],
            last_name=validated_data['lastname']
        )
        role = validated_data.get('role')
        if role:
            user.groups.add(role)
        user.save()
        employee.user = user
        # save projects
        for project in projects:
            employee.projects.add(project)
        employee.save()
        return employee


class EmployeeDetailSerializer(serializers.ModelSerializer):
    user = RelatedUserPutSerializer(required=False)

    class Meta:
        model = Employee
        fields = ('id', 'firstname', 'lastname', 'membership_group', 'organization', 'department', 'position',
                  'qualification', 'region', 'phone', 'user', 'role', 'IFI', 'PFI', 'beneficiary', 'agency',
                  'consultant', 'projects')

    def update(self, instance, validated_data):

        user_data = validated_data.pop('user')
        role = validated_data.get('role', instance.role)
        password = user_data.get('password')
        projects = validated_data.pop('projects')
        instance.user.username = user_data.get('username', instance.user.username)
        if password:
            instance.user.password = make_password(password)

        instance.user.groups.clear()
        instance.user.groups.add(role)
        instance.user.save()

        instance.projects.clear()
        for project in projects:
            instance.projects.add(project)

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        return instance


class EmployeeListSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    user = RelatedUserSerializer()

    region = RelatedRegionSerializer()
    department = RelatedDepartmentSerializer()
    position = RelatedPositionSerializer()
    role = GroupSerializer()

    class Meta:
        model = Employee
        fields = ('id', 'firstname', 'lastname', 'membership_group', 'organization', 'department', 'position',
                  'qualification', 'region', 'phone', 'email', 'user', 'role', 'projects')


class CountrySerializer(LocaleSerializer):

    class Meta:
        model = Region
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en')


class CountryDetailSerializer(LocaleSerializer):

    class Meta:
        model = Region
        fields = ('id', 'name_uz', 'name_ru', 'name_en')


class CurrencyField(serializers.Field):
    def to_internal_value(self, data):
        pass

    def to_representation(self, instance):
        return instance.currency.name


class ExchangeRateSerializer(serializers.ModelSerializer):
    currency = CurrencyField(source='*')

    class Meta:
        model = ExchangeRate
        fields = ('id', 'code', 'currency', 'nominal', 'rate', 'diff', 'date', 'symbol')


class GeoJsonObjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = GeoJsonObject
        fields = ('id', 'name', 'code', 'geo_json')


class EntityTypeSerializer(LocaleSerializer):
    class Meta:
        model = EntityType
        fields = ('id', 'name', 'name_uz', 'name_ru', 'name_en')


class RelatedEntityTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = EntityType
        fields = ('id', 'name')


class EntityTypeDetailSerializer(LocaleSerializer):
    class Meta:
        model = EntityType
        fields = ('id', 'name_uz', 'name_ru', 'name_en')
