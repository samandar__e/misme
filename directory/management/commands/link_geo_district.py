from django.core.management import BaseCommand

from directory.models import GeoJsonObject, District


class Command(BaseCommand):
    help = 'Linked geo jsons to districts'

    def handle(self, *args, **options):
        geo_jsons = GeoJsonObject.objects.filter(id__gt=14).order_by('id')
        for geo in geo_jsons:
            district = District.objects.filter(code__endswith=geo.code).first()
            if district:
                print("{0} - {1}".format(district, geo))
                district.geo_json = geo
                district.save()
