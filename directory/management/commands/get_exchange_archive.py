import datetime

from django.core.management import BaseCommand
import requests

from directory.utils import save_rates
from misme import settings


class Command(BaseCommand):
    help = 'Get exchange rates archive by year from cbu.uz'

    def add_arguments(self, parser):
        parser.add_argument('--year', type=int)

    def handle(self, *args, **options):
        try:
            year = options.get('year', datetime.date.year)
            start_date = datetime.date(year, 1, 1)
            end_date = datetime.date.today()
            delta = end_date - start_date

            for day in range(delta.days + 1):
                date = (start_date + datetime.timedelta(days=day)).isoformat()
                route = f"{settings.CBU_EX_RATES_URL}all/{date}/"
                r = requests.get(route, timeout=5)
                rates = r.json()
                save_rates(rates, date)

        except requests.exceptions.Timeout:
            print("Timeout is except")
        except requests.exceptions.TooManyRedirects:
            print("Many redirects")
        except requests.exceptions.RequestException as e:
            print(e)
