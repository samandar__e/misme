from django.core.management import BaseCommand

from directory.models import GeoJsonObject, Region


class Command(BaseCommand):
    help = 'Linked geo jsons to regions'

    def handle(self, *args, **options):
        geo_jsons = GeoJsonObject.objects.filter(id__lte=14).order_by('id')
        for geo in geo_jsons:
            region = Region.objects.filter(code__endswith=geo.code).first()
            if region:
                print("{0} - {1}".format(region, geo))
                region.geo_json = geo
                region.save()