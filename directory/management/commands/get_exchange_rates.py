import datetime
from django.core.management import BaseCommand

from directory.utils import get_exchange_rates


class Command(BaseCommand):
    help = 'Get exchange rates from cbu.uz'

    def add_arguments(self, parser):
        parser.add_argument('-d', '--date', type=str, help='Parsing custom date')

    def handle(self, *args, **options):

        date = options.get("date")
        if date is None:
            date = datetime.date.today()
        get_exchange_rates(date)
