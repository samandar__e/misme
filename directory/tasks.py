import datetime
from celery.utils.log import get_task_logger
from celery import shared_task

from directory.utils import get_exchange_rates

logger = get_task_logger(__name__)


@shared_task
def task_get_exchanges_from_cbu():
    logger.info("Starting parsing exchanges")
    date = datetime.date.today()
    get_exchange_rates(date)
    logger.info("Finished parsing exchanges")

