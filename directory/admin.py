from django.contrib import admin

from directory.models import District, Region, Department, Position, Employee, Qualification, Currency, UOM, Indicator, \
    IFIFP, SER, Sector, FOA, Country, ExchangeRate, GeoJsonObject, EntityType


@admin.register(GeoJsonObject)
class GeoJsonObjectAdmin(admin.ModelAdmin):
    exclude = ('created_by', 'updated_by')


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'name_en', 'name_ru', 'name_uz')
    fields = ('code', 'name', 'name_en', 'name_ru', 'name_uz')
    search_fields = ('code', 'name', 'name_en', 'name_ru', 'name_uz')


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')
    fields = ('name', 'name_en', 'name_ru', 'name_uz', 'code', 'geo_json')
    search_fields = ('name', 'name_en', 'name_ru', 'name_uz', 'code')


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'region')
    fields = ('name', 'name_en', 'name_ru', 'name_uz', 'code', 'region', 'geo_json')
    search_fields = ('name', 'name_en', 'name_ru', 'name_uz', 'code')


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'department')
    fields = ('name', 'name_en', 'name_ru', 'name_uz', 'department')
    search_fields = ('name', 'name_en', 'name_ru', 'name_uz')


@admin.register(Qualification)
class QualificationAdmin(admin.ModelAdmin):
    list_display = ('name',)
    fields = ('name',)
    search_fields = ('name',)


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'organization', 'department')
    search_fields = ('firstname',)


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz', 'symbol')
    fields = ('name_en', 'name_ru', 'name_uz', 'symbol')
    search_fields = ('name_en', 'name_ru', 'name_uz', 'symbol')


@admin.register(UOM)
class UOMAdmin(admin.ModelAdmin):
    list_display = ('name', 'symbol')
    fields = ('name', 'symbol')
    search_fields = ('name', 'symbol')


@admin.register(Indicator)
class IndicatorAdmin(admin.ModelAdmin):
    list_display = ('name', 'level', 'type', 'phase')


@admin.register(IFIFP)
class IFFPAdmin(admin.ModelAdmin):
    list_display = ('name', 'amount', 'term')


@admin.register(SER)
class SERAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')


@admin.register(Sector)
class SectorAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name', 'name_en', 'name_ru', 'name_uz',)


@admin.register(FOA)
class FOAAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_en', 'name_ru', 'name_uz', 'sector')
    fields = ('name', 'name_en', 'name_ru', 'name_uz', 'sector')


@admin.register(ExchangeRate)
class ExchangeRateAdmin(admin.ModelAdmin):
    list_display = ('currency', 'symbol', 'nominal', 'rate', 'diff', 'date')
    fields = ('code', 'currency', 'symbol', 'nominal', 'rate', 'diff', 'date')
    search_fields = ('symbol', 'date')


@admin.register(EntityType)
class EntityTypeAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name', 'name_en', 'name_ru', 'name_uz',)
