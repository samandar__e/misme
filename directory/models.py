from django.db import models
from django.utils.translation import ugettext_lazy as _

from directory.managers.sector import SectorManager
from restapp.models import BaseModel
from users.models import User, Role


class GeoJsonObject(BaseModel):
    name = models.CharField(_('Object name'), max_length=255, blank=True)
    code = models.CharField(_('Object code'), max_length=20, blank=True)
    geo_json = models.TextField(_('GeoJson'), blank=False)

    class Meta:
        verbose_name = _('GeoJson object')
        verbose_name_plural = _('GeoJson objects')

    def __str__(self):
        return self.name


class Country(BaseModel):
    code = models.CharField(_('Country code'), max_length=3, blank=True)
    name = models.CharField(_('Country name'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')

    def __str__(self):
        return self.name


class Region(BaseModel):
    code = models.CharField(_('Region code'), max_length=50, blank=False)
    name = models.CharField(max_length=255, blank=False)
    geo_json = models.OneToOneField(GeoJsonObject, related_name='region', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('region')
        verbose_name_plural = _('regions')

    def __str__(self):
        return self.name


class District(BaseModel):
    code = models.CharField(_('District code'), max_length=50, blank=False)
    name = models.CharField(_('District name'), max_length=255, blank=False)
    region = models.ForeignKey(Region, related_name='districts', on_delete=models.SET_NULL, null=True)
    geo_json = models.OneToOneField(GeoJsonObject, related_name='district', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('district')
        verbose_name_plural = _('districts')

    def __str__(self):
        return self.name


class Department(BaseModel):
    name = models.CharField(_('Department name'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('department')
        verbose_name_plural = _('departments')

    def __str__(self):
        return self.name


class Position(BaseModel):
    name = models.CharField(_('Position name'), max_length=255, blank=False)
    department = models.ForeignKey(Department, related_name='positions', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('position')
        verbose_name_plural = _('positions')

    def __str__(self):
        return self.name


class Qualification (BaseModel):
    name = models.CharField(_('Qualification name'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('qualification')
        verbose_name_plural = _('qualifications')

    def __str__(self):
        return self.name


class Employee(BaseModel):
    MEMBERSHIP_GROUP = (
        ('GOVERNMENT', _('Government')),
        ('IFI', _('IFI')),
        ('PFI', _('PFI')),
        ('BENEFICIARY', _('Beneficiary')),
        ('US', _('UZAIFSA Staff')),
        ('UC', _('UZAIFSA Consultant')),
    )

    firstname = models.CharField(_('Employee firstname'), max_length=255, null=True, blank=True)
    lastname = models.CharField(_('Employee lastname'), max_length=255, null=True, blank=True)
    membership_group = models.CharField(max_length=50, choices=MEMBERSHIP_GROUP, default='GOVERNMENT',
                                        verbose_name=_('Membership group'))
    organization = models.CharField(_('Organization name'), max_length=255, blank=True)
    department = models.ForeignKey(Department, related_name='employees', on_delete=models.SET_NULL, null=True)
    position = models.ForeignKey(Position, related_name='employees', on_delete=models.SET_NULL, null=True)
    qualification = models.CharField(_('Qualification'), max_length=255, blank=True, null=True)
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True)
    phone = models.CharField(_('Contact phone number'), max_length=20, blank=True)
    email = models.CharField(_('Contact email'), max_length=255, blank=True)
    user = models.ForeignKey(User, related_name='employees', on_delete=models.SET_NULL, null=True)
    role = models.ForeignKey(Role, on_delete=models.SET_NULL, null=True)
    IFI = models.ForeignKey('crm.InternationalFinancialInstitute', related_name='employees', on_delete=models.SET_NULL,
                            null=True)
    PFI = models.ForeignKey('crm.ParticipatingFinancialInstitution', related_name='employees', on_delete=models.SET_NULL,
                            null=True)
    beneficiary = models.ForeignKey('crm.Beneficiary', related_name='employees', on_delete=models.SET_NULL, null=True)
    agency = models.ForeignKey('crm.Agency', related_name='employees', on_delete=models.SET_NULL, null=True)
    consultant = models.ForeignKey('crm.Consultant', related_name='employees', on_delete=models.SET_NULL, null=True)
    projects = models.ManyToManyField('projects.Projects', blank=True)

    class Meta:
        verbose_name = _('employee')
        verbose_name_plural = _('employees')

    def __str__(self):
        return f'{self.firstname} {self.lastname}'


class Currency(BaseModel):
    name = models.CharField(_('Currency name'), max_length=255, blank=False)
    symbol = models.CharField(_('Symbol'), max_length=10, blank=False)
    code = models.CharField(_('Currency code'), max_length=10, blank=True)

    class Meta:
        verbose_name = _('currency')
        verbose_name_plural = _('currencies')

    def __str__(self):
        return self.name


class UOM(BaseModel):
    name = models.CharField(_('Name of a unit of measurement'), max_length=255, blank=False)
    symbol = models.CharField(_('Symbol'), max_length=10, blank=False)

    class Meta:
        verbose_name = _('Unit of measurement')
        verbose_name_plural = _('Units of measurement')

    def __str__(self):
        return self.name


class IFIFP(BaseModel):
    name = models.CharField(_('Product name'), max_length=255, blank=False)
    amount = models.IntegerField(_('Amount'), blank=False)
    term = models.IntegerField(_('Term (in month)'), blank=False)
    loan_interest = models.FloatField(_('Loan interest'), blank=False)
    grace_period = models.IntegerField(_('Grace period (in months)'), blank=False)
    requirement = models.TextField(_('Requirement'), blank=False)

    class Meta:
        verbose_name = _('IFI financial product')
        verbose_name_plural = _('IFI financial products')

    def __str__(self):
        return self.name


class SER(BaseModel):
    CATEGORY = (
        ('GGE', _('Glavgosexpertiza')),
        ('IFI', _('IFI')),
    )

    name = models.CharField(_('Requirement name'), max_length=255, blank=False)
    category = models.CharField(_('Category'), max_length=50, choices=CATEGORY, default='GGE')
    description = models.CharField(_('Description'), max_length=255, blank=True)

    class Meta:
        verbose_name = _('Subprojects environmental requirement')
        verbose_name_plural = _('Subprojects environmental requirements')

    def __str__(self):
        return self.name


class Sector(BaseModel):
    name = models.CharField(_('Sector name'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('Sector ')
        verbose_name_plural = _('Sectors')

    def __str__(self):
        return self.name

    # object = SectorManager()


class FOA(BaseModel):
    name = models.CharField(_('Field of activity'), max_length=255, blank=False)
    sector = models.ForeignKey(Sector, related_name='foas', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('Field of activity')
        verbose_name_plural = _('Fields of activity')

    def __str__(self):
        return self.name


class Indicator(BaseModel):
    LEVELS = (
        ('PI', 'Project indicators'),
        ('SI', 'Subproject indicators'),
    )
    TYPES = (
        ('FINANCIAL', _('Financial')),
        ('PRODUCTION', _('Production')),
        ('PROJECT', _('Project')),
    )
    PHASES = (
        ('ITI', _('Intermediate target indicators')),
        ('FTI', _('Final target indicators')),
    )

    FREQUENCIES = (
        ('MONTHLY', _('Monthly')),
        ('YEARLY', _('Yearly')),
        ('CUSTOM', _('Custom')),
    )

    name = models.CharField(_('Indicator name'), max_length=255, blank=False)
    level = models.CharField(_('Level'), choices=LEVELS, max_length=20, default='PI')
    type = models.CharField(_('Indicator type'), choices=TYPES, max_length=20, default='FINANCIAL')
    phase = models.CharField(_('Indicator phase'), choices=PHASES, max_length=10, default='ITI')
    description = models.CharField(_('Indicator brief description'), max_length=255, blank=False)
    uom = models.ForeignKey(UOM, verbose_name=_('Unit of measurement'), related_name='indicators', on_delete=models.SET_NULL, null=True)
    collection_methods = models.CharField(_('Data collection methods'), max_length=255)
    data_source = models.CharField(_('Data source'), max_length=255)
    frequency = models.CharField(_('Collection/Refresh frequency'), choices=FREQUENCIES, max_length=20, default='MONTHLY')
    responsible_person = models.CharField(_('Responsible person'), max_length=255)
    reporting_format = models.CharField(_('Reporting format'), max_length=255)
    control_tool = models.CharField(_('Quality control tool'), max_length=255)

    class Meta:
        verbose_name = _('Indicator')
        verbose_name_plural = _('Indicators')

    def __str__(self):
        return self.name


class ExchangeRate(BaseModel):
    code = models.CharField(_('Currency code'), max_length=10, blank=True)
    currency = models.ForeignKey(Currency, related_name='rates', on_delete=models.SET_NULL, null=True)
    nominal = models.IntegerField(_('Nominal'), null=True)
    rate = models.FloatField(_('Currency rate'), null=True)
    diff = models.FloatField(_('Different'), null=True)
    date = models.DateField(_('Date'), null=True)
    rate_updated_at = models.DateField(_('Rate updated date'), null=True)
    symbol = models.CharField(_('Currency symbol'), max_length=10, null=True)

    class Meta:
        verbose_name = _('Exchange rate')
        verbose_name_plural = _('Exchange rates')

    def __str__(self):
        return self.code


class EntityType(BaseModel):
    name = models.CharField(_('Entity type'), max_length=255, blank=False)

    class Meta:
        verbose_name = _('Entity type')
        verbose_name_plural = _('Entity types')

    def __str__(self):
        return self.name
