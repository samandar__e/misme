from datetime import datetime

import requests

from directory.models import ExchangeRate, Currency
from misme import settings


def get_exchange_rates(date):
    route = f"{settings.CBU_EX_RATES_URL}all/{date}/"
    try:
        r = requests.get(route, timeout=5)
        rates = r.json()
        save_rates(rates, parse_date=date)
    except requests.exceptions.Timeout:
        print("Timeout is except")
    except requests.exceptions.TooManyRedirects:
        print("Many redirects")
    except requests.exceptions.RequestException as e:
        print(e)


def save_rates(rates, parse_date):
    currencies = Currency.objects.all()
    for currency in currencies:
        rate = next((item for item in rates if item["Ccy"] == currency.symbol), None)
        if rate is not None:
            instance, created = ExchangeRate.objects.update_or_create(
                code=rate.get("Code", 0),
                symbol=rate.get("Ccy", ""),
                currency=currency,
                nominal=rate.get("Nominal", 1),
                rate=rate.get("Rate", 0.0) or 0.0,
                diff=rate.get("Diff", 0.0) or 0.0,
                date=parse_date,
                rate_updated_at=datetime.strptime(rate.get("Date", parse_date), '%d.%m.%Y'),
                )
            print(instance.id)
