from modeltranslation.translator import register, TranslationOptions
from .models import Region, District, Department, Position, Currency, Country, Sector, FOA, EntityType


@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Region)
class RegionTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(District)
class DistrictTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Department)
class DepartmentTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Position)
class PositionTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Currency)
class CurrencyTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Sector)
class SectorTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(FOA)
class FOATranslationOptions(TranslationOptions):
    fields = ('name',)


@register(EntityType)
class EntityTypeTranslationOptions(TranslationOptions):
    fields = ('name',)
