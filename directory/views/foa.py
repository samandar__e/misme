from rest_framework import status, filters
from rest_framework.exceptions import APIException
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import FOA
from directory.serializers import FOASerializer, FOAListSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class FOAView(ListCreateAPIView):
    serializer_class = FOASerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'sector')
    ordering = ['pk']

    def get_queryset(self):
        queryset = FOA.objects.all()
        sector_ids = self.request.GET.get('sector')
        if sector_ids is not None:
            try:
                sectors = [int(id) for id in sector_ids.split(',')]
                return queryset.filter(sector__in=sectors)
            except ValueError:
                raise APIException("ID is not number")
        return queryset

    def get(self, request, *args, **kwargs):
        self.serializer_class = FOAListSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class FOADetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = FOASerializer

    def get(self, request, pk):
        instance = get_object_or_404(FOA, id=pk)
        serializer = FOASerializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(FOA, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(FOA, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
