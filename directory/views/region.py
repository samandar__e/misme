from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from directory.models import Region, District
from directory.serializers import RegionSerializer, RegionDetailSerializer, GeoJsonObjectSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class RegionView(ListCreateAPIView):
    serializer_class = RegionSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('code', 'name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        return Region.objects.all()

    def post(self, request):
        serializer = RegionDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class RegionDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = RegionDetailSerializer

    def get(self, request, pk):
        region = get_object_or_404(Region, id=pk)
        serializer = RegionSerializer(region)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        region = get_object_or_404(Region, id=pk)
        serializer = self.serializer_class(region, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        region = get_object_or_404(Region, id=pk)
        region.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class RegionGeoJsonView(APIView):

    def get(self, request, pk):
        region = get_object_or_404(Region, id=pk)
        serializer = GeoJsonObjectSerializer(region.geo_json)
        return Response(serializer.data, status.HTTP_200_OK)


class RegionGeoJsonListView(APIView):

    def get(self, request):
        regions = Region.objects.order_by('id').all()
        geo_jsons = []
        for region in regions:
            if region.geo_json:
                geo_jsons.append({"id": region.id, "name": region.name, "geo_json": region.geo_json.geo_json})
        serializer = GeoJsonObjectSerializer(geo_jsons, many=True)
        return Response(serializer.data, status.HTTP_200_OK)


class RegionDistrictsGeoJsonView(APIView):

    def get(self, request, pk):
        region = get_object_or_404(Region, id=pk)
        districts = District.objects.filter(region=region)
        geo_jsons = []
        for district in districts:
            if district.geo_json:
                geo_jsons.append({"id": district.id, "name": district.name, "geo_json": district.geo_json.geo_json})

        serializer = GeoJsonObjectSerializer(geo_jsons, many=True)
        return Response(serializer.data, 200)
