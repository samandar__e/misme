from rest_framework.response import Response
from rest_framework.views import APIView

from directory.models import Employee, SER, Indicator
from restapp.utils.enumserialize import enum_serialize


class MembershipGroupsList(APIView):

    def get(self, request):
        return Response(enum_serialize(Employee.MEMBERSHIP_GROUP))


class SERCategoriesList(APIView):

    def get(self, request):
        return Response(enum_serialize(SER.CATEGORY))


class IndicatorLevelsList(APIView):

    def get(self, request):
        return Response(enum_serialize(Indicator.LEVELS))


class IndicatorTypesList(APIView):

    def get(self, request):
        return Response(enum_serialize(Indicator.TYPES))


class IndicatorPhasesList(APIView):

    def get(self, request):
        return Response(enum_serialize(Indicator.PHASES))


class IndicatorFrequenciesList(APIView):

    def get(self, request):
        return Response(enum_serialize(Indicator.FREQUENCIES))
