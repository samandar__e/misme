from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import Employee
from directory.serializers import EmployeeSerializer, EmployeeListSerializer, EmployeeDetailSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class EmployeeView(ListCreateAPIView):
    serializer_class = EmployeeSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('firstname',)
    ordering = ['pk']

    def get_queryset(self):
        return Employee.objects.all()

    def get(self, request, *args, **kwargs):
        self.serializer_class = EmployeeListSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class EmployeeDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = EmployeeDetailSerializer

    def get_queryset(self):
        return Employee.objects.all()

    def get(self, request, pk):
        instance = get_object_or_404(Employee, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Employee, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Employee, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
