from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.exceptions import APIException
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from directory.filterset import DistrictFilter
from directory.models import District
from directory.serializers import DistrictListSerializer, DistrictDetailSerializer, GeoJsonObjectSerializer

from restapp.pagination import ResultsSetPagination


class DistrictView(ListCreateAPIView):
    serializer_class = DistrictListSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = DistrictFilter
    search_fields = ('name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        queryset = District.objects.all()
        region_ids = self.request.GET.get('region')
        if region_ids is not None:
            try:
                regions = [int(id) for id in region_ids.split(',')]
                queryset = queryset.filter(region__in=regions)
            except ValueError:
                raise APIException("ID is not number")
        return queryset

    def post(self, request):
        serializer = DistrictDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class DistrictDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = DistrictDetailSerializer

    def get_queryset(self):
        return District.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        district = get_object_or_404(District, id=pk)
        serializer = self.serializer_class(district)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        district = get_object_or_404(District, id=pk)
        serializer = self.serializer_class(district, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class DistrictGeoJsonView(APIView):

    def get(self, request, pk):
        district = get_object_or_404(District, id=pk)
        serializer = GeoJsonObjectSerializer(district.geo_json)
        return Response(serializer.data, status.HTTP_200_OK)
