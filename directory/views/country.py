from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import Country
from directory.serializers import CountrySerializer, CountryDetailSerializer

from restapp.pagination import ResultsSetPagination


class CountryView(ListCreateAPIView):
    serializer_class = CountrySerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        return Country.objects.all()

    def post(self, request):
        serializer = CountryDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class CountryDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = CountryDetailSerializer

    def get_queryset(self):
        return Country.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        country = get_object_or_404(Country, id=pk)
        serializer = self.serializer_class(country)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        country = get_object_or_404(Country, id=pk)
        serializer = self.serializer_class(country, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
