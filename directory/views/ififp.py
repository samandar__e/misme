from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import IFIFP
from directory.serializers import IFIFPSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class IFIFPView(ListCreateAPIView):
    serializer_class = IFIFPSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'amount', 'term', 'loan_interest', 'grace_period')
    ordering = ['pk']

    def get_queryset(self):
        return IFIFP.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class IFIFPDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = IFIFPSerializer

    def get(self, request, pk):
        instance = get_object_or_404(IFIFP, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(IFIFP, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(IFIFP, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
