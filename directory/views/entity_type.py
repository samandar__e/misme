from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import EntityType
from directory.serializers import EntityTypeSerializer, EntityTypeDetailSerializer, RelatedEntityTypeSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class EntityTypeView(ListCreateAPIView):
    serializer_class = EntityTypeSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        return EntityType.objects.all()

    def post(self, request):
        serializer = EntityTypeDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class EntityTypeDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = EntityTypeDetailSerializer

    def get(self, request, pk):
        entity_type = get_object_or_404(EntityType, id=pk)
        serializer = self.serializer_class(entity_type)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        entity_type = get_object_or_404(EntityType, id=pk)
        serializer = self.serializer_class(entity_type, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        entity_type = get_object_or_404(EntityType, id=pk)
        entity_type.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)