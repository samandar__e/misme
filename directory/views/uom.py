from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import UOM
from directory.serializers import UOMSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class UOMView(ListCreateAPIView):
    serializer_class = UOMSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name',)
    ordering = ['pk']

    def get_queryset(self):
        return UOM.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class UOMDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = UOMSerializer

    def get(self, request, pk):
        unit = get_object_or_404(UOM, id=pk)
        serializer = self.serializer_class(unit)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        unit = get_object_or_404(UOM, id=pk)
        serializer = self.serializer_class(unit, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        unit = get_object_or_404(UOM, id=pk)
        unit.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
