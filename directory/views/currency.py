from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import Currency
from directory.serializers import CurrencySerializer, CurrencyDetailSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class CurrencyView(ListCreateAPIView):
    serializer_class = CurrencySerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        return Currency.objects.all()

    def post(self, request):
        serializer = CurrencyDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class CurrencyDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = CurrencyDetailSerializer

    def get(self, request, pk):
        currency = get_object_or_404(Currency, id=pk)
        serializer = CurrencySerializer(currency)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        currency = get_object_or_404(Currency, id=pk)
        serializer = self.serializer_class(currency, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        currency = get_object_or_404(Currency, id=pk)
        currency.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
