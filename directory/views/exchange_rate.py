from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.generics import ListAPIView

from directory.filterset import ExchangeRateFilter
from directory.models import ExchangeRate
from directory.serializers import ExchangeRateSerializer

from restapp.pagination import ResultsSetPagination


class ExchangeRatesView(ListAPIView):
    serializer_class = ExchangeRateSerializer

    pagination_class = ResultsSetPagination
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ExchangeRateFilter
    ordering = ['-pk']

    def get_queryset(self):
        return ExchangeRate.objects.all()
