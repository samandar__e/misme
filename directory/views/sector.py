from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import Sector
from directory.serializers import SectorListSerializer, SectorDetailSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class SectorView(ListCreateAPIView):
    serializer_class = SectorListSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name',)
    ordering = ['pk']

    def get_queryset(self):
        return Sector.objects.all()

    def post(self, request):
        serializer = SectorDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class SectorDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = SectorDetailSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Sector, id=pk)
        serializer = SectorListSerializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Sector, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Sector, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
