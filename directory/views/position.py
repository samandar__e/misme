from rest_framework import filters, status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from directory.models import Position
from directory.serializers import PositionSerializer, PositionDetailSerializer

from restapp.pagination import ResultsSetPagination


class PositionView(ListCreateAPIView):
    serializer_class = PositionSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name_ru', 'name_en', 'name_uz')
    ordering = ['pk']

    def get_queryset(self):
        return Position.objects.all()

    def post(self, request, **kwargs):
        serializer = PositionDetailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class PositionDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = PositionDetailSerializer

    def get_queryset(self):
        return Position.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        region = get_object_or_404(Position, id=pk)
        serializer = PositionSerializer(region)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        position = get_object_or_404(Position, id=pk)
        serializer = self.serializer_class(position, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
