from django.apps import AppConfig


class SubprojectConfig(AppConfig):
    name = 'subproject'

    def ready(self):
        import subproject.signals