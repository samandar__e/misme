from django_filters.rest_framework import FilterSet

from subproject.models import Subproject


class SubprojectFilter(FilterSet):

    class Meta:
        model = Subproject
        fields = {
            'id': ['exact'],
            'name': ['exact', 'startswith', 'contains'],
            'project': ['exact'],
            'component': ['exact'],
            'beneficiary': ['exact'],
            'beneficiary__taxId': ['exact', 'startswith'],
            'total_cost': ['lt', 'gt'],
            'total_cost_currency': ['exact'],
            'loan_amount': ['lt', 'gt'],
            'loan_currency': ['exact'],
            'beneficiary_amount': ['lt', 'gt'],
            'beneficiary_amount_currency': ['exact'],
            'cofinanced_pfi': ['lt', 'gt'],
            'cofinanced_pfi_currency': ['exact'],
            'production_capacity': ['lt', 'gt'],
            'production_capacity_unit': ['exact'],
            'start_activity': ['lt', 'gt'],
            'start_financing': ['lt', 'gt'],
            'sectors': ['exact'],
            'field_of_activities': ['exact'],
            'environment_requirements': ['exact'],
            'region': ['exact'],
            'district': ['exact'],
            'goods_imported_countries': ['exact'],
            'pfi': ['exact'],
            'overall_jobs': ['lt', 'gt'],
            'men': ['lt', 'gt'],
            'women': ['lt', 'gt'],
            'revolver_means': ['exact'],
            'status': ['exact'],
        }
