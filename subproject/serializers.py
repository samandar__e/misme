from rest_framework import serializers

from components.models import Component
from crm.models import ParticipatingFinancialInstitution
from crm.serializers import RelatedBeneficiaryForListSerializer
from directory.serializers import RelatedRegionSerializer, RelatedDistrictSerializer, SectorSerializer, \
    RelatedFOASerializer, UOMSerializer, CountrySerializer, SERSerializer
from projects.models import Projects
from .models import Subproject, SubprojectChronology, Repayment, Visit, SubprojectKPI


class TotalCostField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.total_cost_currency.id if instance.total_cost_currency else None
        return {"number": instance.total_cost, "currency": currency}

    def to_internal_value(self, data):
        return {"total_cost": data["number"], "total_cost_currency_id": data["currency"]}


class LoanAmountField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.loan_currency.id if instance.loan_currency else None
        return {"number": instance.loan_amount, "currency": currency}

    def to_internal_value(self, data):
        return {"loan_amount": data["number"], "loan_currency_id": data["currency"]}


class BeneficiaryAmountField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.beneficiary_amount_currency.id if instance.beneficiary_amount_currency else None
        return {"number": instance.beneficiary_amount, "currency": currency}

    def to_internal_value(self, data):
        return {"beneficiary_amount": data["number"], "beneficiary_amount_currency_id": data["currency"]}


class CofinancedPfiField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.cofinanced_pfi_currency.id if instance.cofinanced_pfi_currency else None
        return {"number": instance.cofinanced_pfi, "currency": currency}

    def to_internal_value(self, data):
        return {"cofinanced_pfi": data["number"], "cofinanced_pfi_currency_id": data["currency"]}


class UOMField(serializers.Field):
    def to_representation(self, instance):
        uom = instance.production_capacity_unit.id if instance.production_capacity_unit else None
        return uom

    def to_internal_value(self, data):
        return {"production_capacity_unit_id": data}


class PFIDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = ('id', 'name')


class ProjectDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'name')


class ComponentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'name')


class SubprojectListSerializer(serializers.ModelSerializer):
    beneficiary = RelatedBeneficiaryForListSerializer()
    region = RelatedRegionSerializer()
    district = RelatedDistrictSerializer()
    total_cost = TotalCostField(source='*', required=False)
    loan_amount = LoanAmountField(source='*', required=False)
    beneficiary_amount = BeneficiaryAmountField(source='*', required=False)
    cofinanced_pfi = CofinancedPfiField(source='*', required=False)
    uom = UOMField(source='*', required=False, allow_null=True)
    overall = serializers.IntegerField(source='overall_jobs', required=False, allow_null=True)

    pfi = PFIDetailSerializer(read_only=True)
    project = ProjectDetailSerializer(read_only=True)
    sectors = SectorSerializer(read_only=True, many=True)
    field_of_activities = RelatedFOASerializer(read_only=True, many=True)
    production_capacity_unit = UOMSerializer(read_only=True)
    goods_imported_countries = CountrySerializer(read_only=True, many=True)
    environment_requirements = SERSerializer(read_only=True, many=True)
    component = ComponentDetailSerializer(read_only=True)

    class Meta:
        model = Subproject
        fields = ('id', 'name', 'status', 'beneficiary',  'pfi',
                  'total_cost', 'loan_amount',  'beneficiary_amount', 'cofinanced_pfi',
                  'sectors', 'start_activity', 'start_financing', 'environment_requirements',
                  'field_of_activities', 'production_capacity', 'production_capacity_unit', 'uom',
                  'region', 'district', 'goods_imported_countries',
                  'overall', 'overall_jobs', 'men', 'women', 'project', 'revolver_means', 'component', 'lat', 'lng')


class SubprojectCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subproject
        fields = ('id', 'project', 'component', 'name', 'beneficiary')


class SubprojectChronologySerializer(serializers.ModelSerializer):

    class Meta:
        model = SubprojectChronology
        fields = ('issued_from_bank', 'received_by_agency', 'returned_to_pfi', 'returned_to_agency',
                  'approved_by_agency', 'sent_by_ifi', 'received_by_ifi', 'date_financed', 'funds_transferred')


class SubprojectDetailSerializer(serializers.ModelSerializer):
    total_cost = TotalCostField(source='*', required=False)
    loan_amount = LoanAmountField(source='*', required=False)
    beneficiary_amount = BeneficiaryAmountField(source='*', required=False)
    cofinanced_pfi = CofinancedPfiField(source='*', required=False)
    created_by = serializers.SerializerMethodField(method_name='get_created_user_name', read_only=True)
    created_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    name = serializers.CharField(read_only=True)
    chronology = SubprojectChronologySerializer(read_only=True)
    uom = UOMField(source='*', required=False, allow_null=True)
    overall = serializers.IntegerField(source='overall_jobs', required=False, allow_null=True)

    class Meta:
        model = Subproject
        fields = ('id', 'name', 'status', 'beneficiary', 'total_cost', 'loan_amount', 'beneficiary_amount',
                  'cofinanced_pfi', 'production_capacity', 'uom', 'start_activity', 'start_financing', 'sectors',
                  'field_of_activities', 'environment_requirements', 'pfi', 'goods_imported_countries', 'region',
                  'district', 'overall', 'men', 'women', 'project', 'revolver_means', 'component', 'created_by',
                  'created_time', 'chronology', 'lat', 'lng', 'supplier', 'government')

    def get_created_user_name(self, instance):
        return instance.created_by.username if instance.created_by else None


class SubprojectInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subproject
        fields = ('id', 'name')


class RepaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Repayment
        fields = ('id', 'date_of_report', 'principal_paid', 'principal_overdue_less_then30',
                  'principal_overdue_from30to90', 'principal_overdue_from30to90', 'principal_overdue_more_then90',
                  'interest_paid', 'interest_overdue_less_then30', 'interest_overdue_from30to90',
                  'interest_overdue_more_then90', 'subproject')


class SubprojectVisitSerializer(serializers.ModelSerializer):
    status = serializers.CharField(read_only=True)

    class Meta:
        model = Visit
        fields = ('id', 'purpose_of_visit', 'date_of_visit', 'status', 'subproject')


class SubprojectVisitDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Visit
        fields = ('id', 'purpose_of_visit', 'date_of_visit', 'status', 'subproject', 'participants_of_visit',
                  'signed_contracts', 'environmental_compliance', 'certifications', 'targeted_use_of_funds')


class SubprojectKPISerializer(serializers.ModelSerializer):

    class Meta:
        model = SubprojectKPI
        fields = ('id', 'year', 'data_type', 'production_quantity', 'production_value', 'export_quantity',
                  'export_value', 'expenses', 'net_profit', 'roi', 'subproject')
