from django.db import models
from django.db.models import QuerySet


class SubprojectManager(models.Manager):

    def get_by_region_overall(self, start_date, end_date, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(start_activity__gte=start_date) if start_date else queryset
        queryset = queryset.filter(start_activity__lte=end_date) if end_date else queryset
        queryset = queryset.filter(region=region) if region else queryset
        queryset = queryset.filter(district=district) if district else queryset
        return queryset

    def get_by_region_overall_by_year(self, year, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(start_activity__year=year) if year else queryset
        queryset = queryset.filter(region=region) if region else queryset
        queryset = queryset.filter(district=district) if district else queryset
        return queryset
