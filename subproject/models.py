from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from components.models import Component
from crm.models import Beneficiary, ParticipatingFinancialInstitution
from directory.models import UOM, Sector, FOA, Region, District, Country, Currency, SER
from projects.models import Projects
from restapp.models import BaseModel, Attachment
from restapp.utils.time_helper import current_year
from subproject.managers.subproject import SubprojectManager


class Subproject(BaseModel):
    STATUS = (
        ('NEW', _('New')),
        ('APPROVED', _('Approved')),
        ('IN_PROGRESS', _('In progress')),
        ('COMPLETED', _('Completed')),
        ('DENIED', _('Denied')),
        ('BAD', _('Bad')),
    )

    name = models.CharField(_('Subproject name'), max_length=255, blank=True, null=True)
    project = models.ForeignKey(Projects, related_name='subprojects', on_delete=models.SET_NULL, null=True)
    component = models.ForeignKey(Component, related_name='subprojects', on_delete=models.SET_NULL, null=True)
    beneficiary = models.ForeignKey(Beneficiary, related_name='subprojects', on_delete=models.SET_NULL, null=True, blank=True)
    total_cost = models.FloatField(_('Total cost'), null=True, blank=True)
    total_cost_currency = models.ForeignKey(Currency, related_name='subproject_totals', on_delete=models.SET_NULL,
                                            null=True, blank=True)
    loan_amount = models.FloatField(_('Loan amount'), null=True, blank=True)
    loan_currency = models.ForeignKey(Currency, related_name='subproject_loans', on_delete=models.SET_NULL, null=True,
                                      blank=True)
    beneficiary_amount = models.FloatField(_('Beneficiary amount'), null=True, blank=True)
    beneficiary_amount_currency = models.ForeignKey(Currency, related_name='subproject_beneficiary',
                                                    on_delete=models.SET_NULL, null=True, blank=True)
    cofinanced_pfi = models.FloatField(_('Cofinanced by PFI'), null=True, blank=True)
    cofinanced_pfi_currency = models.ForeignKey(Currency, related_name='cofinanced_pfi', on_delete=models.SET_NULL,
                                                null=True, blank=True)
    production_capacity = models.FloatField(_('Production capacity'), null=True, blank=True)
    production_capacity_unit = models.ForeignKey(UOM, on_delete=models.SET_NULL, null=True, blank=True)
    start_activity = models.DateField(_('Start of activity'), null=True, blank=True)
    start_financing = models.DateField(_('Start of financing'), null=True, blank=True)
    sectors = models.ManyToManyField(Sector, blank=True)
    field_of_activities = models.ManyToManyField(FOA, blank=True)
    environment_requirements = models.ManyToManyField(SER, blank=True)
    region = models.ForeignKey(Region, related_name='subprojects', on_delete=models.SET_NULL, null=True, blank=True)
    district = models.ForeignKey(District, related_name='subprojects', on_delete=models.SET_NULL, null=True, blank=True)
    goods_imported_countries = models.ManyToManyField(Country, blank=True)
    pfi = models.ManyToManyField(ParticipatingFinancialInstitution, blank=True)
    # pfi = models.ForeignKey(ParticipatingFinancialInstitution, related_name='subprojects', on_delete=models.SET_NULL,
    #                         null=True, blank=True)
    overall_jobs = models.IntegerField(_('Overall'), null=True, blank=True, default=0)
    men = models.IntegerField(_('Men'), null=True, blank=True, default=0)
    women = models.IntegerField(_('Women'), null=True, blank=True, default=0)
    revolver_means = models.BooleanField(_('Revolver means'), default=True)
    status = models.CharField(max_length=50, choices=STATUS, default='NEW', verbose_name=_('Status'))
    attachments = models.ManyToManyField(Attachment, blank=True)
    lat = models.FloatField(_('Latitude'), null=True, blank=True)
    lng = models.FloatField(_('Longitude'), null=True, blank=True)
    supplier = models.CharField(_('Supplier name'), max_length=255, blank=True, null=True)
    government = models.CharField(_('Government'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('Subproject')
        verbose_name_plural = _('Subprojects')

    def __str__(self):
        return self.name

    objects = SubprojectManager()


class SubprojectChronology(BaseModel):
    issued_from_bank = models.DateField(_('Issued from bank'), null=True, blank=True)
    received_by_agency = models.DateField(_('Received by agency'), null=True, blank=True)
    returned_to_pfi = models.TextField(_('Returned to pfi for correction'), null=True, blank=True)
    returned_to_agency = models.TextField(_('Returned to agency for correction'), null=True, blank=True)
    approved_by_agency = models.DateField(_('Approved by agency'), null=True, blank=True)
    sent_by_ifi = models.TextField(_('Sent by IFI'), null=True, blank=True)
    received_by_ifi = models.TextField(_('Received by IFI'), null=True, blank=True)
    date_financed = models.DateField(_('Date financed'), null=True, blank=True)
    funds_transferred = models.DateField(_('Funds transferred to beneficiary'), null=True, blank=True)
    subproject = models.OneToOneField(Subproject, related_name='chronology', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('Subproject chronology')
        verbose_name_plural = _('Subproject chronologies')


class Repayment(BaseModel):
    date_of_report = models.DateField(_('Date of report'), null=True, blank=True)
    principal_paid = models.FloatField(_('Principal paid'), null=True, blank=True)
    principal_overdue_less_then30 = models.FloatField(_('Principal overdue less then 30 days'), null=True, blank=True)
    principal_overdue_from30to90 = models.FloatField(_('Principal overdue from 30 to 90 days'), null=True, blank=True)
    principal_overdue_more_then90 = models.FloatField(_('Principal overdue more then 90 days'), null=True, blank=True)
    interest_paid = models.FloatField(_('Interest paid'), null=True, blank=True)
    interest_overdue_less_then30 = models.FloatField(_('Interest overdue less then 30 days'), null=True, blank=True)
    interest_overdue_from30to90 = models.FloatField(_('Interest overdue from 30 to 90 days'), null=True, blank=True)
    interest_overdue_more_then90 = models.FloatField(_('Interest overdue more then 90 days'), null=True, blank=True)
    subproject = models.ForeignKey(Subproject, related_name='repayments', on_delete=models.SET_NULL, null=True,
                                   blank=True)

    class Meta:
        verbose_name = _('Loan repayment')
        verbose_name_plural = _('Loan repayments')


class Visit(BaseModel):

    STATUS = (
        ('SCHEDULED', _('Scheduled')),
        ('CONDUCTED', _('Conducted')),
    )

    purpose_of_visit = models.CharField(_('Purpose of visit'), max_length=255, blank=True)
    date_of_visit = models.DateField(_('Date of visit'), null=True, blank=True)
    status = models.CharField(max_length=50, choices=STATUS, default='SCHEDULED', verbose_name=_('Status'))
    participants_of_visit = models.TextField(_('Participants of visit'), null=True, blank=True)
    signed_contracts = models.TextField(_('Signed contracts'), null=True, blank=True)
    environmental_compliance = models.TextField(_('Environmental compliance'), null=True, blank=True)
    certifications = models.TextField(_('certifications'), null=True, blank=True)
    targeted_use_of_funds = models.TextField(_('Targeted use of funds'), null=True, blank=True)
    subproject = models.ForeignKey(Subproject, related_name='visits', on_delete=models.SET_NULL, null=True,
                                   blank=True)
    attachments = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('Field survey visit')
        verbose_name_plural = _('Field survey visits')


class SubprojectKPI(BaseModel):
    TYPES = (
        ('FORECAST', _('Forecast')),
        ('ACTUAL', _('Actual'))
    )

    year = models.IntegerField(_('Year'), validators=[MinValueValidator(2000), MaxValueValidator(current_year() + 1)])
    data_type = models.CharField(max_length=50, choices=TYPES, default='FORECAST', verbose_name=_('Data type'))
    production_quantity = models.FloatField(_('Production quantity'), null=True, blank=True)
    production_value = models.FloatField(_('Production value'), null=True, blank=True)
    export_quantity = models.FloatField(_('Export quantity'), null=True, blank=True)
    export_value = models.FloatField(_('Export value'), null=True, blank=True)
    expenses = models.FloatField(_('Expenses'), null=True, blank=True)
    net_profit = models.FloatField(_('Net profit'), null=True, blank=True)
    roi = models.FloatField(_('ROI'), null=True, blank=True)
    subproject = models.ForeignKey(Subproject, related_name='kpis', on_delete=models.SET_NULL, null=True,
                                   blank=True)

    class Meta:
        verbose_name = _('Key performance indicator')
        verbose_name_plural = _('Key performance indicators')
        unique_together = ('year', 'data_type', 'subproject',)
