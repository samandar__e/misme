from django.contrib import admin

# Register your models here.
from restapp.admin import BaseAdmin
from subproject.models import Subproject, SubprojectChronology


class SubprojectChronologyInline(admin.TabularInline):
    model = SubprojectChronology
    exclude = ('created_by', 'updated_by')


class SubprojectAdmin(BaseAdmin):
    inlines = (SubprojectChronologyInline, )
    exclude = ('created_by', 'updated_by')


admin.site.register(Subproject, SubprojectAdmin)
