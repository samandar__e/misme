from django.urls import path, re_path

from subproject.views.enums import StatusList, VisitStatusList, KPITypesList
from subproject.views.kpi import KPIView, KPIDetailView
from subproject.views.repayment import RepaymentView, RepaymentDetailView
from subproject.views.subproject import SubprojectView, SubprojectUpdateView, SubprojectRetrieveDestroyView
from subproject.views.chronology import ChronologyUpdate
from subproject.views.visit import VisitView, VisitDetailView

urlpatterns = [
    re_path(r'^subproject/$', SubprojectView.as_view(), name='subprojects_view'),
    path('subproject/<int:pk>', SubprojectRetrieveDestroyView.as_view(), name='subproject_detail'),
    path('subproject/<int:pk>/about', SubprojectUpdateView.as_view(), name='subproject_update'),
    path('subproject/<int:pk>/chronology', ChronologyUpdate.as_view(), name='chronology_update'),

    re_path(r'^subproject-repayment/$', RepaymentView.as_view(), name='subproject_repayment'),
    path('subproject-repayment/<int:pk>', RepaymentDetailView.as_view(), name='subproject_repayment_detail'),

    re_path(r'^subproject-visit/$', VisitView.as_view(), name='subproject_visit'),
    path('subproject-visit/<int:pk>', VisitDetailView.as_view(), name='subproject_visit_detail'),

    re_path(r'^subproject-kpi/$', KPIView.as_view(), name='subproject_kpi'),
    path('subproject-kpi/<int:pk>', KPIDetailView.as_view(), name='subproject_kpi_detail'),

    re_path(r'^enum-subproject-statuses/$', StatusList.as_view(), name='subproject_statuses_view'),
    re_path(r'^enum-subproject-visit_statuses/$', VisitStatusList.as_view(), name='subproject_visit_statuses_view'),
    re_path(r'^enum-subproject-kpi_types/$', KPITypesList.as_view(), name='subproject_kpi_types_view'),
]
