from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from subproject.models import Subproject


def project_overall_update(instance):
    project = instance.project
    if project is None:
        return

    subprojects = project.subprojects.all().values('overall_jobs', 'men', 'women')
    overall = men = women = 0
    for item in subprojects:
        overall = overall + item['overall_jobs'] if item['overall_jobs'] is not None else 0
        men = men + item['men'] if item['men'] is not None else 0
        women = women + item['women'] if item['women'] is not None else 0

    project.overallCount = overall
    project.menCount = men
    project.womenCount = women
    project.save()


@receiver(post_save, sender=Subproject)
def pre_save_handler(sender, instance, **kwargs):
    project_overall_update(instance)


@receiver(pre_delete, sender=Subproject)
def post_delete_handler(sender, instance, **kwargs):
    project_overall_update(instance)
