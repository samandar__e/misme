from rest_framework.response import Response
from rest_framework.views import APIView

from restapp.utils.enumserialize import enum_serialize
from subproject.models import Subproject, Visit, SubprojectKPI


class StatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Subproject.STATUS))


class VisitStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Visit.STATUS))


class KPITypesList(APIView):

    def get(self, request):
        return Response(enum_serialize(SubprojectKPI.TYPES))
