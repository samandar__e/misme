from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from subproject.models import Repayment
from subproject.serializers import RepaymentSerializer


class RepaymentView(ListCreateAPIView):
    serializer_class = RepaymentSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = ('date_of_report',)
    filterset_fields = ['subproject']
    ordering = ['pk']

    def get_queryset(self):
        return Repayment.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class RepaymentDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = RepaymentSerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        repayment = get_object_or_404(Repayment, id=pk)
        serializer = self.serializer_class(repayment)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        repayment = get_object_or_404(Repayment, id=pk)
        serializer = self.serializer_class(repayment, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        repayment = get_object_or_404(Repayment, id=pk)
        repayment.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
