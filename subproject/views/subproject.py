from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import get_object_or_404, ListCreateAPIView, UpdateAPIView, RetrieveDestroyAPIView
from rest_framework.response import Response
from rest_framework import status, filters

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from subproject.filterset import SubprojectFilter
from subproject.models import Subproject, SubprojectChronology
from subproject.serializers import SubprojectListSerializer, SubprojectCreateSerializer, SubprojectDetailSerializer


class SubprojectView(ListCreateAPIView):
    serializer_class = SubprojectCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter)
    filterset_class = SubprojectFilter
    search_fields = ('name',)
    ordering_fields = [
        'pk', 'name', 'project__name', 'component__name', 'beneficiary__name', 'beneficiary__taxId', 'total_cost',
        'total_cost_currency', 'loan_amount', 'loan_currency', 'beneficiary_amount', 'beneficiary_amount_currency',
        'cofinanced_pfi', 'cofinanced_pfi_currency', 'production_capacity', 'production_capacity_unit',
        'start_activity', 'start_financing', 'sectors__name', 'field_of_activities', 'environment_requirements',
        'region__name', 'district__name', 'goods_imported_countries__name', 'pfi__name', 'overall_jobs', 'men', 'women',
        'revolver_means', 'status'
    ]

    def get_queryset(self):
        return Subproject.objects.all()

    def get(self, request, *args, **kwargs):
        self.serializer_class = SubprojectListSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        subproject = serializer.save(created_by=self.request.user)

        # create empty subproject chronology
        SubprojectChronology.objects.create(subproject=subproject, created_by=self.request.user)

        return Response(serializer.data, status.HTTP_201_CREATED)


class SubprojectRetrieveDestroyView(RetrieveDestroyAPIView):
    serializer_class = SubprojectDetailSerializer

    def get_queryset(self):
        return Subproject.objects.all()

    def get(self, request, pk):
        instance = get_object_or_404(Subproject, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        instance = get_object_or_404(Subproject, id=pk)
        if hasattr(instance, 'chronology'):
            instance.chronology.delete()
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class SubprojectUpdateView(UpdateAPIView):
    serializer_class = SubprojectDetailSerializer
    http_method_names = ['put']

    def get_queryset(self):
        return Subproject.objects.all()

    def update(self, request, pk):
        instance = get_object_or_404(Subproject, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
