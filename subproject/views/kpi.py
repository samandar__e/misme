from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from subproject.models import SubprojectKPI
from subproject.serializers import SubprojectKPISerializer


class KPIView(ListCreateAPIView):
    serializer_class = SubprojectKPISerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['subproject']
    ordering = ['pk']

    def get_queryset(self):
        return SubprojectKPI.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class KPIDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = SubprojectKPISerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        kpi = get_object_or_404(SubprojectKPI, id=pk)
        serializer = self.serializer_class(kpi)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        kpi = get_object_or_404(SubprojectKPI, id=pk)
        serializer = self.serializer_class(kpi, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        kpi = get_object_or_404(SubprojectKPI, id=pk)
        kpi.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
