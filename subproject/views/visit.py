from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from subproject.models import Visit
from subproject.serializers import SubprojectVisitSerializer, SubprojectVisitDetailSerializer


class VisitView(ListCreateAPIView):
    serializer_class = SubprojectVisitSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    search_fields = ('date_of_report',)
    filterset_fields = ['subproject']
    ordering = ['pk']

    def get_queryset(self):
        return Visit.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class VisitDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = SubprojectVisitDetailSerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        visit = get_object_or_404(Visit, id=pk)
        serializer = self.serializer_class(visit)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        visit = get_object_or_404(Visit, id=pk)
        serializer = self.serializer_class(visit, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        visit = get_object_or_404(Visit, id=pk)
        visit.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
