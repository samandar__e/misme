from rest_framework.generics import get_object_or_404, UpdateAPIView
from rest_framework.response import Response
from rest_framework import status

from subproject.models import SubprojectChronology
from subproject.serializers import SubprojectChronologySerializer


class ChronologyUpdate(UpdateAPIView):
    serializer_class = SubprojectChronologySerializer
    http_method_names = ['put']

    def update(self, request, pk):
        instance = get_object_or_404(SubprojectChronology, subproject=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)



