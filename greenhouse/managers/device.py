from django.db import models, connection

from restapp.utils.db_helper import DBHelper


class DeviceManager(models.Manager):

    def get_control_panel_data(self, device):
        rows = []
        try:
            with connection.cursor() as cursor:
                query = """
                    select di.id, di.name, di.code_name,
                       (select dih.value from greenhouse_deviceindicatehistory dih
                       where dih.device_id = %s and dih.device_indicator_id=di.id order by id desc limit 1)
                    from greenhouse_deviceindicator di;
                """
                cursor.execute(query, device)
                rows = DBHelper.dictfetchall(cursor)
        except:
            print("Somethins error")
        return rows
