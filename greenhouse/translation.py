from modeltranslation.translator import register, TranslationOptions

from greenhouse.models import Greenhouse


@register(Greenhouse)
class GreenhouseTranslationOptions(TranslationOptions):
    fields = ('name',)
