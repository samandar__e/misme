from django.contrib import admin

from greenhouse.models import Greenhouse, MobileAppGreenhouse, TopicMessage, DeviceLog, Device, DeviceApp, \
    DeviceIndicator, DeviceIndicateHistory
from restapp.admin import BaseAdmin


@admin.register(TopicMessage)
class TopicMessageAdmin(BaseAdmin):
    list_display = ('id', 'topic', 'message', 'created_time')
    fields = ('topic', 'message')


@admin.register(Greenhouse)
class GreenhouseAdmin(admin.ModelAdmin):
    list_display = ('name', 'greenhouse_id')
    fields = ('name', 'greenhouse_id')
    search_fields = ('name', 'greenhouse_id')


@admin.register(MobileAppGreenhouse)
class MobileAppGreenhouseAdmin(admin.ModelAdmin):
    list_display = ('phone', 'verify_code')
    fields = ('phone', 'verify_code')
    search_fields = ('phone', 'verify_code')


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    list_display = ('name', 'greenhouse', 'control_panel_id', 'current_ip')
    fields = ('greenhouse', 'name', 'control_panel_id', 'current_ip')
    search_fields = ('name', 'control_panel_id', 'current_ip')


@admin.register(DeviceLog)
class DeviceLogAdmin(admin.ModelAdmin):
    list_display = ('message', 'last_ping')
    fields = ('message', 'last_ping')
    search_fields = ('message', 'last_ping')


@admin.register(DeviceApp)
class DeviceAppAdmin(admin.ModelAdmin):
    list_display = ('name', 'version')
    fields = ('name', 'version')
    search_fields = ('name', 'version')


@admin.register(DeviceIndicator)
class DeviceIndicatorAdmin(admin.ModelAdmin):
    list_display = ('id', 'device_app', 'name', 'code_name', 'address', 'current_state', 'left_float_point', 'type',
                    'topic', 'read_only', 'subscribe', 'publish', 'active')
    fields = ('device_app', 'name', 'code_name', 'address', 'current_state', 'left_float_point', 'type', 'topic',
              'read_only', 'subscribe', 'publish', 'active')
    search_fields = ('device_app', 'name', 'address', 'type')


@admin.register(DeviceIndicateHistory)
class DeviceIndicateHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'device', 'device_indicator', 'value', 'registered_at')

    def get_ordering(self, request):
        return ['-id']
