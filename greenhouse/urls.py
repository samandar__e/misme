from django.urls import re_path, path

from greenhouse.views.device_indicator_history import DeviceIndicateHistoryView, DeviceIndicateHistoryDetailView, \
    ControlPanelMonitorView
from greenhouse.views.device_log import DeviceLogView, DeviceLogDetailView
from greenhouse.views.device_message import DeviceMessageView
from greenhouse.views.enums import MobileAppGreenhouseList
from greenhouse.views.greenhouse import GreenhouseView, GreenhouseDetailView, UserGreenhouseListView
from greenhouse.views.device import DeviceView, DeviceDetailView
from greenhouse.views.greenhouse_user import GreenhouseUserView, GreenhouseUserDetailView
from greenhouse.views.mobile_app_greenhouse import MobileAppGreenhouseDetailView, MobileAppGreenhouseView

urlpatterns = [

    re_path(r'^greenhouse/$', GreenhouseView.as_view(), name='greenhouse_catalogue_view'),
    path('greenhouse/<int:pk>', GreenhouseDetailView.as_view(), name='greenhouse_catalogue_detail_view'),

    re_path(r'^mobile-app-greenhouse/$', MobileAppGreenhouseView.as_view(), name='mobile_app_greenhouse_view'),
    path('mobile-app-greenhouse/<int:pk>', MobileAppGreenhouseDetailView.as_view(), name='mobile_app_greenhouse_detail_view'),

    re_path(r'^device/$', DeviceView.as_view(), name='device_view'),
    path('device/<int:pk>', DeviceDetailView.as_view(), name='device_detail_view'),

    re_path(r'^device-log/$', DeviceLogView.as_view(), name='device_log_view'),
    path('device-log/<int:pk>', DeviceLogDetailView.as_view(), name='device_log_detail_view'),

    # enums urls
    re_path(r'^enum-mobile-app-greenhouse-status/$', MobileAppGreenhouseList.as_view(), name='mobile_app_greenhouse_status_view'),

    path('send-device-message', DeviceMessageView.as_view(), name='send_device_message'),

    re_path(r'^indicator-history/$', DeviceIndicateHistoryView.as_view(), name='indicator_history_view'),
    path('indicator-history/<int:pk>', DeviceIndicateHistoryDetailView.as_view(), name='indicator_history_detail_view'),

    path('control-panel-monitor', ControlPanelMonitorView.as_view(), name='control_panel_monitor'),

    re_path(r'^greenhouse-user/$', GreenhouseUserView.as_view(), name='greenhouse_users_view'),
    path('greenhouse-user/<int:pk>', GreenhouseUserDetailView.as_view(), name='greenhouse_users_detail_view'),

    re_path(r'^greenhouse-list/$', UserGreenhouseListView.as_view(), name='user_greenhouses_list_view'),
]
