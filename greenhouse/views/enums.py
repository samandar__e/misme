from rest_framework.response import Response
from rest_framework.views import APIView

from greenhouse.models import MobileAppGreenhouse
from restapp.utils.enumserialize import enum_serialize


class MobileAppGreenhouseList(APIView):

    def get(self, request):
        return Response(enum_serialize(MobileAppGreenhouse.STATUS))
