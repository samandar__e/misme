from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response

from greenhouse.filterset import MobileAppGreenhouseFilter
from greenhouse.models import MobileAppGreenhouse
from greenhouse.serializers import MobileAppGreenhouseSerializer
from restapp.pagination import ResultsSetPagination


class MobileAppGreenhouseView(ListCreateAPIView):
    serializer_class = MobileAppGreenhouseSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = MobileAppGreenhouseFilter
    filterset_fields = ['greenhouse']
    search_fields = ('phone', 'verify_code', 'status')
    ordering = ['-pk']

    def get_queryset(self):
        return MobileAppGreenhouse.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class MobileAppGreenhouseDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = MobileAppGreenhouseSerializer

    def get_queryset(self):
        return MobileAppGreenhouse.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        mobile_app_greenhouse = get_object_or_404(MobileAppGreenhouse, id=pk)
        serializer = self.serializer_class(mobile_app_greenhouse)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        mobile_app_greenhouse = get_object_or_404(MobileAppGreenhouse, id=pk)
        serializer = self.serializer_class(mobile_app_greenhouse, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
