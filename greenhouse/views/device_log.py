from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response

from greenhouse.models import DeviceLog
from greenhouse.serializers import DeviceLogSerializer
from restapp.pagination import ResultsSetPagination


class DeviceLogView(ListCreateAPIView):
    serializer_class = DeviceLogSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['device']
    search_fields = ('message', 'last_ping')
    ordering = ['-pk']

    def get_queryset(self):
        return DeviceLog.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class DeviceLogDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = DeviceLogSerializer

    def get_queryset(self):
        return DeviceLog.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        greenhouse_log = get_object_or_404(DeviceLog, id=pk)
        serializer = self.serializer_class(greenhouse_log)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        greenhouse_log = get_object_or_404(DeviceLog, id=pk)
        serializer = self.serializer_class(greenhouse_log, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
