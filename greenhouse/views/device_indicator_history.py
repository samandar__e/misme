from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import get_object_or_404, RetrieveAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from greenhouse.models import DeviceIndicateHistory, Device
from greenhouse.serializers import DeviceIndicateHistorySerializer
from restapp.pagination import ResultsSetPagination


class DeviceIndicateHistoryView(ListAPIView):
    serializer_class = DeviceIndicateHistorySerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['device']
    search_fields = ('device_indicator',)
    ordering = ['-pk']

    def get_queryset(self):
        return DeviceIndicateHistory.objects.all()


class DeviceIndicateHistoryDetailView(RetrieveAPIView):
    serializer_class = DeviceIndicateHistorySerializer

    def get(self, request, pk):
        instance = get_object_or_404(DeviceIndicateHistory, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ControlPanelMonitorView(APIView):

    def get(self, request):
        device = request.query_params.get('device')
        data = Device.objects.get_control_panel_data(device)
        return Response(data, status=status.HTTP_200_OK)
