from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response

from greenhouse.filterset import GreenhouseUserFilter
from greenhouse.serializers import GreenhouseUserSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from users.models import User


class GreenhouseUserView(ListCreateAPIView):
    serializer_class = GreenhouseUserSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = GreenhouseUserFilter
    search_fields = ('username',)
    ordering = ['-pk']

    def get_queryset(self):
        greenhouse_id = self.request.query_params.get('greenhouse')
        return User.objects.filter(greenhouse__in=[greenhouse_id])

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class GreenhouseUserDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = GreenhouseUserSerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        user = get_object_or_404(User, id=pk)
        serializer = self.serializer_class(user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        user = get_object_or_404(User, id=pk)
        serializer = self.serializer_class(user, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        visit = get_object_or_404(User, id=pk)
        visit.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
