import datetime

from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from greenhouse.models import Device, DeviceIndicator
from greenhouse.mqtt.handler import MessageHandler
from greenhouse.mqtt.pydantic import IoTPayload, IoTButtonMessage


class DeviceMessageView(APIView):

    """ {'id': 1, 'value': true } """
    def post(self, request):
        data = request.data
        date_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')
        r_data = IoTButtonMessage(**data)

        device_indicator = get_object_or_404(DeviceIndicator, id=r_data.id)
        topic = device_indicator.topic

        print(topic)

        """ example {value:[true],ts:date_time} """
        payload = IoTPayload(value=[r_data.value], ts=date_time)
        print(payload.json())
        MessageHandler.send_message_to_device(topic, payload=payload.json())
        return Response({"success": True, "msg": "Message sent to control panel"}, 200)
