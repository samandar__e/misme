from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404, ListAPIView
from rest_framework.response import Response

from greenhouse.filterset import GreenhouseFilter
from greenhouse.models import Greenhouse
from greenhouse.serializers import GreenhouseSerializer, GreenhouseDetailSerializer
from restapp.pagination import ResultsSetPagination


class GreenhouseView(ListCreateAPIView):
    serializer_class = GreenhouseSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = GreenhouseFilter
    search_fields = ('name', 'greenhouse_id', 'owner', 'address')
    ordering = ['-pk']

    def get_queryset(self):
        return Greenhouse.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class GreenhouseDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = GreenhouseSerializer

    def get_queryset(self):
        return Greenhouse.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        greenhouse = get_object_or_404(Greenhouse, id=pk)
        serializer = GreenhouseDetailSerializer(greenhouse)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        greenhouse = get_object_or_404(Greenhouse, id=pk)
        serializer = self.serializer_class(greenhouse, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class UserGreenhouseListView(ListAPIView):
    serializer_class = GreenhouseSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    ordering = ['name']

    def get_queryset(self):
        user = self.request.user
        return Greenhouse.objects.filter(users__in=[user.id])
