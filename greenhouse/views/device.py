from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, get_object_or_404
from rest_framework.response import Response

from greenhouse.filterset import DeviceFilter
from greenhouse.models import Device
from greenhouse.serializers import DeviceSerializer
from restapp.pagination import ResultsSetPagination


class DeviceView(ListCreateAPIView):
    serializer_class = DeviceSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = DeviceFilter
    filterset_fields = ['greenhouse']
    search_fields = ('name', 'control_panel_id', 'current_ip')
    ordering = ['-pk']

    def get_queryset(self):
        return Device.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class DeviceDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = DeviceSerializer

    def get_queryset(self):
        return Device.objects.all()

    def perform_update(self, serializer):
        serializer.save(updated_by=self.request.user)

    def get(self, request, pk):
        device = get_object_or_404(Device, id=pk)
        serializer = self.serializer_class(device)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        device = get_object_or_404(Device, id=pk)
        serializer = self.serializer_class(device, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
