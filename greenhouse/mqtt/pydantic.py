from typing import List, Any

from pydantic import BaseModel


class IoTPayload(BaseModel):
    value: List[Any]
    ts: str


class IoTIndicatorMessage(BaseModel):
    id: int
    value: str


class IoTButtonMessage(BaseModel):
    id: int
    value: bool
