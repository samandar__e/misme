import paho.mqtt.client as mqtt

from greenhouse.mqtt.handler import MessageHandler
from misme import settings

cache_value = None


def on_connect(cl, userdata, flags, rc):
    print("Mqtt client connected with result code "+str(rc))
    cl.subscribe("iot/#")


def on_message(cl, userdata, msg):
    MessageHandler.save_message(msg)


client = mqtt.Client()
client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
client.on_connect = on_connect
client.on_message = on_message

client.connect(settings.MQTT_SERVER, settings.MQTT_PORT, 60)
