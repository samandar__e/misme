import datetime
import json
import logging

import pytz
from paho.mqtt.publish import single

from greenhouse.models import DeviceIndicateHistory, Device, DeviceIndicator
from greenhouse.mqtt.pydantic import IoTPayload
from greenhouse.sio import sio
from misme import settings


class MessageHandler:

    @classmethod
    def get_device_name(cls, topic):
        device_name = topic.split('/')[1]
        return device_name

    @classmethod
    def save_message(cls, msg):
        """ Save topic message to db """
        logging.info('Topic:' + msg.topic)

        p_json = json.loads(msg.payload.decode('utf-8'))
        payload = IoTPayload(**p_json)

        logging.debug(payload)

        topic = msg.topic
        device = Device.objects.filter(control_panel_id=cls.get_device_name(topic)).first()
        device_indicator = DeviceIndicator.objects.filter(topic=topic).first()

        if device_indicator:
            logging.debug(device)

            date_time = datetime.datetime.strptime(payload.ts, '%Y-%m-%dT%H:%M:%S.%f')
            ltz = pytz.timezone(settings.TIME_ZONE)
            registered_at = ltz.localize(date_time)

            value = payload.value[0]
            position = device_indicator.left_float_point
            if position > 0:
                value = int(str(payload.value[0])[:position])

            DeviceIndicateHistory.objects.create(
                device=device,
                device_indicator=device_indicator,
                value=value,
                registered_at=registered_at,
            )

            """ send data to front via socket """
            sio.emit(event='message', data={'indicator': device_indicator, 'value': value})

    @classmethod
    def send_message_to_device(cls, topic, payload):
        """ Payload to device """
        single(topic=topic,
               payload=payload,
               qos=0,
               retain=False,
               hostname=settings.MQTT_SERVER,
               port=settings.MQTT_PORT,
               keepalive=60,
               auth={'username': settings.MQTT_USERNAME, 'password': settings.MQTT_PASSWORD},
               transport="tcp")
