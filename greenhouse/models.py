from django.db import models
from django.utils.translation import ugettext_lazy as _
import datetime

from directory.models import Region, District
from greenhouse.managers.device import DeviceManager
from restapp.models import BaseModel
from subproject.models import Subproject
from users.models import User


class Greenhouse(BaseModel):
    """ Greenhouse """
    name = models.CharField(_('Greenhouse Name'), max_length=225, blank=False)
    greenhouse_id = models.IntegerField(_('Greenhouse Id'), blank=True, null=True)
    owner = models.CharField(_('Owner'), max_length=225, blank=True)
    address = models.TextField(_('Address'), blank=True, null=True)
    region = models.ForeignKey(Region, verbose_name=_('Region'), on_delete=models.SET_NULL, null=True)
    district = models.ForeignKey(District, verbose_name=_('District'), on_delete=models.SET_NULL, null=True)
    subproject = models.ForeignKey(Subproject, verbose_name=_('Subproject Ref'), on_delete=models.SET_NULL, blank=False,
                                   null=True)
    users = models.ManyToManyField(User, blank=True)

    class Meta:
        verbose_name = _('greenhouse')
        verbose_name_plural = _('greenhouses')
        db_table = 'greenhouse'

    def __str__(self):
        return self.name


class MobileAppGreenhouse(BaseModel):
    """ Mobile App Greenhouses """
    STATUS = [
        ('ACTIVATED', 'Activated'),
        ('NOTACTIVATED', 'Not activated'),
    ]
    greenhouse = models.ForeignKey(Greenhouse, verbose_name=_('Greenhouse'), on_delete=models.SET_NULL, null=True)
    phone = models.CharField(_('Phone'), max_length=225, blank=False)
    verify_code = models.CharField(_('Verify code'), max_length=225, blank=False)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=True)

    class Meta:
        verbose_name = _('mobile app greenhouse')
        verbose_name_plural = _('mobile app greenhouses')
        db_table = 'greenhouse_mobile_app_greenhouses'

    def __str__(self):
        return self.phone


class DeviceApp(BaseModel):
    """ Each device may has several app """
    name = models.CharField(_('App name'), max_length=255, blank=True)
    version = models.CharField(_('App version'), max_length=50, blank=True)

    class Meta:
        verbose_name = _('Device app')
        verbose_name_plural = _('Device apps')

    def __str__(self):
        return self.name

# @todo app page templates


class Device(BaseModel):
    """ Device """
    greenhouse = models.ForeignKey(Greenhouse, related_name=_('device'), on_delete=models.SET_NULL,
                                   null=True)
    name = models.CharField(_('Name'), max_length=225, blank=False)
    control_panel_id = models.TextField(_('Control Panel Id'), blank=True, null=True)
    current_ip = models.CharField(_('Current IP Address'), max_length=225, blank=True)
    device_app = models.ForeignKey(DeviceApp, verbose_name='Device app', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('Device')
        verbose_name_plural = _('Devices')
        db_table = 'greenhouse_device'

    def __str__(self):
        return self.name

    objects = DeviceManager()


class TopicMessage(BaseModel):
    topic = models.CharField(_('Topic name'), max_length=255, blank=True)
    message = models.TextField(_('Message payload'), blank=True)
    device = models.ForeignKey(Device, related_name='topics', on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _('MQTT message')
        verbose_name_plural = _('MQTT messages')

    def __str__(self):
        return self.message


class DeviceIndicator(BaseModel):
    COMPONENT_TYPE = [
        ('TEXT', 'Text'),
        ('BUTTON', 'Button'),
    ]

    name = models.CharField(_('Indicator name'), max_length=200, blank=True)
    code_name = models.CharField(_('Indicator code name'), max_length=200, blank=True)
    device_app = models.ForeignKey(DeviceApp, related_name='device_app_components', on_delete=models.SET_NULL, null=True)
    address = models.CharField(_('Component address'), max_length=20)
    type = models.CharField(_('Component type'), max_length=50, choices=COMPONENT_TYPE, default='TEXT')
    topic = models.CharField(_('Topic for MQTT'), max_length=150, blank=True)
    read_only = models.BooleanField(default=False)
    subscribe = models.BooleanField(default=False)
    publish = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    left_float_point = models.IntegerField(_('Left of float point'), default=0)
    current_state = models.CharField(_('Current state'), max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = _('CP indicator')
        verbose_name_plural = _('CP indicators')

    def __str__(self):
        return f"{self.name} [{self.address}]"


class DeviceIndicateHistory(BaseModel):
    """ Device indicators History """
    device = models.ForeignKey(Device, related_name='device_indicate_history', on_delete=models.SET_NULL, null=True)
    device_indicator = models.ForeignKey(DeviceIndicator, related_name='indicate_history',
                                         on_delete=models.SET_NULL, null=True)
    value = models.CharField(_('Indicator value'), max_length=100, blank=True)
    registered_at = models.DateTimeField()

    class Meta:
        verbose_name = _('CP indicator history')
        verbose_name_plural = _('CP indicators history')

    def __str__(self):
        return f"{self.device_indicator.name}"


class DeviceLog(BaseModel):
    """ Device Log """
    device = models.ForeignKey(Device, related_name=_('device_log'), on_delete=models.SET_NULL, null=True)
    message = models.TextField(_('Address'), blank=True, null=True)
    last_ping = models.DateField(_('Create date'), default=datetime.date.today, max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('CP log')
        verbose_name_plural = _('CP logs')
        db_table = 'greenhouse_device_log'

    def __str__(self):
        return self.message


