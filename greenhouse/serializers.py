from django.contrib.auth.hashers import make_password
from django.contrib.auth.validators import UnicodeUsernameValidator
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from greenhouse.models import Greenhouse, MobileAppGreenhouse, DeviceLog, Device, DeviceIndicateHistory, DeviceIndicator
from subproject.models import Subproject
from users.models import User


class GreenhouseSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField(method_name='get_created_user_name', read_only=True)
    created_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)

    class Meta:
        model = Greenhouse
        fields = ('id', 'name', 'greenhouse_id', 'owner', 'address', 'region', 'district', 'subproject',
                  'created_by', 'created_time')

    def get_created_user_name(self, instance):
        return instance.created_by.username if instance.created_by else None

    def create(self, validated_data):
        greenhouse = Greenhouse.objects.create(**validated_data)

        subproject = Subproject.objects.get(id=greenhouse.subproject.id)
        greenhouse.region = subproject.region
        greenhouse.district = subproject.district
        greenhouse.owner = str(subproject.beneficiary)

        greenhouse.save()
        return greenhouse


class MobileAppGreenhouseSerializer(serializers.ModelSerializer):

    class Meta:
        model = MobileAppGreenhouse
        fields = ('id', 'phone', 'verify_code', 'status', 'greenhouse')


class GreenhouseDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Greenhouse
        fields = ('id', 'name', 'greenhouse_id', 'owner', 'address', 'region', 'district', 'subproject')


class DeviceSerializer(serializers.ModelSerializer):
    greenhouse_detail = GreenhouseDetailSerializer(source='greenhouse', read_only=True)

    class Meta:
        model = Device
        fields = ('id', 'name', 'control_panel_id', 'current_ip', 'greenhouse', 'greenhouse_detail')


class DeviceLogSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceLog
        fields = ('id', 'device', 'last_ping', 'message')


class DeviceIndicatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = DeviceIndicator
        fields = ('id', 'name', 'address')


class DeviceIndicateHistorySerializer(serializers.ModelSerializer):
    device_indicator = DeviceIndicatorSerializer()

    class Meta:
        model = DeviceIndicateHistory
        fields = ('id', 'device', 'device_indicator', 'value', 'registered_at')


class GreenhouseUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, allow_blank=True, required=False)
    greenhouse = PrimaryKeyRelatedField(queryset=Greenhouse.objects.all(), write_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'greenhouse')
        extra_kwargs = {
            'username': {
                'validators': [UnicodeUsernameValidator()],
            }
        }

    def create(self, validated_data):
        greenhouse = self.validated_data['greenhouse']
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password']
        )
        greenhouse.users.add(user)
        greenhouse.save()
        return user

    def update(self, instance, validated_data):
        username = self.validated_data['username']
        password = self.validated_data['password']

        instance.username = username
        instance.password = make_password(password)
        instance.save()
        return instance
