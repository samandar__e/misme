from django_filters.rest_framework import FilterSet

from greenhouse.models import Greenhouse, MobileAppGreenhouse, Device
from users.models import User


class GreenhouseFilter(FilterSet):

    class Meta:
        model = Greenhouse
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'greenhouse_id': ['lt', 'gt'],
            'owner': ['exact'],
            'address': ['exact'],
            'region': ['exact'],
            'district': ['exact'],
            'subproject': ['exact'],
        }


class MobileAppGreenhouseFilter(FilterSet):

    class Meta:
        model = MobileAppGreenhouse
        fields = {
            'phone': ['exact', 'startswith', 'contains'],
            'verify_code': ['exact'],
            'status': ['exact']
        }


class DeviceFilter(FilterSet):

    class Meta:
        model = Device
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'control_panel_id': ['exact'],
            'current_ip': ['exact'],
            'greenhouse': ['exact']
        }


class GreenhouseUserFilter(FilterSet):

    class Meta:
        model = User
        fields = {'username': ['exact', 'startswith', 'contains'], 'greenhouse': ['exact']}
