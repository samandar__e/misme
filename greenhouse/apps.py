from django.apps import AppConfig


class GreenhouseConfig(AppConfig):
    name = 'greenhouse'

    def ready(self):
        from greenhouse.mqtt.client import client
        try:
            client.loop_start()
        except:
            print("Can't connect to MQTT server")
