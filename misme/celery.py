import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'misme.settings')
app = Celery('misme')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'get-exchange-rate-every-day': {
        'task': 'directory.tasks.task_get_exchanges_from_cbu',
        'schedule': crontab(hour=15, minute=20),
    },
}


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
