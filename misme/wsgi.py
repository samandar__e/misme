"""
WSGI config for misme project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
# import socketio
from django.core.wsgi import get_wsgi_application
# import eventlet

# from greenhouse.sio import sio

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'misme.settings')

application = get_wsgi_application()

# django_app = get_wsgi_application()
# application = socketio.WSGIApp(sio, django_app)
#
# eventlet.wsgi.server(eventlet.listen(('', 8000)), application)

# application = socketio.WSGIApp(sio, django_app)
