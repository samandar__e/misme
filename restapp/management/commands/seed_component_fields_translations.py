from django.core.management import BaseCommand

from restapp.models import TranslationTerm


class Command(BaseCommand):
    help = 'Seed component fields translations'

    def handle(self, *args, **options):
        terms = [
            'name', 'type', 'variant', 'order_number', 'loan_grant_name', 'responsible', 'loan_grant_amount',
            'brief_description', 'project', 'sector', 'field_of_activity', 'requirement', 'pfi', 'region', 'district',
            'repayment_currency', 'source_funding', 'ifi_product']

        names_en = [
            'Name', 'Type', 'Variant', 'Order number', 'Loan grant name', 'Responsible', 'Loan grant amount',
            'Brief Description', 'Project', 'Sector', 'Field of activity', 'Requirement', 'PFI', 'Region', 'District',
            'Repayment currency', 'Source funding', 'IFI product']

        names_ru = [
            "Название компонента", "Тип компонента", "Вариант компонента", "Порядковый номер", "Название_кредита",
            "Ответственное_лицо", "Сумма кредита", "Краткое описание", "Проект", "Сектор", "Сфера деятельности",
            "Экологические требования", "УФО", "Регион", "Район", "Валюта погашения", "Источник финансирования",
            "Продукт МФИ"]

        names_uz = [
            "Komponent nomi", "Komponent turi", "Komponent varianti", "Tartib raqami", "Kredit nomi", "Mas'ul shaxs",
            "Kredit miqdori", "Qisqacha tavsifi", "Loyiha", "Sektor", "Faoliyat sohasi", "Ekologik talab", "IMM",
            "Viloyat", "Tuman", "To'lov valyutasi", "Moliyalash manbai", "XMM mahsuloti"]

        for term, name_en, name_ru, name_uz in zip(terms, names_en, names_ru, names_uz):
            translation = TranslationTerm()
            translation.term_name = f"component_{term}"
            translation.name = name_en
            translation.name_en = name_en
            translation.name_ru = name_ru
            translation.name_uz = name_uz
            translation.save()

        print("Seed translations is completed")



