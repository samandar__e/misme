from django.core.management import BaseCommand

from restapp.models import TranslationTerm


class Command(BaseCommand):
    help = 'Seed subproject fields translations'

    def handle(self, *args, **options):
        terms = [
            'name', 'total_cost', 'beneficiary_amount', 'cofinanced_pfi', 'loan_amount',
            'production_capacity', 'start_activity', 'start_financing', 'status', 'overall_jobs',
            'men', 'women', 'beneficiary', 'component', 'goods_imported_country', 'region', 'district',
            'production_capacity_unit', 'project', 'pfi', 'sectors', 'field_of_activities', 'environment_requirements']

        names_en = [
            'Name', 'Total cost', 'Beneficiary amount', 'Cofinanced PFI', 'Loan amount',
            'Production capacity', 'Start activity', 'Start Financing', 'Status', 'Overall jobs',
            'Men', 'Women', 'Beneficiary', 'Component', 'Goods imported country', 'Region', 'District',
            'Production capacity unit', 'Project', 'PFI', 'Sectors', 'Field of activities', 'Environment requirements']

        names_ru = [
            "Название", "Общая стоимость", "Вклад бенефициара", "Софинансирование УФО", "Сумма кредита",
            "Производственные мощности", "Начало деятельности", "Начало финансирования", "Статус",
            "Общие рабочие места", "Мужчины", "Женщины", "Бенефициар", "Компонент", "Страна импорта товаров",
            "Регион подроекта", "Район подпроекта", "Единица измерения производственной мощности", "Проект", "УФО",
            "Секторы", "Сфера деятельности", "Экологические требования"]

        names_uz = [
            "Nomlanishi", "Umumiy qiymati", "Benefitsiar ulushi", "IMM ulushi", "Kredit qiymati",
            "Ishlab chiqarish quvvati", "Faoliyat bosh sanasi", "Moliyalash bosh sanasi", "Status",
            "Umumiy ishjoyi soni", "Erkaklar soni", "Ayollar soni", "Benefitsiar", "Komponent",
            "Tovarlar importi mamlakati", "Kichik loyiha viloyati", "Kichik loyiha tumani",
            "Ishlab chiqarish quvvati birligi", "Loyiha", "IMM", "Sektorlar", "Faoliyat sohasi",
            "Atrof muhitga talablar"]

        for term, name_en, name_ru, name_uz in zip(terms, names_en, names_ru, names_uz):
            translation = TranslationTerm()
            translation.term_name = f"subproject_{term}"
            translation.name = name_en
            translation.name_en = name_en
            translation.name_ru = name_ru
            translation.name_uz = name_uz
            translation.save()

        print("Seed translations is completed")



