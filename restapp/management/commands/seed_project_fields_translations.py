from django.core.management import BaseCommand

from restapp.models import TranslationTerm


class Command(BaseCommand):
    help = 'Seed project fields translations'

    def handle(self, *args, **options):
        terms = [
            'name', 'projectNumber', 'govResolution', 'description', 'overallProjectCost', 'sdrValue',
            'currencyOne', 'currencyTwo', 'localGovernmentContribution', 'beneficiaryFunds', 'headOfPiu',
            'piuInTable', 'piuEmployeesActual', 'dateSigned', 'dateClosing', 'availableStudy',
            'overallCount', 'menCount', 'womenCount', 'status', 'ifi', 'component', 'region', 'district', 'pfi']

        names_en = [
            'Name', 'Project Number', 'Government resolution', 'Description', 'Overall project cost', 'Sdr value',
            'Currency one', 'Currency two', 'Local government contribution', 'Beneficiary funds', 'Head of PIU',
            'PIU in table', 'PIU employees actual', 'Date signed', 'Date closing', 'Available study',
            'Overall count', 'Men count', 'Women count', 'Status', 'IFI', 'Component', 'Region', 'District', 'PFI']

        names_ru = [
            "Название проекта", 'Номер проекта', 'Постановление правительства', 'Описание', 'Общая стоимость проекта',
            'Сумма в SDR', "Первая валюта", "Вторая валюта", "Вклад местного правительства", "Вклад бенефициара",
            "Глава ГРП", "Штатные единицы ГРП", "Сотрудники ГРП по факту", "Дата подписания",
            "Дата закрытия", "Наличие ТЭО", "Общее количество рабочих мест", "Количество мужчин", "Количество женщин",
            "Статус", "МФИ", "Компонент", "Регион", "Район", "УФО"]

        names_uz = [
            "Nomi", "Loyiha raqami", "Hukumat qarori", "Tavsifi", "Loyihaning umumiy qiymati", "Sdr qiymati",
            "LIG rahbari", "Birinchi valyuta", "Ikkinchi valyuta", "Mahalliy hukumat ulushi", "Benefitsiar ulushi",
            "LIG shtat birliklari", "LIG xodimlar soni amalda", "Imzolangan sanasi", "Yakunlanish sanasi",
            "TIA mavjudligi", "Umumiy ish joyi soni", "Erkaklar soni",  "Ayollar soni", "Holati", "XMM", "Komponent",
            "Viloyat", "Tuman", "IMM"]

        for term, name_en, name_ru, name_uz in zip(terms, names_en, names_ru, names_uz):
            translation = TranslationTerm()
            translation.term_name = f"project_{term}"
            translation.name = name_en
            translation.name_en = name_en
            translation.name_ru = name_ru
            translation.name_uz = name_uz
            translation.save()

        print("Seed translations is completed")



