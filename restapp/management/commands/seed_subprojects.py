import random

from django.core.management import BaseCommand
from faker import Faker

from components.models import Component
from crm.models import Beneficiary, ParticipatingFinancialInstitution
from directory.models import Country, Region, District, UOM, Currency
from projects.models import Projects
from subproject.models import Subproject
from users.models import User


class Command(BaseCommand):
    help = 'Seed subproject with faker data'

    def handle(self, *args, **options):
        faker = Faker()
        uom = UOM.objects.first()
        component = Component.objects.first()
        project = Projects.objects.first()
        currency = Currency.objects.first()
        beneficiary = Beneficiary.objects.first()
        pfi = ParticipatingFinancialInstitution.objects.first()
        user = User.objects.get(pk=1)
        counter = 0
        countries = Country.objects.all()
        for country in countries:
            regions = Region.objects.all()
            for region in regions:
                districts = District.objects.all().filter(region=region)
                for district in districts:
                    for i in range(5):
                        status = random.choice(Subproject.STATUS)
                        name = faker.name()

                        subproject = Subproject()
                        subproject.name = name
                        subproject.goods_imported_country = country
                        subproject.region = region
                        subproject.district = district
                        subproject.status = status[0]
                        subproject.component = component
                        subproject.project = project
                        subproject.total_cost = random.randint(100, 1000)
                        subproject.total_cost_currency = currency
                        subproject.cofinanced_pfi = random.randint(100, 1000)
                        subproject.cofinanced_pfi_currency = currency
                        subproject.loan_amount = random.randint(100, 1000)
                        subproject.loan_currency = currency
                        subproject.beneficiary = beneficiary
                        subproject.beneficiary_amount = random.randint(100, 1000)
                        subproject.beneficiary_amount_currency = currency
                        subproject.production_capacity = random.randint(100, 1000)
                        subproject.production_capacity_unit = uom
                        subproject.start_activity = "2020-11-10"
                        subproject.start_financing = "2020-11-10"
                        subproject.overall_jobs = random.randint(50, 100)
                        subproject.men = random.randint(1, subproject.overall)
                        subproject.women = subproject.overall - subproject.men
                        subproject.pfi = pfi
                        subproject.revolver_means = bool(random.getrandbits(1))
                        subproject.created_by = user
                        subproject.updated_by = user
                        subproject.save()
                        counter = counter + 1

        print("Created {} records".format(counter))



