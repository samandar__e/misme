from django_filters.rest_framework import FilterSet, ModelChoiceFilter

from components.models import Trainings, ReportSchedule, Amendments, Contracts, Announcements
from crm.models import ParticipatingFinancialInstitution
from mail.models import EmailMessage
from restapp.models import Attachment, ModelAudit
from subproject.models import Visit, Subproject


class AttachmentFilter(FilterSet):
    visit = ModelChoiceFilter(queryset=Visit.objects.all())
    subproject = ModelChoiceFilter(queryset=Subproject.objects.all())
    emailmessage = ModelChoiceFilter(queryset=EmailMessage.objects.all())
    trainings = ModelChoiceFilter(queryset=Trainings.objects.all())
    reportschedule = ModelChoiceFilter(queryset=ReportSchedule.objects.all())
    amendments = ModelChoiceFilter(queryset=Amendments.objects.all())
    contracts = ModelChoiceFilter(queryset=Contracts.objects.all())
    announcements = ModelChoiceFilter(queryset=Announcements.objects.all())
    pfi = ModelChoiceFilter(field_name='participatingfinancialinstitution', queryset=ParticipatingFinancialInstitution.objects.all())

    class Meta:
        model = Attachment
        fields = ['visit', 'subproject', 'emailmessage', 'trainings', 'reportschedule',
                  'amendments', 'contracts', 'announcements', 'pfi']


class ModelAuditFilter(FilterSet):

    class Meta:
        model = ModelAudit
        fields = ['module']

    @property
    def qs(self):
        parent = super().qs
        instance_id = getattr(self.request, 'id', None)
        instance = getattr(self.request, 'module', None)
        if instance_id and instance:
            return parent.filter(instance_id=instance_id) & parent.filter(instance=instance)
        else:
            return parent
