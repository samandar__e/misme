from django.urls import re_path, path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

from restapp.views.attachment import AttachmentView, AttachmentDetailView, AttachmentDownloadView
from restapp.views.language import LanguagesView
from restapp.views.logout import LogoutView
from restapp.views.term import TermView, TermDetailView
from restapp.views.translations import TranslationsView
from restapp.views.user_log import UserLogsView


urlpatterns = [
    re_path(r'^auth/token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^auth/token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^auth/logout/$', LogoutView.as_view(), name='auth_logout'),
    path('', include('directory.urls')),
    path('', include('users.urls')),
    path('', include('crm.urls')),
    path('', include('mail.urls')),
    path('', include('subproject.urls')),
    path('', include('components.urls')),
    path('', include('projects.urls')),
    path('', include('reports.urls')),
    path('', include('greenhouse.urls')),
    path('', include('statistic.urls')),

    re_path(r'^settings/languages/$', LanguagesView.as_view(), name='languages_list'),
    re_path(r'^settings/translations/$', TermView.as_view(), name='translations_list'),
    path('settings/translations/<int:pk>', TermDetailView.as_view(), name='translations_list'),
    re_path(r'^settings/userlogs/$', UserLogsView.as_view(), name='user_logs'),

    re_path(r'^attachment/$', AttachmentView.as_view(), name='attachment'),
    path('attachment/<int:pk>', AttachmentDetailView.as_view(), name='attachment_detail'),

    path('translations', TranslationsView.as_view(), name='translations_list')
]


