import mimetypes
import os
import uuid

from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters, permissions
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from restapp.filterset import AttachmentFilter
from restapp.pagination import ResultsSetPagination
from restapp.serializers import AttachmentSerializer
from restapp.utils.responses import nonContent
from subproject.models import Attachment


class AttachmentView(ListCreateAPIView):
    serializer_class = AttachmentSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = AttachmentFilter
    search_fields = ('file_name',)
    ordering = ['-pk']

    def get_queryset(self):
        return Attachment.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class AttachmentDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = AttachmentSerializer
    http_method_names = ['get', 'put', 'delete']

    def get(self, request, pk):
        attachment = get_object_or_404(Attachment, id=pk)
        serializer = self.serializer_class(attachment)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        attachment = get_object_or_404(Attachment, id=pk)
        serializer = self.serializer_class(attachment, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        attachment = get_object_or_404(Attachment, id=pk)
        attachment.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class AttachmentDownloadView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk):
        attachment = get_object_or_404(Attachment, id=pk)
        file_extension = os.path.splitext(attachment.file.path)[1]
        file = open(attachment.file.path, 'rb')
        mime_type, _ = mimetypes.guess_type(attachment.file.path)
        response = HttpResponse(file, content_type=mime_type)
        response['Content-Disposition'] = 'attachment; filename="%s%s"' % (str(uuid.uuid4()), file_extension)
        return response

