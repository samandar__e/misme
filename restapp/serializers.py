import os

from rest_framework import serializers
from rest_framework.generics import get_object_or_404

from components.models import Trainings, ReportSchedule, Amendments, Contracts, Announcements
from crm.models import ParticipatingFinancialInstitution
from subproject.models import Subproject, Visit
from .models import TranslationTerm, ModelAudit, Attachment
from .utils import request_helper


class TermSerializer(serializers.ModelSerializer):

    class Meta:
        model = TranslationTerm
        fields = ('id', 'term_name', 'name_uz', 'name_ru', 'name_en')


class ModelAuditSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField(method_name='get_username')

    class Meta:
        model = ModelAudit
        fields = ('id', 'user', 'instance', 'instance_id', 'field_name', 'old_value', 'new_value', 'data', 'action',
                  'timestamp')

    def get_username(self, instance):
        if instance.user:
            return instance.user.username


class PfiField(serializers.Field):
    def to_representation(self, instance):
        return instance.participatingfinancialinstitution

    def to_internal_value(self, data):
        return data


class AttachmentSerializer(serializers.ModelSerializer):
    file = serializers.FileField(required=False)
    subproject = serializers.IntegerField(write_only=True, required=False)
    visit = serializers.IntegerField(write_only=True, required=False)
    trainings = serializers.IntegerField(write_only=True, required=False)
    reportschedule = serializers.IntegerField(write_only=True, required=False)
    amendments = serializers.IntegerField(write_only=True, required=False)
    contracts = serializers.IntegerField(write_only=True, required=False)
    announcements = serializers.IntegerField(write_only=True, required=False)
    pfi = PfiField(source='participatingfinancialinstitution', required=False)

    class Meta:
        model = Attachment
        fields = ('id', 'file_name', 'file', 'visit', 'subproject', 'trainings', 'reportschedule',
                  'amendments', 'contracts', 'announcements', 'pfi')

    def create(self, validated_data):
        subproject = validated_data.get('subproject', None)
        visit = validated_data.get('visit', None)
        training = validated_data.get('trainings', None)
        schedule = validated_data.get('reportschedule', None)
        amendments = validated_data.get('amendments', None)
        contracts = validated_data.get('contracts', None)
        announcements = validated_data.get('announcements', None)
        pfi = validated_data.get('participatingfinancialinstitution', None)

        attachment = Attachment.objects.create(**validated_data)
        if subproject:
            subproject = get_object_or_404(Subproject, id=subproject)
            subproject.attachments.add(attachment)
            subproject.save()
        if visit:
            visit = get_object_or_404(Visit, id=visit)
            visit.attachments.add(attachment)
            visit.save()
        if training:
            training = get_object_or_404(Trainings, id=training)
            training.file.add(attachment)
            training.save()
        if schedule:
            report_schedule = get_object_or_404(ReportSchedule, id=schedule)
            report_schedule.file.add(attachment)
            report_schedule.save()
        if amendments:
            amendment = get_object_or_404(Amendments, id=amendments)
            amendment.file.add(attachment)
            amendment.save()
        if contracts:
            contract = get_object_or_404(Contracts, id=contracts)
            contract.file.add(attachment)
            contract.save()
        if announcements:
            announcement = get_object_or_404(Announcements, id=announcements)
            announcement.file.add(attachment)
            announcement.save()
        if pfi:
            pfi = get_object_or_404(ParticipatingFinancialInstitution, id=pfi)
            pfi.file.add(attachment)
            pfi.save()

        return attachment

    def to_representation(self, instance):
        request = request_helper.get_request()
        _, tail = os.path.split(instance.file.path)
        return {
            "id": instance.id,
            "file": request.build_absolute_uri("/download/%s" % instance.id),
            "document_name": instance.file_name,
            "file_name": tail
        }


class TranslationSerializer(serializers.ModelSerializer):

    class Meta:
        model = TranslationTerm
        fields = ('term_name', 'name')
