from django.contrib import admin

from components.models import Component, ComponentImplementation, ReportSchedule, PmCosts, Trainings, Participants, \
    ProcurementPlan, Announcements, Contracts, Amendments, PaymentsTranche


@admin.register(Component)
class ComponentAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'variant', 'parent', 'order_number', )
    fields = ('name', 'type', 'variant', 'parent', 'order_number',)
    search_fields = ('name', 'type', 'variant', 'parent', 'order_number',)


@admin.register(ComponentImplementation)
class ComponentImplementationAdmin(admin.ModelAdmin):
    list_display = ('month_year', 'forecast_value', 'actual_value', 'current_balance')
    fields = ('month_year', 'forecast_value', 'actual_value', 'current_balance')
    search_fields = ('month_year', 'forecast_value', 'actual_value', 'current_balance')


@admin.register(ReportSchedule)
class ReportScheduleAdmin(admin.ModelAdmin):
    list_display = ('name', 'due_date', 'type', 'status')
    fields = ('name', 'due_date', 'type', 'status')
    search_fields = ('name', 'due_date', 'type', 'status')


@admin.register(PmCosts)
class PmCostsAdmin(admin.ModelAdmin):
    list_display = ('item_name', 'payment_document', 'date_cr')
    fields = ('item_name', 'payment_document', 'date_cr')
    search_fields = ('item_name', 'payment_document', 'date_cr')


@admin.register(Trainings)
class TrainingsAdmin(admin.ModelAdmin):
    list_display = ('topic', 'date_of_training', 'location', 'notes')
    fields = ('topic', 'date_of_training', 'location', 'notes')
    search_fields = ('topic', 'date_of_training', 'location', 'notes')


@admin.register(Participants)
class ParticipantsAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'organization', 'gender', 'age')
    fields = ('full_name', 'organization', 'gender', 'age')
    search_fields = ('full_name', 'organization', 'gender', 'age')


@admin.register(ProcurementPlan)
class ProcurementPlanAdmin(admin.ModelAdmin):
    list_display = ('package_name', 'package_contract_number', 'budget', 'description')
    fields = ('package_name', 'package_contract_number', 'budget', 'description')
    search_fields = ('package_name', 'package_contract_number', 'budget', 'description')


@admin.register(Announcements)
class AnnouncementsAdmin(admin.ModelAdmin):
    list_display = ('ref_number', 'send_to', 'subject', 'expiration_date')
    fields = ('ref_number', 'send_to', 'subject', 'expiration_date')
    search_fields = ('ref_number', 'send_to', 'subject', 'expiration_date')


@admin.register(Contracts)
class ContractsAdmin(admin.ModelAdmin):
    list_display = ('contractor_name', 'ref_number', 'due_date', 'date_of_signature')
    fields = ('contractor_name', 'ref_number', 'due_date', 'date_of_signature')
    search_fields = ('contractor_name', 'ref_number', 'due_date', 'date_of_signature')


@admin.register(PaymentsTranche)
class PaymentsTrancheAdmin(admin.ModelAdmin):
    list_display = ('name', 'planned_amount', 'paid_amount_no')
    fields = ('name', 'planned_amount', 'paid_amount_no')
    search_fields = ('name', 'planned_amount', 'paid_amount_no')



