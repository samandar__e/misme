from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.models import Contracts
from components.serializers import ContractsSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ContractsView(ListCreateAPIView):
    serializer_class = ContractsSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['procurement_plan']
    search_fields = ('contractor_name',)
    ordering = ['-pk']

    def get_queryset(self):
        queryset = Contracts.objects.all()
        procurement_plan = self.request.query_params.get('procurement_plan', None)
        if procurement_plan is not None:
            queryset = queryset.filter(procurement_plan=procurement_plan)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ContractsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ContractsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Contracts, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Contracts, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Contracts, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
