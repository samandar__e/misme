from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, get_object_or_404, UpdateAPIView, RetrieveDestroyAPIView
from rest_framework.response import Response

from components.filterset import TrainingFilter
from components.models import Trainings
from components.serializers import TrainingsSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class TrainingsView(ListCreateAPIView):
    serializer_class = TrainingsSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = TrainingFilter
    filterset_fields = ['component']
    search_fields = ('topic', 'status', 'location', 'notes')
    ordering = ['-pk']

    def get_queryset(self):
        queryset = Trainings.objects.all()
        component = self.request.query_params.get('component', None)
        if component is not None:
            queryset = queryset.filter(component=component)
        return queryset

    def get(self, request, *args, **kwargs):
        self.serializer_class = TrainingsSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class TrainingsUpdateView(UpdateAPIView):
    serializer_class = TrainingsSerializer
    http_method_names = ['put']

    def get_queryset(self):
        return Trainings.objects.all()

    def update(self, request, pk):
        instance = get_object_or_404(Trainings, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)


class TrainingsDetailView(RetrieveDestroyAPIView):
    serializer_class = TrainingsSerializer

    def get_queryset(self):
        return Trainings.objects.all()

    def get(self, request, pk):
        instance = get_object_or_404(Trainings, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, pk):
        instance = get_object_or_404(Trainings, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)



