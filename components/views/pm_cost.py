from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.filterset import PmCostFilter
from components.models import PmCosts
from components.serializers import PmCostsSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class PmCostsView(ListCreateAPIView):
    serializer_class = PmCostsSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = PmCostFilter
    filterset_fields = ['component']
    search_fields = ('item_name', 'amount_in_uzs', 'amount_in_usd', 'payment_document', 'exchange_rate', 'date_cost')
    ordering = ['-pk']

    def get_queryset(self):
        queryset = PmCosts.objects.all()
        component = self.request.query_params.get('component', None)
        if component is not None:
            queryset = queryset.filter(component=component)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class PmCostsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = PmCostsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(PmCosts, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(PmCosts, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(PmCosts, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
