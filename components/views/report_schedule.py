from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.filterset import ReportScheduleFilter
from components.models import ReportSchedule
from components.serializers import ReportScheduleSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ReportScheduleView(ListCreateAPIView):
    serializer_class = ReportScheduleSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ReportScheduleFilter
    filterset_fields = ['field_id']
    search_fields = ('table_type', 'name', 'type', 'status', 'assignee')
    ordering = ['-pk']

    def get_queryset(self):
        return ReportSchedule.objects.filter(table_type=ReportSchedule.TABLE[0][0])

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user, table_type=ReportSchedule.TABLE[0][0])
        return Response(serializer.data, status.HTTP_201_CREATED)

 
class ReportScheduleDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ReportScheduleSerializer

    def get(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
