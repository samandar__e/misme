from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.models import Participants
from components.serializers import ParticipantsSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ParticipantsView(ListCreateAPIView):
    serializer_class = ParticipantsSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['training']
    search_fields = ('name',)
    ordering = ['-pk']

    def get_queryset(self):
        queryset = Participants.objects.all()
        training = self.request.query_params.get('training', None)
        if training is not None:
            queryset = queryset.filter(training=training)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ParticipantsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ParticipantsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Participants, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Participants, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Participants, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
