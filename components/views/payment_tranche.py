from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.models import PaymentsTranche
from components.serializers import PaymentsTrancheSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class PaymentsTrancheView(ListCreateAPIView):
    serializer_class = PaymentsTrancheSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['contract']
    search_fields = ('name',)
    ordering = ['-pk']

    def get_queryset(self):
        return PaymentsTranche.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class PaymentsTrancheDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = PaymentsTrancheSerializer

    def get(self, request, pk):
        instance = get_object_or_404(PaymentsTranche, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(PaymentsTranche, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(PaymentsTranche, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
