from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.filterset import ImplementationFilter
from components.models import ComponentImplementation
from components.serializers import ComponentImplementationSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ComponentImplementationView(ListCreateAPIView):
    serializer_class = ComponentImplementationSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ImplementationFilter
    filterset_fields = ['component']
    search_fields = ('forecast_value', 'actual_value', 'current_balance')
    ordering = ['-pk']

    def get_queryset(self):
        queryset = ComponentImplementation.objects.all()
        component = self.request.query_params.get('component', None)
        if component is not None:
            queryset = queryset.filter(component=component)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)

 
class ComponentImplementationDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ComponentImplementationSerializer

    def get(self, request, pk):
        instance = get_object_or_404(ComponentImplementation, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ComponentImplementation, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ComponentImplementation, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
