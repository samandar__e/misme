from rest_framework.response import Response
from rest_framework.views import APIView

from components.models import ReportSchedule, Component, Trainings, Participants, ProcurementPlan, Announcements, \
    Contracts, Amendments
from restapp.utils.enumserialize import enum_serialize


class ReportScheduleStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(ReportSchedule.STATUS))

 
class ReportScheduleTypesList(APIView):

    def get(self, request):
        return Response(enum_serialize(ReportSchedule.TYPES))


class ComponentTypesList(APIView):

    def get(self, request):
        return Response(enum_serialize(Component.TYPES))

class ComponentVariantList(APIView):

    def get(self, request):
        return Response(enum_serialize(Component.VARIANT))


class TrainingsStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Trainings.STATUS))


class ParticipantsGenderList(APIView):

    def get(self, request):
        return Response(enum_serialize(Participants.GENDER))


class ProcurementPlanCategoryList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProcurementPlan.CATEGORY))


class ProcurementPlanMethodList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProcurementPlan.METHOD))


class ProcurementPlanMarketList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProcurementPlan.MARKET_APPROACH))


class ProcurementPlanContractTypeList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProcurementPlan.CONTRACT_TYPE))


class ProcurementPlanReviewTypeList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProcurementPlan.REVIEW_TYPE))


class AnnouncementsStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Announcements.STATUS))


class ContractsStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Contracts.STATUS))


class ContractsMethodList(APIView):

    def get(self, request):
        return Response(enum_serialize(Contracts.PAY_METHOD))


class ContractsImpStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Contracts.IMP_STATUS))


class AmendmentsStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Amendments.STATUS))


class AmendmentsMethodList(APIView):

    def get(self, request):
        return Response(enum_serialize(Amendments.PAY_METHOD))