from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.filterset import ProcurementPlanFilter
from components.models import ProcurementPlan
from components.serializers import ProcurementPlanSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProcurementPlanView(ListCreateAPIView):
    serializer_class = ProcurementPlanSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ProcurementPlanFilter
    filterset_fields = ['component']
    search_fields = ('package_contract_number', 'package_name', 'budget', 'description')
    ordering = ['-pk']

    def get_queryset(self):
        queryset = ProcurementPlan.objects.all()
        component = self.request.query_params.get('component', None)
        if component is not None:
            queryset = queryset.filter(component=component)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProcurementPlanDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProcurementPlanSerializer

    def get(self, request, pk):
        instance = get_object_or_404(ProcurementPlan, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ProcurementPlan, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ProcurementPlan, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
