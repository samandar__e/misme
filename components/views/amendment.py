from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.models import Amendments
from components.serializers import AmendmentsSerializer, AmendmentsCreateSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class AmendmentsView(ListCreateAPIView):
    serializer_class = AmendmentsCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_fields = ['contract_number']
    search_fields = ('amendment_number',)
    ordering = ['-pk']

    def get_queryset(self):
        queryset = Amendments.objects.all()
        contract_number = self.request.query_params.get('contract_number', None)
        if contract_number is not None:
            queryset = queryset.filter(contract_number=contract_number)
        return queryset

    def get(self, request, *args, **kwargs):
        self.serializer_class = AmendmentsSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class AmendmentsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = AmendmentsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Amendments, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Amendments, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Amendments, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
