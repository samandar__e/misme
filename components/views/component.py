from django.db.models import Count
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from components.filterset import ComponentFilter
from components.models import Component
from components.serializers import ComponentSerializer, ComponentCreateSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ComponentView(ListCreateAPIView):
    serializer_class = ComponentCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = ComponentFilter
    filterset_fields = ['project']
    search_fields = ('name', 'order_number', 'type', 'variant', 'parent__name', 'project__name')
    # ordering = ['order_number']

    def get_queryset(self):
        return Component.objects.annotate(Count('project')).order_by('project__name', 'order_number')

    def get(self, request, *args, **kwargs):
        self.serializer_class = ComponentSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ComponentDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ComponentSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class ComponentDetailByProjectView(RetrieveUpdateDestroyAPIView):
    serializer_class = ComponentCreateSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Component, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
