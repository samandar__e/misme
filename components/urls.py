from django.urls import re_path, path

from components.views.amendment import AmendmentsView, AmendmentsDetailView
from components.views.announcement import AnnouncementsView, AnnouncementsDetailView
from components.views.contract import ContractsView, ContractsDetailView
from components.views.participant import ParticipantsView, ParticipantsDetailView
from components.views.payment_tranche import PaymentsTrancheView, PaymentsTrancheDetailView
from components.views.procurement_plan import ProcurementPlanView, ProcurementPlanDetailView
from components.views.training import TrainingsView, TrainingsDetailView, TrainingsUpdateView
from .views.pm_cost import PmCostsDetailView, PmCostsView
from .views.enums import ReportScheduleTypesList, ReportScheduleStatusList, ComponentTypesList, TrainingsStatusList, \
    ParticipantsGenderList, ComponentVariantList, ProcurementPlanCategoryList, ProcurementPlanMethodList, \
    ProcurementPlanMarketList, ProcurementPlanContractTypeList, ProcurementPlanReviewTypeList, AnnouncementsStatusList, \
    ContractsStatusList, ContractsMethodList, ContractsImpStatusList, AmendmentsStatusList, AmendmentsMethodList
from .views.report_schedule import ReportScheduleDetailView, ReportScheduleView
from .views.component_implementation import ComponentImplementationDetailView, ComponentImplementationView
from .views.component import ComponentDetailView, ComponentView, ComponentDetailByProjectView

urlpatterns = [

    re_path(r'^report-schedule/$', ReportScheduleView.as_view(), name='report_schedule_view'),
    path('report-schedule/<int:pk>', ReportScheduleDetailView.as_view(), name='report_schedule_detail_view'),

    re_path(r'^component-implementation/$', ComponentImplementationView.as_view(), name='component_implementation_view'),
    path('component-implementation/<int:pk>', ComponentImplementationDetailView.as_view(),
         name='component_implementation_detail_view'),

    re_path(r'^component/$', ComponentView.as_view(), name='component_view'),
    path('component/<int:pk>', ComponentDetailView.as_view(), name='component_detail_view'),
    path('component-by-project/<int:pk>', ComponentDetailByProjectView.as_view(), name='component_detail_view'),

    re_path(r'^pm-costs/$', PmCostsView.as_view(), name='pm_cost_view'),
    path('pm-costs/<int:pk>', PmCostsDetailView.as_view(), name='pm_cost_detail_view'),

    re_path(r'^trainings/$', TrainingsView.as_view(), name='training_view'),
    path('trainings/<int:pk>/update', TrainingsUpdateView.as_view(), name='training_update'),
    path('trainings/<int:pk>', TrainingsDetailView.as_view(), name='training_detail_view'),

    re_path(r'^participants/$', ParticipantsView.as_view(), name='participant_view'),
    path('participants/<int:pk>', ParticipantsDetailView.as_view(), name='participant_detail_view'),

    re_path(r'^procurement-plan/$', ProcurementPlanView.as_view(), name='procurement_plan_view'),
    path('procurement-plan/<int:pk>', ProcurementPlanDetailView.as_view(), name='procurement_plan_detail_view'),

    re_path(r'^announcements/$', AnnouncementsView.as_view(), name='announcement_view'),
    path('announcements/<int:pk>', AnnouncementsDetailView.as_view(), name='announcement_detail_view'),

    re_path(r'^contracts/$', ContractsView.as_view(), name='contract_view'),
    path('contracts/<int:pk>', ContractsDetailView.as_view(), name='contract_detail_view'),

    re_path(r'^amendments/$', AmendmentsView.as_view(), name='amendment_view'),
    path('amendments/<int:pk>', AmendmentsDetailView.as_view(), name='amendment_detail_view'),

    re_path(r'^payments-tranche/$', PaymentsTrancheView.as_view(), name='payment_tranche_view'),
    path('payments-tranche/<int:pk>', PaymentsTrancheDetailView.as_view(), name='payment_tranche_detail_view'),

    # enums urls
    re_path(r'^enum-report-schedule-types/$', ReportScheduleTypesList.as_view(), name='report_schedule_types_view'),
    re_path(r'^enum-report-schedule-status/$', ReportScheduleStatusList.as_view(), name='report_schedule_status_view'),
    re_path(r'^enum-component-types/$', ComponentTypesList.as_view(), name='component_types_view'),
    re_path(r'^enum-component-variant/$', ComponentVariantList.as_view(), name='component_variant_view'),
    re_path(r'^enum-trainings-status/$', TrainingsStatusList.as_view(), name='trainings_status_view'),
    re_path(r'^enum-participants-gender/$', ParticipantsGenderList.as_view(), name='participants_gender_view'),
    re_path(r'^enum-procurement-plan-category/$', ProcurementPlanCategoryList.as_view(),
            name='procurement_plan_category_view'),
    re_path(r'^enum-procurement-plan-method/$', ProcurementPlanMethodList.as_view(), name='procurement-plan_method_view'),
    re_path(r'^enum-procurement-plan-market/$', ProcurementPlanMarketList.as_view(), name='procurement-plan_market_view'),
    re_path(r'^enum-procurement-plan-contract-type/$', ProcurementPlanContractTypeList.as_view(),
            name='procurement-plan_contract_type_view'),
    re_path(r'^enum-procurement-plan-review-type/$', ProcurementPlanReviewTypeList.as_view(),
            name='procurement-plan_review_type_view'),
    re_path(r'^enum-announcements-status/$', AnnouncementsStatusList.as_view(), name='announcements_status_view'),
    re_path(r'^enum-contracts-status/$', ContractsStatusList.as_view(), name='contracts_status_view'),
    re_path(r'^enum-contracts-method/$', ContractsMethodList.as_view(), name='contracts_method_view'),
    re_path(r'^enum-contracts-imp-status/$', ContractsImpStatusList.as_view(), name='contracts_imp_status_view'),
    re_path(r'^enum-amendments-status/$', AmendmentsStatusList.as_view(), name='amendments_status_view'),
    re_path(r'^enum-amendments-method/$', AmendmentsMethodList.as_view(), name='amendments_method_view'),

]
