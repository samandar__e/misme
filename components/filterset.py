from django_filters.rest_framework import FilterSet

from components.models import Component, ProcurementPlan, PmCosts, ReportSchedule, ComponentImplementation, Trainings


class ComponentFilter(FilterSet):

    class Meta:
        model = Component
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'project': ['exact'],
            'type': ['exact'],
            'variant': ['exact'],
            'parent': ['exact'],
            'order_number': ['exact'],
            'loan_grant_name': ['exact'],
            'implementation_start_date': ['lt', 'gt'],
            'implementation_end_date': ['lt', 'gt'],
            'responsible': ['exact'],
            'loan_grant_amount': ['lt', 'gt'],
            'loan_grant_amount_currency': ['exact'],
            'repayment_currency': ['exact'],
            'brief_description': ['exact'],
            'source_fundings': ['exact'],
            'ifi_products': ['exact'],
            'regions': ['exact'],
            'districts': ['exact'],
            'sectors': ['exact'],
            'foas': ['exact'],
            'pfis': ['exact'],
        }


class ProcurementPlanFilter(FilterSet):

    class Meta:
        model = ProcurementPlan
        fields = {
            'package_name': ['exact', 'startswith', 'contains'],
            'component': ['exact'],
            'package_contract_number': ['exact'],
            'budget': ['lt', 'gt'],
            'responsible_person': ['exact'],
            'procurement_category': ['exact'],
            'procurement_method': ['exact'],
            'market_approach': ['exact'],
            'contract_type': ['exact'],
            'review_type': ['exact'],
            'ifi': ['exact'],
            'description': ['exact'],
        }


class PmCostFilter(FilterSet):

    class Meta:
        model = PmCosts
        fields = {
            'item_name': ['exact', 'startswith', 'contains'],
            'amount_in_uzs': ['lt', 'gt'],
            'amount_in_usd': ['lt', 'gt'],
            'exchange_rate': ['lt', 'gt'],
            'payment_document': ['exact'],
            'date_cr': ['exact'],
        }


class ReportScheduleFilter(FilterSet):

    class Meta:
        model = ReportSchedule
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'table_type': ['exact'],
            'due_date': ['exact'],
            'type': ['exact'],
            'status': ['exact'],
            'assignee': ['exact'],
        }


class ImplementationFilter(FilterSet):

    class Meta:
        model = ComponentImplementation
        fields = {
            'month_year': ['exact', 'startswith', 'contains'],
            'forecast_value': ['lt', 'gt'],
            'actual_value': ['lt', 'gt'],
            'current_balance': ['lt', 'gt'],
        }


class TrainingFilter(FilterSet):

    class Meta:
        model = Trainings
        fields = {
            'topic': ['exact', 'startswith', 'contains'],
            'date_of_training': ['exact'],
            'region': ['exact'],
            'district': ['exact'],
            'status': ['exact'],
            'location': ['exact'],
            'notes': ['exact'],
        }
