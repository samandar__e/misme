from django.db import models
from django.db.models import QuerySet


class ComponentManager(models.Manager):

    def get_by_region_overall(self, start_date, end_date, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(implementation_start_date__gte=start_date) if start_date else queryset
        queryset = queryset.filter(implementation_start_date__lte=end_date) if end_date else queryset
        queryset = queryset.filter(regions=region) if region else queryset
        queryset = queryset.filter(districts=district) if district else queryset
        return queryset

    def get_by_region_overall_by_year(self, year, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(implementation_start_date__year=year) if year else queryset
        queryset = queryset.filter(regions=region) if region else queryset
        queryset = queryset.filter(districts=district) if district else queryset
        return queryset
