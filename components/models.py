from django.db import models
from django.utils.translation import ugettext_lazy as _
import datetime

from components.managers.component import ComponentManager
from restapp.models import BaseModel, Attachment
from crm.models import ParticipatingFinancialInstitution, InternationalFinancialInstitute
from directory.models import Employee, Currency, Region, District, SER, FOA, Sector, IFIFP
from projects.models import Projects


class Component(BaseModel):
    """ Component """
    TYPES = [
        ('LOAN', 'Loan'),
        ('GRANT', 'Grant'),
        ('MIXED', 'Mixed'),
    ]
    VARIANT = [
        ('PR', 'Procurement'),
        ('AL', 'Access to loans'),
        ('PM', 'Project management'),
    ]

    name = models.CharField(_('Name'), max_length=225, blank=False)
    project = models.ForeignKey(Projects, related_name='component', on_delete=models.SET_NULL, blank=False, null=True)
    type = models.CharField(max_length=50, verbose_name=_('Type'), choices=TYPES, blank=False, null=True)
    variant = models.CharField(max_length=50, verbose_name=_('Variant'), choices=VARIANT, blank=False)
    parent = models.ForeignKey('self', related_name='component_parent', on_delete=models.CASCADE, null=True, blank=True)
    order_number = models.CharField(_('Order No'), max_length=225, blank=True)
    loan_grant_name = models.CharField(_('Loan facility/Grant Name'), max_length=225, blank=True, null=True)
    implementation_start_date = models.DateField(_('Implementation start date'), max_length=255, blank=True, null=True)
    implementation_end_date = models.DateField(_('Implementation end date'), max_length=255, blank=True, null=True)
    responsible = models.CharField(_('Responsible for implementation'), max_length=225, blank=True, null=True)
    loan_grant_amount = models.FloatField(_('Loan (Grant) Amount'), blank=True, null=True)
    loan_grant_amount_currency = models.ForeignKey(Currency, related_name='loan_grant_amount_currency',
                                                   on_delete=models.SET_NULL, null=True, blank=True)
    repayment_currency = models.ForeignKey(Currency, verbose_name=_('Repayment currency'), on_delete=models.SET_NULL,
                                           null=True)
    brief_description = models.TextField(_('Brief description'), blank=True, null=True)
    source_fundings = models.ManyToManyField(InternationalFinancialInstitute, verbose_name=_('Source of funding'),
                                             blank=True)
    ifi_products = models.ManyToManyField(IFIFP, verbose_name=_('IFI Financial Products'), blank=True)
    sectors = models.ManyToManyField(Sector, verbose_name=_('Financed sector'), blank=True)
    foas = models.ManyToManyField(FOA, verbose_name=_('Field of activity'), blank=True)
    pfis = models.ManyToManyField(ParticipatingFinancialInstitution, verbose_name=_('PFIs'), blank=True)
    regions = models.ManyToManyField(Region, verbose_name=_('Component Loan Grantes Regions'), blank=True)
    districts = models.ManyToManyField(District, verbose_name=_('Component Loan Grantes Districts'), blank=True)

    class Meta:
        verbose_name = _('component')
        verbose_name_plural = _('components')
        db_table = 'components_component'

    def __str__(self):
        return self.name

    objects = ComponentManager()


class ComponentImplementation(BaseModel):
    """ implementation """
    component = models.ForeignKey(Component, related_name='component_implementations', on_delete=models.SET_NULL,
                                  null=True, blank=True)
    month_year = models.DateField(_('Month/ Year'), max_length=255, blank=True, null=True)
    forecast_value = models.FloatField(_('Forecast Value'), blank=False)
    actual_value = models.FloatField(_('Actual Value'), blank=True, null=True)
    current_balance = models.FloatField(_('Current balance'), blank=True, null=True)

    class Meta:
        verbose_name = _('implementation')
        verbose_name_plural = _('implementations')
        db_table = 'components_implementation'

    def __str__(self):
        return f'{self.component}'


class ReportSchedule(BaseModel):
    """ Report schedule """
    TYPES = [
        ('YEARLY', 'Yearly'),
        ('MONTHLY', 'Monthly'),
        ('CUSTOM', 'Custom'),
    ]
    STATUS = [
        ('SCHEDULE', 'Schedule'),
        ('INPROGRESS', 'In progress'),
        ('SUBMITTED', 'Submitted'),
    ]
    TABLE = [
        ('COMPONENT', 'Component'),
        ('PROJECT', 'Project'),
    ]

    table_type = models.CharField(max_length=50, verbose_name=_('Table Type'), choices=TABLE, blank=True, null=True)
    field_id = models.IntegerField(_('Field Id'), blank=True, null=True)
    due_date = models.DateField(_('Due Date'), max_length=255, blank=True, null=True)
    name = models.CharField(_('Name'), max_length=225, blank=False)
    type = models.CharField(max_length=50, verbose_name=_('Type'), choices=TYPES, blank=False)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=False)
    assignee = models.ForeignKey(Employee, verbose_name=_('Assignee'), on_delete=models.SET_NULL, null=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('report schedule')
        verbose_name_plural = _('report schedules')
        db_table = 'components_report_schedule'

    def __str__(self):
        return self.name


class PmCosts(BaseModel):
    """ PM costs """
    component = models.ForeignKey(Component, related_name='pm_costs', on_delete=models.SET_NULL, null=True, blank=True)
    item_name = models.CharField(_('Item name'), max_length=225, blank=False)
    amount_in_uzs = models.FloatField(_('Amount in UZS'), blank=False)
    amount_in_usd = models.DecimalField(_('Amount in USD'), max_digits=20, blank=True, decimal_places=3, null=True)
    exchange_rate = models.FloatField(_('Exchange rate'), blank=True, default=0, null=True)
    payment_document = models.CharField(_('Payment document'), max_length=225, blank=True, null=True)
    date_cr = models.DateField(_('Date'), default=datetime.date.today, max_length=255, blank=False, null=True)
    date_cost = models.DateField(_('Date cost'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('PM cost')
        verbose_name_plural = _('PM costs')
        db_table = 'components_pm_cost'

    def __str__(self):
        return self.item_name


class Trainings(BaseModel):
    """ Trainings """
    STATUS = [
        ('CONDUCTED', 'Conducted'),
        ('SCHEDULED', 'Scheduled'),
    ]
    component = models.ForeignKey(Component, related_name='trainings', on_delete=models.SET_NULL, null=True)
    topic = models.CharField(_('Training topic'), max_length=225, blank=False)
    date_of_training = models.DateField(_('Date of trainings'), max_length=255, blank=False, null=True)
    region = models.ForeignKey(Region, verbose_name=_('Region'), on_delete=models.SET_NULL, null=True, blank=False)
    district = models.ForeignKey(District, verbose_name=_('District'), on_delete=models.SET_NULL, null=True, blank=False)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=True)
    location = models.CharField(_('Location'), max_length=225, blank=True, null=True)
    notes = models.TextField(_('Notes'), blank=True, null=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('training')
        verbose_name_plural = _('trainings')
        db_table = 'components_trainings'

    def __str__(self):
        return self.topic


class Participants(BaseModel):
    """ Participants """
    GENDER = [
        ('MALE', 'Male'),
        ('FEMALE', 'Female'),
    ]

    training = models.ForeignKey(Trainings, related_name='participants', on_delete=models.SET_NULL, null=True)
    full_name = models.CharField(_('Full Name'), max_length=225, blank=False)
    organization = models.CharField(_('Organization'), max_length=225, blank=False)
    gender = models.CharField(max_length=50, verbose_name=_('Gender'), choices=GENDER, blank=True)
    age = models.IntegerField(_('Age'), blank=True, null=True)

    class Meta:
        verbose_name = _('participant')
        verbose_name_plural = _('participants')
        db_table = 'components_trainings_participants'

    def __str__(self):
        return self.full_name


class ProcurementPlan(BaseModel):
    """ Procurement Plan """
    CATEGORY = [
        ('CS', 'Consultant Services'),
        ('GOODS', 'Goods'),
        ('WORKS', 'Works'),
        ('NCS', 'Non-consulting Services'),
    ]
    METHOD = [
        ('QCBS', 'QCBS'),
        ('FBS', 'FBS'),
        ('LCS', 'LCS'),
        ('CQS', 'CQS'),
    ]
    MARKET_APPROACH = [
        ('ON', 'Open | National'),
        ('OI', 'Open | International'),
        ('LN', 'Limited | National'),
        ('LI', 'Limited | International'),
        ('DN', 'Direct |National'),
        ('DI', 'Direct | International'),
    ]
    CONTRACT_TYPE = [
        ('LS', 'Lump sum'),
        ('TK', 'Turnkey'),
        ('PB', 'Performance-based'),
        ('UP', 'Unit price'),
        ('TB', 'Time-based'),
    ]
    REVIEW_TYPE = [
        ('POSTREVIEW', 'Post review'),
        ('PRIORREVIEW', 'Prior review'),
    ]

    component = models.ForeignKey(Component, related_name='procurement_plans', on_delete=models.SET_NULL, blank=True,
                                  null=True)
    package_contract_number = models.CharField(_('Package/Contract No'), max_length=225, blank=False)
    package_name = models.CharField(_('Package name'), max_length=500, blank=False)
    budget = models.FloatField(_('Budget'), blank=True, null=True)
    responsible_person = models.ForeignKey(Employee, verbose_name=_('Responsible person'), on_delete=models.SET_NULL, null=True)
    procurement_category = models.CharField(max_length=50, verbose_name=_('Procurement category'), choices=CATEGORY,
                                            blank=True, null=True)
    procurement_method = models.CharField(max_length=50, verbose_name=_('Procurement method'), choices=METHOD,
                                          blank=True, null=True)
    market_approach = models.CharField(max_length=50, verbose_name=_('Market approach'), choices=MARKET_APPROACH,
                                       blank=True, null=True)
    contract_type = models.CharField(max_length=50, verbose_name=_('Contract type'), choices=CONTRACT_TYPE,
                                     blank=True, null=True)
    review_type = models.CharField(max_length=50, verbose_name=_('Review type'), choices=REVIEW_TYPE, blank=True,
                                   null=True)
    ifi = models.ForeignKey(InternationalFinancialInstitute, verbose_name=_('IFI'), on_delete=models.SET_NULL,
                            blank=True, null=True)
    description = models.TextField(_('Brief description'), blank=True, null=True)
    actual_sum = models.FloatField(_('Actual sum'), blank=True, null=True)

    class Meta:
        verbose_name = _('procurement plan')
        verbose_name_plural = _('procurement plans')
        db_table = 'components_procurement'

    def __str__(self):
        return self.package_contract_number


class Announcements(BaseModel):
    """ Announcements """
    STATUS = [
        ('DRAFT', 'Draft'),
        ('PUBLISHED', 'Published')
    ]

    procurement_plan = models.ForeignKey(ProcurementPlan, related_name='announcements', on_delete=models.SET_NULL,
                                         blank=False, null=True)
    ref_number = models.CharField(_('Ref No'), max_length=225, blank=True)
    send_to = models.EmailField(_('Send to'), max_length=225, blank=False)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=False)
    subject = models.CharField(_('Subject'), max_length=225, blank=True, null=True)
    expiration_date = models.DateField(_('Contract deadline'), max_length=255, blank=True, null=True)
    description = models.TextField(_('Description'), blank=True, null=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('announcement')
        verbose_name_plural = _('announcements')
        db_table = 'components_procurement_announcements'

    def __str__(self):
        return self.ref_number


class Contracts(BaseModel):
    """ Contracts """
    STATUS = [
        ('DRAFT', 'Draft'),
        ('PUBLISHED', 'Published')
    ]
    PAY_METHOD = [
        ('LS', 'Lum sum'),
        ('TK', 'Turnkey'),
        ('PB', 'Performance-based'),
        ('UP', 'Unit price'),
        ('TB', 'Time-based'),
    ]
    IMP_STATUS = [
        ('INPROGRESS', 'In-progress'),
        ('COMPLETED', 'Completed'),
    ]

    procurement_plan = models.ForeignKey(ProcurementPlan, related_name='contracts', on_delete=models.SET_NULL,
                                         blank=True, null=True)
    ref_number = models.CharField(_('Contract Ref No'), max_length=225, blank=False)
    contractor_name = models.CharField(_('Contractor name'), max_length=225, blank=False)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=True)
    due_date = models.DateField(_('Due date'), max_length=255, blank=True, null=True)
    date_of_signature = models.DateField(_('Date of signature'), max_length=255, blank=True, null=True)
    amount_in_uzs = models.FloatField(_('Contract amount in UZS'), blank=True, null=True)
    amount_in_usd = models.DecimalField(_('Contract amount in USD'), max_digits=20, blank=True, decimal_places=3,
                                        null=True)
    exchange_rate = models.FloatField(_('Exchange rate'), default=0, blank=True)
    payment_method = models.CharField(max_length=50, verbose_name=_('Payment method'), choices=PAY_METHOD, blank=True)
    implementation_status = models.CharField(max_length=50, verbose_name=_('Implementation status'), choices=IMP_STATUS,
                                             blank=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('contract')
        verbose_name_plural = _('contracts')
        db_table = 'components_procurement_contracts'

    def __str__(self):
        return self.contractor_name


class Amendments(BaseModel):
    """ Amendments """
    STATUS = [
        ('CANCELLED', 'Cancelled'),
        ('SIGNED', 'Signed'),
    ]
    PAY_METHOD = [
        ('LS', 'Lum sum'),
        ('TK', 'Turnkey'),
        ('PB', 'Performance-based'),
        ('UP', 'Unit price'),
        ('TB', 'Time-based'),
    ]

    contract_number = models.ForeignKey(Contracts, related_name='amendments', on_delete=models.SET_NULL,
                                        null=True)
    amendment_number = models.CharField(_('Amendment Ref No'), max_length=225, blank=False)
    reason_for_amendment = models.CharField(_('Reason for amendment'), max_length=225, blank=False)
    amendment_amount_in_uzs = models.FloatField(_('Contract amount in UZS'), blank=True, null=True)
    amendment_amount_in_usd = models.DecimalField(_('Contract amount in USD'), max_digits=20, blank=True,
                                                  decimal_places=3, null=True)
    exchange_rate = models.FloatField(_('Exchange rate'), blank=True, default=0, null=True)
    amendment_due_date = models.DateField(_('Due date'), max_length=255, blank=True, null=True)
    amendment_date_of_signature = models.DateField(_('Date of signature'), max_length=255, blank=True, null=True)
    status = models.CharField(max_length=50, verbose_name=_('Status'), choices=STATUS, blank=True)
    amendment_payment_method = models.CharField(max_length=50, verbose_name=_('Payment method'), choices=PAY_METHOD,
                                                blank=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('amendment')
        verbose_name_plural = _('amendments')
        db_table = 'components_contract_amendment'

    def __str__(self):
        return self.amendment_number


class PaymentsTranche(BaseModel):
    """ Payments breakdown """
    contract = models.ForeignKey(Contracts, related_name='payments_breakdowns', on_delete=models.SET_NULL,
                                 null=True)
    name = models.CharField(_('Tranche name'), max_length=225, blank=False)
    planned_amount = models.FloatField(_('Planted amount'), blank=False)
    paid_amount_no = models.FloatField(_('Paid amount No'), blank=True, null=True)

    class Meta:
        verbose_name = _('payments breakdown')
        verbose_name_plural = _('payments breakdowns')
        db_table = 'components_contract_payments'

    def __str__(self):
        return self.name
