from rest_framework import serializers

from components.models import ReportSchedule, Component, ComponentImplementation, PmCosts, Trainings, Participants, \
    ProcurementPlan, Announcements, Contracts, Amendments, PaymentsTranche
from directory.models import ExchangeRate
from directory.serializers import RelatedRegionSerializer, RelatedDistrictSerializer
from projects.models import Projects
from reports.serializers.network_schedule import SubprojectSerializer
from rest_framework.utils import json

from subproject.models import Subproject


class LoanGrantAmountField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.loan_grant_amount_currency.id if instance.loan_grant_amount_currency else None
        return {"number": instance.loan_grant_amount, "currency": currency}

    def to_internal_value(self, data):
        return {"loan_grant_amount": data["number"], "loan_grant_amount_currency_id": data["currency"]}


class ImplementationPeriodField(serializers.Field):
    def to_representation(self, instance):
        return {"start": instance.implementation_start_date, "end": instance.implementation_end_date}

    def to_internal_value(self, data):
        return {"implementation_start_date": data["start"], "implementation_end_date": data["end"]}


class LoanGrantAmountCountField(serializers.Field):
    def to_representation(self, instance):
        components = Component.objects.all().filter(parent=instance.id)
        serializer_comp = ComponentAmountSerializer(components, many=True)
        components_dict = json.loads(json.dumps(serializer_comp.data))
        found_drawn_components = 0
        for i in range(0, len(components_dict)):
            found_drawn_components = found_drawn_components + int(components_dict[i]['loan_grant_amount'] or 0)
        subprojects = Subproject.objects.all().filter(component=instance.id)
        serializer_sub = SubprojectSerializer(subprojects, many=True)
        subprojects_dict = json.loads(json.dumps(serializer_sub.data))
        found_drawn_subprojects = 0
        for i in range(0, len(subprojects_dict)):
            found_drawn_subprojects = found_drawn_subprojects + int(subprojects_dict[i]['total_cost'] or 0)
        found_drawn = found_drawn_components + found_drawn_subprojects
        found_remained = int(instance.loan_grant_amount or 0) - found_drawn
        found_percent = float("{:.0f}".format(
            (found_drawn / instance.loan_grant_amount) * 100)) if int(instance.loan_grant_amount or 0) != 0 else 0
        return {"found_drawn": found_drawn, "found_remained": found_remained, "found_percent": found_percent}


# class ProjectNumberField(serializers.Field):
#     def to_representation(self, instance):
#         project = Projects.objects.get(id=36)
#         return project.projectNumber


class ExchangeRateListSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangeRate
        fields = ('id', 'code', 'currency', 'nominal', 'rate', 'diff', 'date', 'symbol')


class ComponentImplementationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComponentImplementation
        fields = ('id', 'component', 'month_year', 'forecast_value', 'actual_value', 'current_balance')


class AnnouncementsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Announcements
        fields = ('id', 'procurement_plan', 'ref_number', 'send_to', 'status', 'subject', 'expiration_date',
                  'description')


class AmendmentsCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amendments
        fields = ('id', 'contract_number', 'amendment_number', 'reason_for_amendment', 'amendment_amount_in_uzs',
                      'amendment_amount_in_usd', 'exchange_rate', 'amendment_due_date', 'amendment_date_of_signature',
                  'status', 'amendment_payment_method')

    def create(self, validated_data, null=None):
        amendments = Amendments.objects.create(**validated_data)
        if amendments.amendment_date_of_signature is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=amendments.amendment_date_of_signature)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                amendments.exchange_rate = exchange_dict[0]['rate']
        else:
            exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
            serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
            exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
            if len(exchange_dict_null) != 0:
                amendments.exchange_rate = exchange_dict_null[0]['rate']
        if amendments.amendment_amount_in_uzs != null and amendments.exchange_rate != null:
            amendments.amendment_amount_in_usd = amendments.amendment_amount_in_uzs / amendments.exchange_rate
        amendments.save()
        return amendments


class AmendmentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Amendments
        fields = ('id', 'contract_number', 'amendment_number', 'reason_for_amendment',
                  'amendment_amount_in_uzs', 'amendment_amount_in_usd', 'exchange_rate', 'amendment_due_date',
                  'amendment_date_of_signature', 'status', 'amendment_payment_method')

    def update(self, instance, validated_data, null=None):
        instance.contract_number = validated_data.get('contract_number', instance.contract_number)
        instance.amendment_number = validated_data.get('amendment_number', instance.amendment_number)
        instance.reason_for_amendment = validated_data.get('reason_for_amendment', instance.reason_for_amendment)
        instance.amendment_amount_in_uzs = validated_data.get('amendment_amount_in_uzs',
                                                              instance.amendment_amount_in_uzs)
        instance.amendment_due_date = validated_data.get('amendment_due_date', instance.amendment_due_date)
        instance.amendment_date_of_signature = validated_data.get('amendment_date_of_signature',
                                                                  instance.amendment_date_of_signature)
        if instance.amendment_date_of_signature is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=instance.amendment_date_of_signature)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                instance.exchange_rate = exchange_dict[0]['rate']
        else:
            exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
            serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
            exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
            if len(exchange_dict_null) != 0:
                instance.exchange_rate = exchange_dict_null[0]['rate']

        if instance.amendment_amount_in_uzs != null and instance.exchange_rate != null and instance.exchange_rate != 0:
            instance.amendment_amount_in_usd = instance.amendment_amount_in_uzs / instance.exchange_rate
        instance.status = validated_data.get('status', instance.status)
        instance.amendment_payment_method = validated_data.get('amendment_payment_method',
                                                               instance.amendment_payment_method)
        instance.save()
        return instance


class PaymentsTrancheSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentsTranche
        fields = ('id', 'contract', 'name', 'planned_amount', 'paid_amount_no')


class ProcurementPlanDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProcurementPlan
        fields = ('id', 'package_contract_number', 'package_name',)


class ContractsSerializer(serializers.ModelSerializer):
    amendments = AmendmentsSerializer(many=True, read_only=True)
    payments_breakdowns = PaymentsTrancheSerializer(many=True, read_only=True)
    procurement_plan_detail = ProcurementPlanDetailSerializer(source='procurement_plan', read_only=True)

    class Meta:
        model = Contracts
        fields = ('id', 'procurement_plan', 'procurement_plan_detail', 'ref_number', 'contractor_name', 'status', 'due_date', 'date_of_signature',
                  'amount_in_uzs', 'amount_in_usd', 'exchange_rate', 'payment_method', 'implementation_status',
                  'amendments', 'payments_breakdowns')

    def create(self, validated_data, null=None):
        contracts = Contracts.objects.create(**validated_data)
        if contracts.date_of_signature is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=contracts.date_of_signature)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                contracts.exchange_rate = exchange_dict[0]['rate']
        else:
            exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
            serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
            exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
            if len(exchange_dict_null) != 0:
                contracts.exchange_rate = exchange_dict_null[0]['rate']
        if contracts.amount_in_uzs != null and contracts.exchange_rate != null:
            contracts.amount_in_usd = contracts.amount_in_uzs / contracts.exchange_rate
        contracts.save()
        return contracts

    def update(self, instance, validated_data, null=None):

        instance.procurement_plan = validated_data.get('procurement_plan', instance.procurement_plan)
        instance.ref_number = validated_data.get('ref_number', instance.ref_number)
        instance.contractor_name = validated_data.get('contractor_name', instance.contractor_name)
        instance.status = validated_data.get('status', instance.status)
        instance.due_date = validated_data.get('due_date', instance.due_date)
        instance.date_of_signature = validated_data.get('date_of_signature', instance.date_of_signature)
        instance.amount_in_uzs = validated_data.get('amount_in_uzs', instance.amount_in_uzs)

        if instance.date_of_signature is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=instance.date_of_signature)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                instance.exchange_rate = exchange_dict[0]['rate']
        else:
            exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
            serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
            exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
            if len(exchange_dict_null) != 0:
                instance.exchange_rate = exchange_dict_null[0]['rate']

        if instance.amount_in_uzs != null and instance.exchange_rate != null and instance.exchange_rate != 0:
            instance.amount_in_usd = instance.amount_in_uzs / instance.exchange_rate
        instance.payment_method = validated_data.get('payment_method', instance.payment_method)
        instance.implementation_status = validated_data.get('implementation_status', instance.implementation_status)
        instance.save()
        return instance


class ProcurementPlanSerializer(serializers.ModelSerializer):
    announcements = AnnouncementsSerializer(many=True, read_only=True)
    contracts = ContractsSerializer(many=True, read_only=True)

    class Meta:
        model = ProcurementPlan
        fields = ('id', 'component', 'package_contract_number', 'package_name', 'budget', 'actual_sum', 'responsible_person',
                  'procurement_category', 'procurement_method', 'market_approach', 'contract_type', 'review_type',
                  'ifi', 'description', 'announcements', 'contracts')


class PmCostsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PmCosts
        fields = ('id', 'component', 'item_name', 'amount_in_uzs', 'amount_in_usd', 'exchange_rate', 'payment_document',
                  'date_cr', 'date_cost')

    def create(self, validated_data, null=None):
        pm_cost = PmCosts.objects.create(**validated_data)

        if pm_cost.date_cr is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=pm_cost.date_cr)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                pm_cost.exchange_rate = exchange_dict[0]['rate']
            else:
                exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
                serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
                exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
                if len(exchange_dict_null) != 0:
                    pm_cost.exchange_rate = exchange_dict_null[0]['rate']

        if pm_cost.amount_in_uzs != null and pm_cost.exchange_rate != null and pm_cost.exchange_rate != 0:
            pm_cost.amount_in_usd = pm_cost.amount_in_uzs / pm_cost.exchange_rate
        pm_cost.save()
        return pm_cost

    def update(self, instance, validated_data, null=None):
        instance.component = validated_data.get('component', instance.component)
        instance.item_name = validated_data.get('item_name', instance.item_name)
        instance.amount_in_uzs = validated_data.get('amount_in_uzs', instance.amount_in_uzs)
        instance.payment_document = validated_data.get('payment_document', instance.payment_document)
        instance.date_cr = validated_data.get('date_cr', instance.date_cr)
        instance.date_cost = validated_data.get('date_cost', instance.date_cost)

        if instance.date_cr is not None:
            exchange = ExchangeRate.objects.all().filter(symbol='USD', date=instance.date_cr)
            serializer_comp = ExchangeRateListSerializer(exchange, many=True)
            exchange_dict = json.loads(json.dumps(serializer_comp.data))
            if len(exchange_dict) != 0:
                instance.exchange_rate = exchange_dict[0]['rate']
            else:
                exchange_null = ExchangeRate.objects.filter(symbol='USD').order_by('-date')[:1]
                serializer_comp_null = ExchangeRateListSerializer(exchange_null, many=True)
                exchange_dict_null = json.loads(json.dumps(serializer_comp_null.data))
                if len(exchange_dict_null) != 0:
                    instance.exchange_rate = exchange_dict_null[0]['rate']

        if instance.amount_in_uzs != null and instance.exchange_rate != null and instance.exchange_rate != 0:
            instance.amount_in_usd = instance.amount_in_uzs / instance.exchange_rate
        instance.save()
        return instance


class ParticipantsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participants
        fields = ('id', 'training', 'full_name', 'organization', 'gender', 'age')


class TrainingsSerializer(serializers.ModelSerializer):
    participants = ParticipantsSerializer(many=True, read_only=True)
    region_detail = RelatedRegionSerializer(source='region', read_only=True)
    district_detail = RelatedDistrictSerializer(source='district', read_only=True)

    class Meta:
        model = Trainings
        fields = ('id', 'component', 'topic', 'date_of_training', 'region', 'region_detail', 'district', 'district_detail',
                  'status', 'location', 'notes', 'participants')


class ProjectsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Projects
        fields = (
            'id',
            'name',
            'projectNumber',
        )


class ComponentSerializer(serializers.ModelSerializer):
    component_implementations = ComponentImplementationSerializer(many=True, read_only=True)
    procurement_plans = ProcurementPlanSerializer(many=True, read_only=True)
    pm_costs = PmCostsSerializer(many=True, read_only=True)
    trainings = TrainingsSerializer(many=True, read_only=True)
    loan_grant_amount = LoanGrantAmountField(source='*', required=False)
    created_by = serializers.SerializerMethodField(method_name='get_created_user_name', read_only=True)
    project_detail = ProjectsSerializer(source='project', read_only=True)
    created_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)
    implementationPeriod = ImplementationPeriodField(source='*', required=False)
    loan_grant_amount_count = LoanGrantAmountCountField(source='*', required=False)

    class Meta:
        model = Component
        fields = ('id', 'name', 'project', 'project_detail', 'type', 'variant', 'parent', 'order_number', 'loan_grant_name', 'implementationPeriod',
                  'responsible', 'loan_grant_amount', 'repayment_currency', 'brief_description', 'source_fundings',
                  'ifi_products', 'sectors', 'foas', 'pfis', 'regions', 'districts', 'component_implementations',
                  'procurement_plans', 'pm_costs', 'trainings', 'loan_grant_amount_count', 'created_by', 'created_time')

    def get_created_user_name(self, instance):
        return instance.created_by.username if instance.created_by else None


class ComponentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'project', 'type', 'variant', 'parent', 'order_number', 'name', 'parent')


class ComponentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'project', 'type', 'variant', 'parent', 'order_number', 'name')
        
    def create(self, validated_data):
        component = Component.objects.create(**validated_data)
        if component.parent is not None:
            number_parent = component.parent
        else:
            number_parent = Component.objects.get(id=component.id)
        components = Component.objects.all().filter(project=component.project)
        serializer_comp = ComponentListSerializer(components, many=True)
        components_dict = json.loads(json.dumps(serializer_comp.data))
        number_null = []
        number_not_null = []
        for i in range(0, len(components_dict)):
            if components_dict[i]['parent'] is None:
                number_null.append(components_dict[i]['order_number'])
            elif components_dict[i]['parent'] == number_parent.id:
                number_not_null.append(components_dict[i]['order_number'])
        if component.parent is None:
            component.order_number = (0 if max(number_null) == '' else int(max(number_null))) + 1
        else:
            if max(number_not_null, default=0) != '':
                float_parse = max(number_not_null)
                a, b = map(int, float_parse.split(".", 1))
                component.order_number = float(str(a) + '.' + str(b+1))
            else:
                component.order_number = float(str(number_parent.order_number) + '.1')
        component.save()
        return component


class ComponentAmountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Component
        fields = ('id', 'project', 'loan_grant_amount')


class ReportScheduleCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportSchedule
        fields = ('id', 'table_type', 'field_id', 'due_date', 'name', 'type', 'status', 'assignee')


class ReportScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportSchedule
        fields = ('id', 'table_type', 'field_id', 'due_date', 'name', 'type', 'status', 'assignee')

