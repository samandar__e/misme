from modeltranslation.translator import register, TranslationOptions
from .models import Component, ReportSchedule


@register(Component)
class ComponentTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ReportSchedule)
class ReportScheduleTranslationOptions(TranslationOptions):
    fields = ('name',)
