from django.urls import re_path, path

from projects.views.loan_lacility import LoanFacilityView, LoanFacilityDetailView
from projects.views.project_indicator import ProjectIndicatorsView, ProjectIndicatorsDelete, ProjectIndicatorsDeleteOne
from projects.views.project_indicator_value import ProjectIndicatorsValueView, ProjectIndicatorsValueDetailView
from projects.views.projects import ProjectsView, ProjectsDetailView, ProjectUpdateView
from projects.views.project_documentation import ProjectDocumentationsView, ProjectDocumentationsDetailView
from projects.views.project_chronolgy import ProjectChronologiesView, ProjectChronologyDetailView
from projects.views.project_implementation import ProjectImplementationView, ProjectImplementationDetailView
from projects.views.enums import TranchesTypeList, TranchesTransList, TranchesCounterList, ProjectStatusList, \
    IndicatorsTypeList, IndicatorsCollectionTypeList, ProjectControlStatusList
from projects.views.tranches import ProjectTranchesView, ProjectTranchesDetailView
from projects.views.report_schedule import ReportScheduleView, ReportScheduleDetailView

urlpatterns = [
    re_path(r'^project/$', ProjectsView.as_view(), name='projects_view'),
    path('project/<int:pk>', ProjectsDetailView.as_view(), name='project_detail_view'),
    path('project/<int:pk>/status', ProjectUpdateView.as_view(), name='project_status_update'),

    re_path(r'^project-documentation/$', ProjectDocumentationsView.as_view(), name='project_documentations_view'),
    path('project-documentation/<int:pk>', ProjectDocumentationsDetailView.as_view(),
         name='project_documentation_detail_view'),

    re_path(r'^project-chronology/$', ProjectChronologiesView.as_view(), name='project_chronologies_view'),
    path('project-chronology/<int:pk>', ProjectChronologyDetailView.as_view(), name='project_chronology_detail_view'),

    re_path(r'^implementation/$', ProjectImplementationView.as_view(), name='implementation_view'),
    path('implementation/<int:pk>', ProjectImplementationDetailView.as_view(), name='implementation_detail_view'),

    re_path(r'^enum-tranches-type/$', TranchesTypeList.as_view(), name='tranches_type_view'),
    re_path(r'^enum-tranches-trans/$', TranchesTransList.as_view(), name='tranches_trans_view'),
    re_path(r'^enum-tranches-counter/$', TranchesCounterList.as_view(), name='tranches_trans_view'),
    re_path(r'^enum-project-status/$', ProjectStatusList.as_view(), name='project_status_list'),
    re_path(r'^enum-project-control-status/$', ProjectControlStatusList.as_view(), name='project_control_status_list'),
    re_path(r'^enum-indicators-type/$', IndicatorsTypeList.as_view(), name='indicators_type_view'),
    re_path(r'^enum-indicators-collection-type/$', IndicatorsCollectionTypeList.as_view(),
            name='indicators_collection_type_view'),


    re_path(r'^project-tranches/$', ProjectTranchesView.as_view(), name='tranches_view'),
    path('project-tranches/<int:pk>', ProjectTranchesDetailView.as_view(), name='tranches_detail_view'),

    re_path(r'^loan-facility/$', LoanFacilityView.as_view(), name='loan_facility_view'),
    path('loan-facility/<int:pk>', LoanFacilityDetailView.as_view(), name='loan_facility_detail_view'),

    re_path(r'^project-schedule/$', ReportScheduleView.as_view(), name='schedule_view'),
    path('project-schedule/<int:pk>', ReportScheduleDetailView.as_view(), name='schedule_detail_view'),

    re_path(r'^project-indicators/$', ProjectIndicatorsView.as_view(), name='indicators_view'),
    path('project-indicators/<int:pk>', ProjectIndicatorsDelete.as_view(), name='indicators_detail_view'),
    path('project-indicators/<int:pk>/one', ProjectIndicatorsDeleteOne.as_view(), name='indicators_delete_view'),

    re_path(r'^project-indicators-value/$', ProjectIndicatorsValueView.as_view(), name='indicators_value_view'),
    path('project-indicators-value/<int:pk>', ProjectIndicatorsValueDetailView.as_view(),
         name='indicators_value_detail_view'),
]
