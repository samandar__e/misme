from django.db import models
from django.utils.translation import ugettext_lazy as _
from crm.models import ParticipatingFinancialInstitution, InternationalFinancialInstitute
from projects.managers.project import ProjectManager
from restapp.models import BaseModel, Attachment
from directory.models import Currency, Region, District, Employee, Indicator


class Projects(BaseModel):
    STATUS = (
        ('NEW', _('New')),
        ('ONGOING', _('Ongoing')),
        ('COMPLETED', _('Completed')),
    )
    CONTROL_STATUS = (
        ('APPROVED', _('Approved')),
        ('UNAPPROVED', _('Unapproved')),
    )

    name = models.CharField(_('Project Name'), max_length=255, blank=False)
    pdo = models.CharField(_('The project development objective(PDO)'), max_length=255, blank=True, null=True)
    projectNumber = models.CharField(_('Project ID'), max_length=255, blank=False, unique=True)
    govResolution = models.CharField(_('Gov Resolution'), max_length=500, blank=True)
    description = models.TextField(_('Project main objective'), blank=True)
    overallProjectCost = models.FloatField(_('Overall project'), blank=True, null=True)
    overallProjectCostCurrency = models.ForeignKey(Currency, related_name='overall_cost',
                                                   on_delete=models.SET_NULL, null=True, blank=True)
    localGovernmentContribution = models.FloatField(_('Local government contribution'), blank=True, null=True)
    localGovernmentContributionCurrency = models.ForeignKey(Currency, related_name='projects_local_government',
                                                            on_delete=models.SET_NULL, null=True, blank=True)
    beneficiaryFunds = models.FloatField(_('Beneficiary Funds'), blank=True, null=True)
    beneficiaryFundsCurrency = models.ForeignKey(Currency, related_name='projects_beneficiary_founds',
                                                 on_delete=models.SET_NULL, null=True, blank=True)

    headOfPiu = models.CharField(_('Head of PIU'), max_length=255, blank=True)
    piuInTable = models.FloatField(_('PIU employees in manning table'), blank=True, null=True)
    piuEmployeesActual = models.FloatField(_('PIU employees actual'), blank=True, null=True)
    dateSigned = models.DateField(_('Date loan agreement signed'), max_length=255, blank=True, null=True)
    dateClosing = models.DateField(_('Date loan agreement closing'), max_length=255, blank=True, null=True)
    availableStudy = models.TextField(_('Availability of feasibility study'), blank=True)
    overallCount = models.IntegerField(_('Overall'), blank=True, null=True)
    menCount = models.IntegerField(_('Men'), blank=True, null=True)
    womenCount = models.IntegerField(_('Women'), blank=True, null=True)
    implementationStartDate = models.DateField(_('Implementation start date'), max_length=255, blank=True, null=True)
    implementationEndDate = models.DateField(_('Implementation end date'), max_length=255, blank=True, null=True)
    pfis = models.ManyToManyField(ParticipatingFinancialInstitution, blank=True)
    regions = models.ManyToManyField(Region, blank=True)
    districts = models.ManyToManyField(District, blank=True)
    status = models.CharField(max_length=50, choices=STATUS, default='NEW', verbose_name=_('Status'))
    control_status = models.CharField(max_length=50, choices=CONTROL_STATUS, default='UNAPPROVED', verbose_name=_('Control status'))

    class Meta:
        verbose_name = _('project')
        verbose_name_plural = _('projects')

    def __str__(self):
        return self.name

    objects = ProjectManager()


class ProjectDocumentation(BaseModel):
    project = models.ForeignKey(Projects, verbose_name=_('Project'), on_delete=models.SET_NULL, blank=True, null=True)
    name = models.CharField(_('Name'), max_length=255, blank=True)
    file = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('Project Documentation')
        verbose_name_plural = _('Project Documentations')
        db_table = 'projects_project_documentation'


class ProjectChronology(BaseModel):
    project = models.ForeignKey(Projects, related_name='chronology', on_delete=models.SET_NULL, blank=True, null=True)
    text = models.TextField(_('Event'), blank=False, null=True,)
    date = models.DateField(_('Date'), max_length=255, blank=False, null=True)

    class Meta:
        verbose_name = _('Project Chronology')
        verbose_name_plural = _('Project Chronologies')
        db_table = 'projects_project_chronology'


class ProjectImplementation(BaseModel):
    project = models.ForeignKey(Projects, verbose_name=_('Project'), on_delete=models.SET_NULL, blank=True, null=True)
    date = models.DateField(_('Report Date'), max_length=255, null=True, blank=True)
    activitiesImplemented = models.TextField(_('Activities implemented'), blank=True, null=True,)
    problems = models.TextField(_('Problems'), blank=True, null=True)

    class Meta:
        verbose_name = _('Project Implementation')
        verbose_name_plural = _('Project Implementations')
        db_table = 'projects_project_implementation'


class ProjectTranches(BaseModel):
    TYPE = [
        ('INFLOW', _('INFLOW')),
        ('OUTFLOW', _('OUTFLOW')),
    ]
    TYPE_COUNTER = [
        ('IFI', _('IFI')),
        ('PFI', _('PFI')),
        ('MINISTRY', _('MINISTRY')),
    ]
    TYPE_TRANS = [
        ('LOAN', _('LOAN')),
        ('GRANT', _('GRANT'))
    ]
    project = models.ForeignKey(Projects, verbose_name=_('Project'), on_delete=models.SET_NULL, blank=True, null=True)
    text = models.TextField(_('Tranche Text'), blank=False, null=True)
    type = models.CharField(_('Tranche type'), max_length=50, choices=TYPE, default='INFLOW', blank=False)
    counterpartType = models.CharField(_('Counterpart type'),
                                       max_length=50, choices=TYPE_COUNTER, default='IFI', blank=False)
    transferredFundsType = models.CharField(_('Transferred funds type'),
                                            max_length=50, choices=TYPE_TRANS, default='LOAN', blank=False)
    counterpartName = models.CharField(_('Counterpart name'), max_length=255, blank=True)
    transferAmount = models.FloatField(_('Transfer amount'), blank=True, null=True)
    transferAmountCurrency = models.ForeignKey(Currency, verbose_name=_('Transfer Currency'),
                                               related_name='transfer_amount_currency', on_delete=models.SET_NULL,
                                               blank=True, null=True)

    class Meta:
        verbose_name = _('Project Tranche')
        verbose_name_plural = _('Project Tranches')
        db_table = 'projects_project_tranches'


class ProjectIndicators(BaseModel):
    COLLECTION_TYPE = [
        ('Yearly', _('Yearly')),
        ('Monthly', _('Monthly')),
    ]
    TYPE = [
        ('PDO_INDICATORS', _('PDO INDICATORS')),
        ('INTERMEDIATE_RESULTS_INDICATORS', _('INTERMEDIATE RESULTS INDICATORS')),
    ]

    project = models.ForeignKey(Projects, verbose_name=_('Project'), on_delete=models.SET_NULL, blank=False, null=True)
    startDate = models.DateField(_('Start Date'), max_length=255, blank=True, null=True)
    endDate = models.DateField(_('End Date'), max_length=255, blank=True, null=True)
    collectionType = models.CharField(_('Data collection frequency'), max_length=50, choices=COLLECTION_TYPE, blank=False)
    type = models.CharField(_('Type of indicator'), max_length=50, choices=TYPE, blank=False)
    indicator = models.ForeignKey(Indicator, verbose_name=_('Indicator'), on_delete=models.SET_NULL, blank=False, null=True)

    class Meta:
        verbose_name = _('Project Indicator')
        verbose_name_plural = _('Project Indicators')
        db_table = 'projects_indicators'


class ProjectIndicatorsValue(BaseModel):
    indicator = models.ForeignKey(ProjectIndicators, related_name='indicators_value', on_delete=models.SET_NULL, blank=True,
                                  null=True)
    planValue = models.FloatField(_('Plan Value'), blank=True, null=True)
    factualValue = models.FloatField(_('Factual Value'), blank=True, null=True)
    date = models.DateField(_('Date'), max_length=255, blank=True, null=True)
    sorting = models.IntegerField(_('Sorting'), blank=True, null=True)

    class Meta:
        verbose_name = _('Project Indicator value')
        verbose_name_plural = _('Project Indicator values')
        db_table = 'projects_indicators_values'


class LoanFacility(BaseModel):
    project = models.ForeignKey(Projects, related_name='loanFacility', on_delete=models.SET_NULL, blank=True, null=True)
    ifi = models.ForeignKey(InternationalFinancialInstitute, on_delete=models.SET_NULL, blank=True, null=True)
    sdrValue = models.FloatField(_('SDR value'), blank=True, null=True)
    sdrValueCurrency = models.ForeignKey(Currency, related_name='projects_sdr_value',
                                         on_delete=models.SET_NULL, null=True, blank=True)
    currencyOne = models.FloatField(_('Currency One'), blank=True, null=True)
    currencyOneCurrency = models.ForeignKey(Currency, verbose_name=_('Currency One Cur'),
                                            related_name='currency_one_cur',
                                            on_delete=models.SET_NULL, blank=True, null=True)
    currencyTwo = models.FloatField(_('Currency Two'), blank=True, null=True)
    currencyTwoCurrency = models.ForeignKey(Currency, verbose_name=_('Currency Two Cur'),
                                            related_name='currency_two_cur',
                                            on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = _('Loan facility')
        verbose_name_plural = _('Loan facilitys')
        db_table = 'projects_loan_facility'

