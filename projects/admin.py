from django.contrib import admin

# Register your models here.
from projects.models import Projects, ProjectDocumentation, ProjectChronology, ProjectImplementation, ProjectTranches, \
    ProjectIndicators, ProjectIndicatorsValue


@admin.register(Projects)
class ProjectsAdmin(admin.ModelAdmin):
    list_display = ('name', 'projectNumber', 'description')
    fields = (
        'name', 'projectNumber', 'govResolution', 'description', 'overallProjectCost', 'localGovernmentContribution',
        'beneficiaryFunds', 'sdrValue', 'headOfPiu', 'piuInTable', 'piuEmployeesActual', 'dateSigned', 'dateClosing',
        'availableStudy', 'overallCount', 'menCount', 'womenCount', 'implementationStartDate', 'implementationEndDate',
        'ifi')
    search_fields = ('name', 'projectNumber', 'govResolution')


@admin.register(ProjectDocumentation)
class ProjectDocumentationAdmin(admin.ModelAdmin):
    list_display = ('project', 'name')
    fields = ('project', 'name', 'file')
    search_fields = ('project',)


@admin.register(ProjectChronology)
class ProjectChronologyAdmin(admin.ModelAdmin):
    list_display = ('project', 'text')
    fields = ('project', 'text')
    search_fields = ('project',)


@admin.register(ProjectImplementation)
class ProjectImplementationAdmin(admin.ModelAdmin):
    list_display = ('date', 'problems', 'activitiesImplemented')
    fields = ('date', 'problems', 'activitiesImplemented')
    search_fields = ('date', 'problems', 'activitiesImplemented')


@admin.register(ProjectTranches)
class ProjectTranchesAdmin(admin.ModelAdmin):
    list_display = ('text', 'counterpartName', 'transferAmount')
    fields = ('text', 'counterpartName', 'transferAmount')
    search_fields = ('text', 'counterpartName', 'transferAmount')


@admin.register(ProjectIndicators)
class ProjectIndicatorsAdmin(admin.ModelAdmin):
    list_display = ('startDate', 'endDate', 'collectionType')
    fields = ('startDate', 'endDate', 'collectionType')
    search_fields = ('startDate', 'endDate', 'collectionType')


@admin.register(ProjectIndicatorsValue)
class ProjectIndicatorsValueAdmin(admin.ModelAdmin):
    list_display = ('planValue', 'factualValue', 'sorting')
    fields = ('planValue', 'factualValue', 'sorting')
    search_fields = ('planValue', 'factualValue', 'sorting')
