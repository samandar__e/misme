from modeltranslation.translator import register, TranslationOptions
from .models import Projects


@register(Projects)
class ProjectTranslationOptions(TranslationOptions):
    fields = ('name', 'govResolution')
