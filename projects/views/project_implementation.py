from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework import filters, status
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from projects.models import ProjectImplementation
from projects.serializers import ProjectImplementationSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProjectImplementationView(ListCreateAPIView):
    serializer_class = ProjectImplementationSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filterset_fields = ['project']
    search_fields = ('name',)
    ordering = ['pk']

    def get_queryset(self):
        return ProjectImplementation.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProjectImplementationDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectImplementationSerializer

    def get(self, request, pk):
        instance = get_object_or_404(ProjectImplementation, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ProjectImplementation, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ProjectImplementation, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
