from rest_framework.generics import ListCreateAPIView, DestroyAPIView, get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.utils import json

from projects.models import ProjectIndicators, ProjectIndicatorsValue
from projects.serializers import ProjectIndicatorsCreateSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProjectIndicatorsView(ListCreateAPIView):
    serializer_class = ProjectIndicatorsCreateSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ['project']
    search_fields = ('startDate',)
    ordering = ['pk']

    def get_queryset(self):
        return ProjectIndicators.objects.all()

    def post(self, request):
        pdo = request.data['pdo']
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        project = serializer.validated_data.get('project', None)
        if pdo != "":
            project.pdo = pdo
            project.save()

        if project.implementationStartDate is not None and project.implementationEndDate is not None:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status.HTTP_201_CREATED)
        message = {'startDate': 'Это значение должно быть заполнено!',
                   'endDate': 'Это значение должно быть заполнено!'}
        return Response({'message': message}, status=HTTP_400_BAD_REQUEST)


class ProjectIndicatorsDelete(DestroyAPIView):
    def delete(self, request, pk):
        project_indicators = ProjectIndicators.objects.all().filter(project=pk)
        serializer = ProjectIndicatorsCreateSerializer(project_indicators, many=True)
        project_indicators_dict = json.loads(json.dumps(serializer.data))
        for i in range(0, len(project_indicators_dict)):
            id_indicator = project_indicators_dict[i]['id']
            ProjectIndicatorsValue.objects.filter(indicator=id_indicator).delete()
        ProjectIndicators.objects.filter(project=pk).delete()

        return Response(nonContent(), status.HTTP_204_NO_CONTENT)


class ProjectIndicatorsDeleteOne(DestroyAPIView):

    def delete(self, request, pk):
        instance = get_object_or_404(ProjectIndicators, id=pk)
        ProjectIndicatorsValue.objects.filter(indicator=pk).delete()
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)