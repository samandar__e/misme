from rest_framework.response import Response
from rest_framework.views import APIView

from projects.models import ProjectTranches, Projects, ProjectIndicators
from restapp.utils.enumserialize import enum_serialize


class TranchesCounterList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProjectTranches.TYPE_COUNTER))


class TranchesTypeList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProjectTranches.TYPE))


class TranchesTransList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProjectTranches.TYPE_TRANS))


class ProjectStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Projects.STATUS))


class ProjectControlStatusList(APIView):

    def get(self, request):
        return Response(enum_serialize(Projects.CONTROL_STATUS))


class IndicatorsTypeList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProjectIndicators.TYPE))


class IndicatorsCollectionTypeList(APIView):

    def get(self, request):
        return Response(enum_serialize(ProjectIndicators.COLLECTION_TYPE))
