from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework import status
from rest_framework.response import Response

from projects.models import ProjectChronology
from projects.serializers import ProjectChronologySerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProjectChronologiesView(ListCreateAPIView):
    serializer_class = ProjectChronologySerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ['project']
    ordering = ['pk']

    def get_queryset(self):
        return ProjectChronology.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProjectChronologyDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectChronologySerializer

    def get(self, request, pk):
        instance = get_object_or_404(ProjectChronology, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ProjectChronology, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ProjectChronology, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
