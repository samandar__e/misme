from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework import filters, status
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from projects.models import ProjectIndicatorsValue
from projects.serializers import ProjectIndicatorsValueSerializer, ProjectIndicatorsValueUpdateSerializer
from restapp.pagination import ResultsSetPagination


class ProjectIndicatorsValueView(ListCreateAPIView):
    serializer_class = ProjectIndicatorsValueSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filterset_fields = ['indicator']
    search_fields = ('date',)
    ordering = ['date']

    def get_queryset(self):
        queryset = ProjectIndicatorsValue.objects.all()
        indicator = self.request.query_params.get('indicator', None)
        if indicator is not None:
            queryset = queryset.filter(indicator=indicator)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProjectIndicatorsValueDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectIndicatorsValueUpdateSerializer
    http_method_names = ['put']

    def put(self, request, pk):
        for i in range(0, len(request.data)):
            instance = get_object_or_404(ProjectIndicatorsValue, id=request.data[i]['id'])
            serializer = self.serializer_class(instance, data=request.data[i])
            serializer.is_valid(raise_exception=True)
            serializer.save()
        return Response(status.HTTP_202_ACCEPTED)


