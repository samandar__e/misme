from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from components.models import ReportSchedule
from components.serializers import ReportScheduleSerializer, ReportScheduleCreateSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ReportScheduleView(ListCreateAPIView):
    serializer_class = ReportScheduleCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filterset_fields = ['field_id']
    search_fields = ('name',)
    ordering = ['-pk']

    def get_queryset(self):
        return ReportSchedule.objects.filter(table_type=ReportSchedule.TABLE[1][0])

    def get(self, request, *args, **kwargs):
        self.serializer_class = ReportScheduleSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user, table_type=ReportSchedule.TABLE[1][0])
        return Response(serializer.data, status.HTTP_201_CREATED)


class ReportScheduleDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ReportScheduleSerializer

    def get(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(ReportSchedule, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
