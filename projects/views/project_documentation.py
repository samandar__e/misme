from rest_framework.generics import ListCreateAPIView, DestroyAPIView, get_object_or_404
from rest_framework import status
from rest_framework.response import Response

from projects.models import ProjectDocumentation
from projects.serializers import ProjectDocumentationsSerializer, ProjectDocumentationCreateSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProjectDocumentationsView(ListCreateAPIView):
    serializer_class = ProjectDocumentationCreateSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ['project']
    ordering = ['pk']

    def get_queryset(self):
        return ProjectDocumentation.objects.all()

    def get(self, request, *args, **kwargs):
        self.serializer_class = ProjectDocumentationsSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProjectDocumentationsDetailView(DestroyAPIView):
    serializer_class = ProjectDocumentationsSerializer

    def delete(self, request, pk):
        instance = get_object_or_404(ProjectDocumentation, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
