from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404, UpdateAPIView
from rest_framework import filters, status
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from projects.models import Projects
from projects.serializers import ProjectsSerializer, ProjectCreateSerializer, ProjectsStatusSerializer
from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class ProjectsView(ListCreateAPIView):
    serializer_class = ProjectCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ('name', 'projectNumber', 'status', 'control_status')
    ordering = ['-pk']

    def get_queryset(self):
        queryset = Projects.objects.all()
        # if self.request.user.has_perm(''):
        #    queryset = Projects.objects.get_by_employee(self.request.user)
        return queryset

    def get(self, request, *args, **kwargs):
        self.serializer_class = ProjectsSerializer
        return self.list(request, *args, **kwargs)

    def post(self, request,  **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class ProjectsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = ProjectsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Projects, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Projects, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        try:
            instance = get_object_or_404(Projects, id=pk, component=None, subprojects=None)
            instance.delete()
            return Response(nonContent(), status.HTTP_204_NO_CONTENT)
        except Exception:
            message = {'errorMessage': 'Есть информация, связанная с этим проектом, сначала удалите их!'}
            return Response({'message': message}, status=HTTP_400_BAD_REQUEST)


class ProjectUpdateView(UpdateAPIView):
    serializer_class = ProjectsStatusSerializer
    http_method_names = ['put']

    def get_queryset(self):
        return Projects.objects.all()

    def update(self, request, pk):
        instance = get_object_or_404(Projects, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user, control_status='APPROVED')
        return Response(serializer.data, status.HTTP_202_ACCEPTED)
