from django.db import models
from django.db.models import QuerySet


class ProjectManager(models.Manager):

    def get_by_region_overall(self, start_date, end_date, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(implementationStartDate__gte=start_date) if start_date else queryset
        queryset = queryset.filter(implementationStartDate__lte=end_date) if end_date else queryset
        queryset = queryset.filter(regions=region) if region else queryset
        queryset = queryset.filter(districts=district) if district else queryset
        return queryset

    def get_by_region_overall_by_year(self, year, region=None, district=None) -> QuerySet:
        queryset = self.get_queryset()
        queryset = queryset.filter(implementationStartDate__year=year) if year else queryset
        queryset = queryset.filter(regions=region) if region else queryset
        queryset = queryset.filter(districts=district) if district else queryset
        return queryset

    def get_by_employee(self, user):
        employees = user.employees.all()
        queryset = self.get_queryset()
        queryset = queryset.filter(employees__in=employees)
        return queryset
