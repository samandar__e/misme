from rest_framework import serializers
from datetime import date
from rest_framework.utils import json

from components.models import Component
from crm.models import InternationalFinancialInstitute
from directory.models import Indicator, Employee
from mail.models import UserNotifications
from reports.serializers.network_schedule import SubprojectSerializer
from restapp.models import Attachment
from restapp.serializers import AttachmentSerializer
from subproject.models import Subproject
from subproject.serializers import SubprojectListSerializer
from .models import Projects, ProjectDocumentation, ProjectChronology, ProjectImplementation, ProjectTranches, \
    ProjectIndicators, ProjectIndicatorsValue, LoanFacility
from components.serializers import ComponentSerializer, ComponentAmountSerializer


class LocaleSerializer(serializers.ModelSerializer):
    name_uz = serializers.CharField(allow_blank=False)
    name_ru = serializers.CharField(allow_blank=False)
    name_en = serializers.CharField(allow_blank=False)
    govResolution_uz = serializers.CharField(allow_blank=False)
    govResolution_ru = serializers.CharField(allow_blank=False)
    govResolution_en = serializers.CharField(allow_blank=False)


class OverallProjectCostField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.overallProjectCostCurrency.id if instance.overallProjectCostCurrency else None
        return {"number": instance.overallProjectCost, "currency": currency}

    def to_internal_value(self, data):
        return {"overallProjectCost": data["number"], "overallProjectCostCurrency_id": data["currency"]}


class LocalGovernmentContributionField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.localGovernmentContributionCurrency.id if instance.localGovernmentContributionCurrency else None
        return {"number": instance.localGovernmentContribution, "currency": currency}

    def to_internal_value(self, data):
        return {"localGovernmentContribution": data["number"],
                "localGovernmentContributionCurrency_id": data["currency"]}


class BeneficiaryFundsField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.beneficiaryFundsCurrency.id if instance.beneficiaryFundsCurrency else None
        return {"number": instance.beneficiaryFunds, "currency": currency}

    def to_internal_value(self, data):
        return {"beneficiaryFunds": data["number"], "beneficiaryFundsCurrency_id": data["currency"]}


class SdrValueField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.sdrValueCurrency.id if instance.sdrValueCurrency else None
        return {"number": instance.sdrValue, "currency": currency}

    def to_internal_value(self, data):
        return {"sdrValue": data["number"], "sdrValueCurrency_id": data["currency"]}


class SdrValueListField(serializers.Field):
    def to_representation(self, instance):
        symbol = instance.sdrValueCurrency.symbol if instance.sdrValueCurrency else None
        return {"number": instance.sdrValue, "symbol": symbol}

    def to_internal_value(self, data):
        return {"sdrValue": data["number"], "sdrValueCurrency_id": data["symbol"]}


class CurrencyOneListField(serializers.Field):
    def to_representation(self, instance):
        symbol = instance.currencyOneCurrency.symbol if instance.currencyOneCurrency else None
        return {"number": instance.currencyOne, "symbol": symbol}

    def to_internal_value(self, data):
        return {"currencyOne": data["number"], "currencyOneCurrency_id": data["symbol"]}


class CurrencyTwoListField(serializers.Field):
    def to_representation(self, instance):
        symbol = instance.currencyTwoCurrency.symbol if instance.currencyTwoCurrency else None
        return {"number": instance.currencyTwo, "symbol": symbol}

    def to_internal_value(self, data):
        return {"currencyTwo": data["number"], "currencyTwoCurrency_id": data["symbol"]}


class CurrencyOneField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.currencyOneCurrency.id if instance.currencyOneCurrency else None
        return {"number": instance.currencyOne, "currency": currency}

    def to_internal_value(self, data):
        return {"currencyOne": data["number"], "currencyOneCurrency_id": data["currency"]}


class CurrencyTwoField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.currencyTwoCurrency.id if instance.currencyTwoCurrency else None
        return {"number": instance.currencyTwo, "currency": currency}

    def to_internal_value(self, data):
        return {"currencyTwo": data["number"], "currencyTwoCurrency_id": data["currency"]}


class TransferAmountField(serializers.Field):
    def to_representation(self, instance):
        currency = instance.transferAmountCurrency.id if instance.transferAmountCurrency else None
        return {"number": instance.transferAmount, "currency": currency}

    def to_internal_value(self, data):
        return {"transferAmount": data["number"], "transferAmountCurrency_id": data["currency"]}


class ImplementationPeriodField(serializers.Field):
    def to_representation(self, instance):
        return {"start": instance.implementationStartDate, "end": instance.implementationEndDate}

    def to_internal_value(self, data):
        return {"implementationStartDate": data["start"], "implementationEndDate": data["end"]}


class FoundCountField(serializers.Field):
    def to_representation(self, instance):
        components = Component.objects.all().filter(project=instance.id)
        serializer_comp = ComponentAmountSerializer(components, many=True)
        components_dict = json.loads(json.dumps(serializer_comp.data))
        found_drawn_components = 0
        for i in range(0, len(components_dict)):
            found_drawn_components = found_drawn_components + int(components_dict[i]['loan_grant_amount'] or 0)
        subprojects = Subproject.objects.all().filter(project=instance.id)
        serializer_sub = SubprojectSerializer(subprojects, many=True)
        subprojects_dict = json.loads(json.dumps(serializer_sub.data))
        found_drawn_subprojects = 0
        for i in range(0, len(subprojects_dict)):
            found_drawn_subprojects = found_drawn_subprojects + int(subprojects_dict[i]['total_cost'] or 0)
        found_drawn = found_drawn_components + found_drawn_subprojects
        found_remained = int(instance.overallProjectCost or 0) - found_drawn
        found_percent = float("{:.0f}".format(
            (found_drawn/instance.overallProjectCost)*100)) if int(instance.overallProjectCost or 0) != 0 else 0
        return {"found_drawn": found_drawn, "found_remained": found_remained, "found_percent": found_percent}


class IFISerializer(serializers.ModelSerializer):

    class Meta:
        model = InternationalFinancialInstitute
        fields = ('id', 'name',)


class LoanFacilitySerializer(serializers.ModelSerializer):
    sdrValue = SdrValueField(source='*', required=False)
    currencyOne = CurrencyOneField(source='*', required=False)
    currencyTwo = CurrencyTwoField(source='*', required=False)
    ifi_detail = IFISerializer(source='ifi', read_only=True)

    class Meta:
        model = LoanFacility
        fields = (
            'id',
            'project',
            'ifi',
            'ifi_detail',
            'sdrValue',
            'currencyOne',
            'currencyTwo',
        )


class LoanFacilityListSerializer(serializers.ModelSerializer):
    sdrValue = SdrValueListField(source='*', required=False)
    currencyOne = CurrencyOneListField(source='*', required=False)
    currencyTwo = CurrencyTwoListField(source='*', required=False)
    ifi_detail = IFISerializer(source='ifi', read_only=True)

    class Meta:
        model = LoanFacility
        fields = (
            'id',
            'project',
            'ifi',
            'ifi_detail',
            'sdrValue',
            'currencyOne',
            'currencyTwo',
        )


class ProjectsStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Projects
        fields = (
            'id',
            'name',
            'projectNumber',
            'control_status',
        )
        extra_kwargs = {'name': {'required': False}, 'projectNumber': {'required': False}}


class ProjectChronologySerializer(serializers.ModelSerializer):
    date = serializers.DateField(required=True)
    text = serializers.CharField(required=True)

    class Meta:
        model = ProjectChronology
        fields = ('id', 'project', 'text', 'date')


class EmployeeSerializerList(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('id', 'firstname', 'user', 'user', 'project')


class NotificationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserNotifications
        fields = ('id', 'title', 'description', 'status')


class ProjectsSerializer(LocaleSerializer):
    overallProjectCost = OverallProjectCostField(source='*', required=False)
    localGovernmentContribution = LocalGovernmentContributionField(source='*', required=False)
    beneficiaryFunds = BeneficiaryFundsField(source='*', required=False)
    implementationPeriod = ImplementationPeriodField(source='*', required=False)
    component = ComponentSerializer(many=True, read_only=True)
    # subprojects = SubprojectListSerializer(many=True, read_only=True)
    notifications = NotificationListSerializer(many=True, read_only=True)
    foundCount = FoundCountField(source='*', required=False)
    loanFacility = LoanFacilityListSerializer(many=True, read_only=True)
    chronology = ProjectChronologySerializer(many=True, read_only=True)
    employees = EmployeeSerializerList(many=True, read_only=True)

    class Meta:
        model = Projects
        fields = (
            'id',
            'name',
            'name_uz',
            'name_ru',
            'name_en',
            'projectNumber',
            'govResolution',
            'govResolution_uz',
            'govResolution_ru',
            'govResolution_en',
            'description',
            'overallProjectCost',
            'localGovernmentContribution',
            'beneficiaryFunds',
            'headOfPiu',
            'piuInTable',
            'piuEmployeesActual',
            'dateSigned',
            'dateClosing',
            'availableStudy',
            'implementationPeriod',
            'regions',
            'districts',
            'overallCount',
            'pfis',
            'menCount',
            'womenCount',
            'status',
            'control_status',
            'foundCount',
            'component',
            # 'subprojects',
            'pdo',
            'loanFacility',
            'chronology',
            'employees',
            'notifications',
        )
        extra_kwargs = {'name': {'required': False}}


class ProjectCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'name_uz', 'name_ru', 'name_en', 'projectNumber',)


class ProjectDocumentationCreateSerializer(serializers.ModelSerializer):
    file = serializers.FileField(write_only=True)

    class Meta:
        model = ProjectDocumentation
        fields = ('id', 'project', 'name', 'file',)

    def create(self, validated_data):
        file = validated_data.pop('file')
        project_documentation = ProjectDocumentation.objects.create(**validated_data)
        if file:
            file = Attachment.objects.create(file_name=file.name, file=file)
            file.save()
            project_documentation.file.add(file)
            project_documentation.save()
        return project_documentation


class ProjectDocumentationsSerializer(serializers.ModelSerializer):
    file = AttachmentSerializer(many=True)

    class Meta:
        model = ProjectDocumentation
        fields = ('id', 'project', 'name', 'file',)


class ProjectImplementationSerializer(serializers.ModelSerializer):
    date = serializers.DateField(required=True)
    problems = serializers.CharField(required=True)

    class Meta:
        model = ProjectImplementation
        fields = (
            'id',
            'project',
            'date',
            'activitiesImplemented',
            'problems'
        )


class ProjectTranchesSerializer(serializers.ModelSerializer):
    transferAmount = TransferAmountField(source='*', required=False)
    type = serializers.CharField(required=True)
    counterpartType = serializers.CharField(required=True)
    transferredFundsType = serializers.CharField(required=True)
    text = serializers.CharField(required=True)

    class Meta:
        model = ProjectTranches
        fields = (
            'id',
            'type',
            'counterpartType',
            'text',
            'transferredFundsType',
            'counterpartName',
            'transferAmount',
            'project'
        )


class ProjectIndicatorsValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicatorsValue
        fields = (
            'id',
            'indicator',
            'planValue',
            'factualValue',
            'date',
            'sorting'
        )


class IndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Indicator
        fields = ('name',)


class ProjectIndicatorsCreateSerializer(serializers.ModelSerializer):
    indicators_value = ProjectIndicatorsValueSerializer(many=True, read_only=True)
    indicator_detail = IndicatorSerializer(source='indicator', read_only=True)

    class Meta:
        model = ProjectIndicators
        fields = (
            'id',
            'project',
            'startDate',
            'endDate',
            'collectionType',
            'type',
            'indicator',
            'indicator_detail',
            'indicators_value'
        )
        extra_kwargs = {'indicator': {'required': True}}

    def create(self, validated_data, null=None):
        project = validated_data.get('project', None)
        projects_indicators = ProjectIndicators.objects.create(**validated_data)
        indicator = ProjectIndicators.objects.filter(project=project).first()

        if project.implementationStartDate != null and project.implementationEndDate != null:
            if indicator.collectionType != projects_indicators.collectionType:
                projects_indicators.startDate = project.implementationStartDate
                projects_indicators.endDate = project.implementationEndDate
                projects_indicators.collectionType = indicator.collectionType
                projects_indicators.save()
            else:
                projects_indicators.startDate = project.implementationStartDate
                projects_indicators.endDate = project.implementationEndDate
                projects_indicators.save()

            def indicators_values_add(ii, jj):
                indicators_value = ProjectIndicatorsValue.objects.create(
                    indicator=projects_indicators,
                    sorting=0,
                    date=date(projects_indicators.startDate.year + ii - 1, month=jj, day=1)
                )
                return indicators_value

            start_year = projects_indicators.startDate.year
            end_year = projects_indicators.endDate.year
            start_month = projects_indicators.startDate.month
            end_month = projects_indicators.endDate.month
            counts = end_year - start_year

            if projects_indicators.collectionType == 'Yearly':
                for i in range(1, counts + 2):
                    indicators_values = indicators_values_add(i, 1)
                    indicators_values.save()
            elif projects_indicators.collectionType == 'Monthly':
                for i in range(1, counts + 2):
                    if counts == 0:
                        for j in range(start_month, end_month + 1):
                            indicators_values = indicators_values_add(i, j)
                            indicators_values.save()
                    elif start_month == 1 and end_month == 12:
                        for j in range(1, 13):
                            indicators_values = indicators_values_add(i, j)
                            indicators_values.save()
                    else:
                        if i == 1:
                            for j in range(start_month, 13):
                                indicators_values = indicators_values_add(i, j)
                                indicators_values.save()
                        elif i == counts + 1:
                            for j in range(1, end_month + 1):
                                indicators_values = indicators_values_add(i, j)
                                indicators_values.save()
                        else:
                            for j in range(1, 13):
                                indicators_values = indicators_values_add(i, j)
                                indicators_values.save()

            return projects_indicators
        else:
            message = {'startDate': 'Это значение должно быть заполнено!',
                       'endDate': 'Это значение должно быть заполнено!'}
            raise serializers.ValidationError(message)


class ProjectIndicatorsValueUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectIndicatorsValue
        fields = (
            'id',
            'planValue',
            'factualValue',
        )

    def update(self, instance, validated_data):
        instance.planValue = validated_data['planValue']
        instance.factualValue = validated_data['factualValue']
        instance.save()
        return instance
