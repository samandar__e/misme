# INSTALL & SETUP INSTRUCTIONS

to clone project open terminal & write: git clone https://gitlab.com/samandar__e/misme.git,
then: cd misme,
if you have got other branch than master, write: git checkout your_branch


#### 1. Create a virtual environment to isolate our package dependencies locally
pip install virtualenv  
python -m venv env 
 
source env/bin/activate for Linux/MacOS  
env\Scripts\activate.bat for Windows  

#### 2. Up postgres  
docker run --name misme -e POSTGRES_DB=dbname -e POSTGRES_USER=dbuser -e POSTGRES_PASSWORD=dbpassword   -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres

#### 3. Export variables to environment:
export MISME_DB_NAME='dbname'  
export MISME_DB_USER='dbuser'  
export MISME_DB_PASSWORD='dbpassword'  
export DJANGO_SECRET_KEY=SECRET_KEY  
export DJANGO_DEBUG=False  

for Windows:  
set MISME_DB_NAME=dbname  
set MISME_DB_USER=dbuser  
set MISME_DB_PASSWORD=dbpassword  
set DJANGO_SECRET_KEY=SECRET_KEY  
set DJANGO_DEBUG=False  

PowerShell:  
$env:MISME_DB_NAME = 'dbname'  
........ 
  
#### 4. Install requirements
pip install -r requirements.txt

#### 5. Migrate
python manage.py makemigrations
python manage.py migrate

#### 6. Create superuser
python manage.py createsuperuser  

#### 7. Load fixtures
python manage.py loaddata directory/fixtures/\*.json  
python manage.py loaddata directory/fixtures/\*.yaml  

#### 8. Run server
python manage.py runserver

#### 9. Enjoy
http://localhost:8000/api/v1/  
http://localhost:8000/admin/  