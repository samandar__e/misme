from modeltranslation.translator import register, TranslationOptions
from .models import \
    InternationalFinancialInstitute, \
    ParticipatingFinancialInstitution, \
    Beneficiary, \
    PfiProduct, \
    Agency, \
    Consultant, \
    BeneficiaryFounder


@register(InternationalFinancialInstitute)
class InternationalFinancialInstituteTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ParticipatingFinancialInstitution)
class ParticipatingFinancialInstitutionTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Beneficiary)
class BeneficiaryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(PfiProduct)
class PfiProductTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Agency)
class AgencyTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Consultant)
class ConsultantTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(BeneficiaryFounder)
class BeneficiaryFounderTranslationOptions(TranslationOptions):
    fields = ('name',)
