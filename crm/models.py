from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from restapp.models import BaseModel, Attachment
from directory.models import Region, District, FOA, Sector, SER, EntityType


class InternationalFinancialInstitute(BaseModel):
    name = models.CharField(max_length=60)
    legalAddress = models.TextField(blank=True)
    responsiblePersonFullname = models.CharField(max_length=60, blank=True)
    responsiblePersonPosition = models.CharField(max_length=50, blank=True)
    responsiblePersonPhoneNumber = models.CharField(max_length=50, blank=True)
    responsiblePersonEmail = models.EmailField(max_length=50, blank=True)
    additionalContactPersonFullname = models.CharField(max_length=60, blank=True)
    additionalContactPersonPosition = models.CharField(max_length=50, blank=True)
    additionalContactPersonPhoneNumber = models.CharField(max_length=50, blank=True)
    additionalContactPersonEmail = models.EmailField(max_length=50, blank=True)
    
    def __str__(self):
        return self.name


class ParticipatingFinancialInstitution(BaseModel):
    LEVEL_CHOICES = [
        ('HEAD_OFFICE', 'HEAD OFFICE'),
        ('BRANCH', 'BRANCH'),
    ]

    level = models.CharField(max_length=60, choices=LEVEL_CHOICES, default='HEAD_OFFICE')
    name = models.CharField(max_length=60)
    region = models.ForeignKey(
        Region, 
        on_delete=models.SET_NULL, 
        blank=True,
        null=True,
    )
    district = models.ForeignKey(
        District,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    legalAddress = models.TextField(blank=True)
    contactPersonFullName = models.CharField(max_length=60, blank=True)
    contactPersonPhoneNumber = models.CharField(max_length=50, blank=True)
    financingVolume = models.CharField(max_length=255, blank=True)
    maturity = models.CharField(max_length=255, blank=True)
    gracePeriod = models.CharField(max_length=255, blank=True)
    commission = models.CharField(max_length=255, blank=True)
    loanInterestRate = models.CharField(max_length=255, blank=True)
    interestPaymentCurrency = models.CharField(max_length=255, blank=True)
    principalDebtPaymentCurrency = models.CharField(max_length=255, blank=True)
    feePaymentCurrency = models.CharField(max_length=255, blank=True)
    finesPaymentCurrency = models.CharField(max_length=255, blank=True)
    interest = models.CharField(max_length=255, blank=True)
    interestAndFeeTerm = models.CharField(max_length=255, blank=True)
    dateOfLoanAccountClosing = models.DateField(max_length=255, null=True, blank=True)
    dateOfPayment = models.DateField(max_length=255, null=True, blank=True)
    file = models.ManyToManyField(Attachment, blank=True)
    taxId = models.IntegerField(
        validators=[MaxValueValidator(999999999), MinValueValidator(100000000)],
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name


class Beneficiary(BaseModel):
    name = models.CharField(max_length=60)
    ownershipForm = models.CharField(max_length=255, blank=True)
    sectors = models.ManyToManyField(Sector, blank=True)
    field_of_activities = models.ManyToManyField(FOA, blank=True)
    region = models.ForeignKey(
        Region, 
        on_delete=models.SET_NULL, 
        blank=True,
        null=True,
    )
    district = models.ForeignKey(
        District, 
        on_delete=models.SET_NULL, 
        blank=True,
        null=True,
    )
    legalAddress = models.TextField(blank=True)
    numberOfEmployeesMen = models.IntegerField(blank=True, null=True)
    numberOfEmployeesWomen = models.IntegerField(blank=True, null=True)
    landArea = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)
    environmentalCategory = models.ForeignKey(SER, on_delete=models.SET_NULL, blank=True, null=True)
    directorFullname = models.CharField(max_length=60, blank=True)
    directorPhoneNumber = models.CharField(max_length=50, blank=True)
    directorEmail = models.EmailField(max_length=50, blank=True)
    additionalContactPersonFullname = models.CharField(max_length=60, blank=True)
    additionalContactPersonPosition = models.CharField(max_length=50, blank=True)
    additionalContactPersonPhoneNumber = models.CharField(max_length=50, blank=True)
    additionalContactPersonEmail = models.EmailField(max_length=50, blank=True)
    taxId = models.IntegerField(
        validators=[MaxValueValidator(999999999), MinValueValidator(100000000)],
        blank=True,
        null=True
    )
    entity_type = models.ForeignKey(
        EntityType,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class PfiProduct(BaseModel):
    name = models.CharField(max_length=50)
    amount = models.CharField(max_length=50, blank=True)
    term = models.CharField(max_length=50, blank=True)
    loanInterest = models.CharField(max_length=50, blank=True)
    gracePeriod = models.CharField(max_length=50, blank=True)
    sectionCriteria = models.CharField(max_length=50, blank=True)
    requirement = models.CharField(max_length=50, blank=True, null=True)

    pfi = models.ForeignKey(
        ParticipatingFinancialInstitution,
        related_name='pfi_products',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class Agency(BaseModel):
    name = models.CharField(max_length=60)
    legalAddress = models.TextField(blank=True)
    responsiblePersonFullname = models.CharField(max_length=60, blank=True)
    responsiblePersonPosition = models.CharField(max_length=50, blank=True)
    responsiblePersonPhoneNumber = models.CharField(max_length=50, blank=True)
    responsiblePersonEmail = models.EmailField(max_length=50, blank=True)
    additionalContactPersonFullname = models.CharField(max_length=60, blank=True)
    additionalContactPersonPosition = models.CharField(max_length=50, blank=True)
    additionalContactPersonPhoneNumber = models.CharField(max_length=50, blank=True)
    additionalContactPersonEmail = models.EmailField(max_length=50, blank=True)
    taxId = models.IntegerField(
        validators=[MaxValueValidator(999999999), MinValueValidator(100000000)],
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name


class Consultant(BaseModel):
    name = models.CharField(max_length=60)
    legalAddress = models.TextField(blank=True)
    responsiblePersonFullname = models.CharField(max_length=60, blank=True)
    responsiblePersonPosition = models.CharField(max_length=50, blank=True)
    responsiblePersonPhoneNumber = models.CharField(max_length=50, blank=True)
    responsiblePersonEmail = models.EmailField(max_length=50, blank=True)
    additionalContactPersonFullname = models.CharField(max_length=60, blank=True)
    additionalContactPersonPosition = models.CharField(max_length=50, blank=True)
    additionalContactPersonPhoneNumber = models.CharField(max_length=50, blank=True)
    additionalContactPersonEmail = models.EmailField(max_length=50, blank=True)
    consultant_type = models.BooleanField(null=True, blank=False)
    taxId = models.IntegerField(
        validators=[MaxValueValidator(999999999), MinValueValidator(100000000)],
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name


class BeneficiaryFounder(BaseModel):
    TYPE_CHOICES = [
        ('CORPORATE', 'CORPORATE'),
        ('INDIVIDUAL', 'INDIVIDUAL'),
        ('UNKNOWN', 'UNKNOWN')
    ]

    GENDER_CHOICES = [
        ('MALE', 'MALE'),
        ('FEMALE', 'FEMALE')
    ]
    type = models.CharField(max_length=60, choices=TYPE_CHOICES, default=1)
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=60, choices=GENDER_CHOICES, default=1)
    percentage = models.FloatField(blank=True, null=True)
    taxId = models.IntegerField(
        validators=[MaxValueValidator(999999999), MinValueValidator(100000000)],
        blank=True,
        null=True
    )
    beneficiary = models.ForeignKey(
        Beneficiary,
        related_name='beneficiary_founders',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name
