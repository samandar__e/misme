from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, get_object_or_404, RetrieveDestroyAPIView, UpdateAPIView, \
    RetrieveUpdateDestroyAPIView
from rest_framework.response import Response

from crm.filterset import BeneficiaryFilter
from crm.models import Beneficiary
from crm.serializers import BeneficiarySerializer, BeneficiaryDetailSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class BeneficiaryView(ListCreateAPIView):
    serializer_class = BeneficiaryDetailSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = BeneficiaryFilter
    ordering = ['pk']
    search_fields = (
        'name',
        'ownershipForm',
        'sector',
        'foa',
        'region',
        'district',
        'legalAddress',
        'numberOfEmployeesMen',
        'numberOfEmployeesWomen',
        'landArea',
        'environmentalCategory',
        'directorFullname',
        'directorPhoneNumber',
        'directorEmail',
        'additionalContactPersonFullname',
        'additionalContactPersonPosition',
        'additionalContactPersonPhoneNumber',
        'additionalContactPersonEmail',
        'taxId'
    )

    def get_queryset(self):
        return Beneficiary.objects.all()

    def get(self, request, *args, **kwargs):
        self.serializer_class = BeneficiarySerializer
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class BeneficiaryDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = BeneficiaryDetailSerializer

    def get(self, request, pk):
        instance = get_object_or_404(Beneficiary, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Beneficiary, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Beneficiary, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
