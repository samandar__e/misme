from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from crm.filterset import PfiProductFilter
from crm.models import PfiProduct
from crm.serializers import PfiProductSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class PfiProductView(ListCreateAPIView):
    serializer_class = PfiProductSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = PfiProductFilter
    search_fields = (
        'name',
        'amount',
        'term',
        'loan_interest',
        'grace_period',
        'sectionCriteria',
        'requirement',
        'pfi'
    )
    filterset_fields = ['pfi']
    ordering = ['pk']

    def get_queryset(self):
        queryset = PfiProduct.objects.all()
        pfi = self.request.query_params.get('pfi', None)
        if pfi is not None:
            queryset = queryset.filter(pfi=pfi)
        return queryset

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class PfiProductDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = PfiProductSerializer

    def get(self, request, pk):
        instance = get_object_or_404(PfiProduct, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(PfiProduct, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(PfiProduct, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
