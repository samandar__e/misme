from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response

from crm.filterset import AgencyFilter
from crm.models import Agency
from crm.serializers import AgencySerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class AgencyView(ListCreateAPIView):
    serializer_class = AgencySerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = AgencyFilter
    search_fields = (
        'name',
        'legalAddress',
        'responsiblePersonFullname',
        'responsiblePersonPosition',
        'responsiblePersonEmail',
        'responsiblePersonPhoneNumber',
        'additionalContactPersonFullname',
        'additionalContactPersonPosition',
        'additionalContactPersonEmail',
        'additionalContactPersonPhoneNumber',
        'taxId'
    )
    ordering = ['pk']

    def get_queryset(self):
        return Agency.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class AgencyDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = AgencySerializer

    def get(self, request, pk):
        instance = get_object_or_404(Agency, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(Agency, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(Agency, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
