from rest_framework import status, filters
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from crm.filterset import InternationalFinancialInstituteFilter
from crm.models import InternationalFinancialInstitute
from crm.serializers import InternationalFinancialInstituteSerializer

from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent


class InternationalFinancialInstituteView(ListCreateAPIView):
    serializer_class = InternationalFinancialInstituteSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, DjangoFilterBackend)
    filterset_class = InternationalFinancialInstituteFilter
    search_fields = (
        'name_en',
        'legalAddress',
        'responsiblePersonFullname',
        'responsiblePersonPosition',
        'responsiblePersonEmail',
        'responsiblePersonPhoneNumber',
        'additionalContactPersonFullname',
        'additionalContactPersonPosition',
        'additionalContactPersonEmail',
        'additionalContactPersonPhoneNumber'
    )
    ordering = ['pk']

    def get_queryset(self):
        return InternationalFinancialInstitute.objects.all()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class InternationalFinancialInstituteDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = InternationalFinancialInstituteSerializer

    def get(self, request, pk):
        instance = get_object_or_404(InternationalFinancialInstitute, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(InternationalFinancialInstitute, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(InternationalFinancialInstitute, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
