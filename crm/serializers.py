from rest_framework import serializers

from directory.serializers import \
    RelatedRegionSerializer, \
    RelatedDistrictSerializer, \
    SERSerializer, SectorSerializer, RelatedFOASerializer

from .models import \
    InternationalFinancialInstitute, \
    ParticipatingFinancialInstitution, \
    Beneficiary, \
    PfiProduct, \
    Agency, \
    Consultant, \
    BeneficiaryFounder


class LocaleSerializer(serializers.ModelSerializer):
    name_uz = serializers.CharField(allow_blank=False)
    name_ru = serializers.CharField(allow_blank=False)
    name_en = serializers.CharField(allow_blank=False)


class InternationalFinancialInstituteSerializer(LocaleSerializer):

    class Meta:
        model = InternationalFinancialInstitute
        fields = (
            'id',
            'name',
            'name_ru',
            'name_uz', 
            'name_en', 
            'legalAddress', 
            'responsiblePersonFullname', 
            'responsiblePersonPosition', 
            'responsiblePersonPhoneNumber',
            'responsiblePersonEmail',
            'additionalContactPersonFullname',
            'additionalContactPersonPosition',
            'additionalContactPersonPhoneNumber',
            'additionalContactPersonEmail',
        )
        extra_kwargs = {'name': {'required': False}}


class BeneficiaryFounderSerializer(serializers.ModelSerializer):

    class Meta:
        model = BeneficiaryFounder
        fields = (
            'id',
            'type',
            'name',
            'gender',
            'percentage',
            'taxId',
            'beneficiary'
        )


class BeneficiarySerializer(LocaleSerializer):
    beneficiary_founders = BeneficiaryFounderSerializer(many=True, read_only=True)
    region_detail = RelatedRegionSerializer(source='region', read_only=True)
    district_detail = RelatedDistrictSerializer(source='district', read_only=True)
    environmentalCategory_detail = SERSerializer(source='environmentalCategory', read_only=True)
    field_of_activities = RelatedFOASerializer(read_only=True, many=True)
    sectors = SectorSerializer(read_only=True, many=True)

    class Meta:
        model = Beneficiary
        fields = (
            'id',
            'name',
            'name_ru',
            'name_uz', 
            'name_en', 
            'ownershipForm',
            'sectors',
            'field_of_activities',
            'region',
            'region_detail',
            'district',
            'district_detail',
            'legalAddress',
            'numberOfEmployeesMen',
            'numberOfEmployeesWomen',
            'landArea',
            'environmentalCategory',
            'environmentalCategory_detail',
            'directorFullname',
            'directorPhoneNumber',
            'directorEmail',
            'additionalContactPersonFullname',
            'additionalContactPersonPosition',
            'additionalContactPersonPhoneNumber',
            'additionalContactPersonEmail',
            'taxId',
            'beneficiary_founders',
            'entity_type'
        )
        extra_kwargs = {'name': {'required': False}}


class BeneficiaryDetailSerializer(LocaleSerializer):
    beneficiary_founders = BeneficiaryFounderSerializer(many=True, read_only=True)
    region_detail = RelatedRegionSerializer(source='region', read_only=True)
    district_detail = RelatedDistrictSerializer(source='district', read_only=True)
    environmentalCategory_detail = SERSerializer(source='environmentalCategory', read_only=True)

    class Meta:
        model = Beneficiary
        fields = (
            'id',
            'name',
            'name_ru',
            'name_uz',
            'name_en',
            'ownershipForm',
            'sectors',
            'field_of_activities',
            'region',
            'region_detail',
            'district',
            'district_detail',
            'legalAddress',
            'numberOfEmployeesMen',
            'numberOfEmployeesWomen',
            'landArea',
            'environmentalCategory',
            'environmentalCategory_detail',
            'directorFullname',
            'directorPhoneNumber',
            'directorEmail',
            'additionalContactPersonFullname',
            'additionalContactPersonPosition',
            'additionalContactPersonPhoneNumber',
            'additionalContactPersonEmail',
            'taxId',
            'beneficiary_founders'
        )
        extra_kwargs = {'name': {'required': False}}


class PfiProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = PfiProduct
        fields = (
            'id',
            'name',
            'amount',
            'term',
            'loanInterest',
            'gracePeriod',
            'sectionCriteria',
            'requirement',
            'pfi'
        )


class ParticipatingFinancialInstitutionSerializer(LocaleSerializer):
    pfi_products = PfiProductSerializer(many=True, read_only=True)
    region_detail = RelatedRegionSerializer(source='region', read_only=True)
    district_detail = RelatedDistrictSerializer(source='district', read_only=True)

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = (
            'id',
            'name',
            'name_ru',
            'name_uz',
            'name_en',
            'level',
            'region',
            'region_detail',
            'district',
            'district_detail',
            'legalAddress',
            'contactPersonFullName',
            'contactPersonPhoneNumber',
            'financingVolume',
            'maturity',
            'gracePeriod',
            'commission',
            'loanInterestRate',
            'interestPaymentCurrency',
            'principalDebtPaymentCurrency',
            'feePaymentCurrency',
            'finesPaymentCurrency',
            'interest',
            'interestAndFeeTerm',
            'dateOfLoanAccountClosing',
            'dateOfPayment',
            'pfi_products',
            'taxId'
        )
        extra_kwargs = {'name': {'required': False}}


class AgencySerializer(LocaleSerializer):

    class Meta:
        model = Agency
        fields = (
            'id',
            'name',
            'name_ru',
            'name_uz',
            'name_en',
            'legalAddress',
            'responsiblePersonFullname',
            'responsiblePersonPosition',
            'responsiblePersonPhoneNumber',
            'responsiblePersonEmail',
            'additionalContactPersonFullname',
            'additionalContactPersonPosition',
            'additionalContactPersonPhoneNumber',
            'additionalContactPersonEmail',
            'taxId'
        )
        extra_kwargs = {'name': {'required': False}}


class ConsultantSerializer(LocaleSerializer):

    class Meta:
        model = Consultant
        fields = (
            'id',
            'name',
            'legalAddress',
            'responsiblePersonFullname',
            'responsiblePersonPosition',
            'responsiblePersonPhoneNumber',
            'responsiblePersonEmail',
            'additionalContactPersonFullname',
            'additionalContactPersonPosition',
            'additionalContactPersonPhoneNumber',
            'additionalContactPersonEmail',
            'taxId',
            'consultant_type'
        )
        extra_kwargs = {'name': {'required': False}}


class RelatedBeneficiaryForListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Beneficiary
        fields = ('id', 'name', 'taxId')


class RelatedBeneficiaryForDetailSerializer(serializers.ModelSerializer):
    beneficiary_founders = BeneficiaryFounderSerializer(many=True, read_only=True)

    class Meta:
        model = Beneficiary
        fields = ('id', 'name', 'taxId', 'legalAddress', 'additionalContactPersonFullname',
                  'additionalContactPersonPhoneNumber', 'beneficiary_founders')
