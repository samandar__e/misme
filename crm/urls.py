from django.urls import re_path, path

from .views.ifi import InternationalFinancialInstituteView, InternationalFinancialInstituteDetailView
from .views.pfi import ParticipatingFinancialInstitutionView, ParticipatingFinancialInstitutionDetailView
from .views.beneficiary import BeneficiaryView, BeneficiaryDetailView
from .views.pfifp import PfiProductView, PfiProductDetailView
from .views.agency import AgencyView, AgencyDetailView
from .views.consultant import ConsultantView, ConsultantDetailView
from .views.beneficiary_founder import BeneficiaryFounderView, BeneficiaryFounderDetailView

urlpatterns = [
    re_path(r'^ifi/$', InternationalFinancialInstituteView.as_view(), name='ifi_view'),
    path('ifi/<int:pk>', InternationalFinancialInstituteDetailView.as_view(), name='ifi_detail_view'),

    re_path(r'^pfi/$', ParticipatingFinancialInstitutionView.as_view(), name='pfi_view'),
    path('pfi/<int:pk>', ParticipatingFinancialInstitutionDetailView.as_view(), name='pfi_detail_view'),

    re_path(r'^beneficiary/$', BeneficiaryView.as_view(), name='beneficiary_view'),
    path('beneficiary/<int:pk>', BeneficiaryDetailView.as_view(), name='beneficiary_detail_view'),

    re_path(r'^pfifp/$', PfiProductView.as_view(), name='pfifp_view'),
    path('pfifp/<int:pk>', PfiProductDetailView.as_view(), name='pfifp_detail_view'),

    re_path(r'^agency/$', AgencyView.as_view(), name='agency_view'),
    path('agency/<int:pk>', AgencyDetailView.as_view(), name='agency_detail_view'),

    re_path(r'^consultant/$', ConsultantView.as_view(), name='consultant_view'),
    path('consultant/<int:pk>', ConsultantDetailView.as_view(), name='consultant_detail_view'),

    re_path(r'^beneficiary-founder/$', BeneficiaryFounderView.as_view(), name='beneficiary_founder_view'),
    path('beneficiary-founder/<int:pk>', BeneficiaryFounderDetailView.as_view(), name='beneficiary_founder_detail_view'),
]
