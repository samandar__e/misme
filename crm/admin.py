from django.contrib import admin
from .models import \
    InternationalFinancialInstitute, \
    ParticipatingFinancialInstitution, \
    Beneficiary, \
    PfiProduct, \
    Agency, \
    Consultant, \
    BeneficiaryFounder

# Register your models here.


@admin.register(InternationalFinancialInstitute)
class InternationalFinancialInstituteAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(ParticipatingFinancialInstitution)
class ParticipatingFinancialInstitutionAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(Beneficiary)
class BeneficiaryAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(PfiProduct)
class PfiProductAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(Agency)
class AgencyAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(Consultant)
class ConsultantAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz')
    search_fields = ('name_en', 'name_ru', 'name_uz')


@admin.register(BeneficiaryFounder)
class BeneficiaryFounderAdmin(admin.ModelAdmin):
    list_display = ('name_en', 'name_ru', 'name_uz')
    fields = ('name_en', 'name_ru', 'name_uz', 'type', 'gender', 'percentage', 'beneficiary')
    search_fields = ('name_en', 'name_ru', 'name_uz')
