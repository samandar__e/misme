from django_filters.rest_framework import FilterSet

from crm.models import InternationalFinancialInstitute, ParticipatingFinancialInstitution, PfiProduct, Agency, \
    Consultant, BeneficiaryFounder, Beneficiary


class InternationalFinancialInstituteFilter(FilterSet):

    class Meta:
        model = InternationalFinancialInstitute
        fields = {
            'name_en': ['exact', 'startswith', 'contains'],
            'legalAddress': ['exact'],
            'responsiblePersonFullname': ['exact'],
            'responsiblePersonPosition': ['exact'],
            'responsiblePersonPhoneNumber': ['exact'],
            'responsiblePersonEmail': ['exact'],
            'additionalContactPersonFullname': ['exact'],
            'additionalContactPersonPosition': ['exact'],
            'additionalContactPersonPhoneNumber': ['exact'],
            'additionalContactPersonEmail': ['exact'],
        }


class ParticipatingFinancialInstitutionFilter(FilterSet):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = {
            'name_en': ['exact', 'startswith', 'contains'],
            'level': ['exact'],
            'region': ['exact'],
            'district': ['exact'],
            'legalAddress': ['exact'],
            'contactPersonFullName': ['exact'],
            'contactPersonPhoneNumber': ['exact'],
            'financingVolume': ['exact'],
            'maturity': ['exact'],
            'gracePeriod': ['exact'],
            'commission': ['exact'],
            'loanInterestRate': ['exact'],
            'interestPaymentCurrency': ['exact'],
            'principalDebtPaymentCurrency': ['exact'],
            'feePaymentCurrency': ['exact'],
            'finesPaymentCurrency': ['exact'],
            'interest': ['exact'],
            'interestAndFeeTerm': ['exact'],
            'dateOfLoanAccountClosing': ['exact'],
            'dateOfPayment': ['exact'],
            'taxId': ['lt', 'gt'],
        }


class BeneficiaryFilter(FilterSet):
    class Meta:
        model = Beneficiary
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'ownershipForm': ['exact'],
            'sectors': ['exact'],
            'field_of_activities': ['exact'],
            'region': ['exact'],
            'district': ['exact'],
            'legalAddress': ['exact'],
            'numberOfEmployeesMen': ['lt', 'gt'],
            'numberOfEmployeesWomen': ['lt', 'gt'],
            'landArea': ['lt', 'gt'],
            'environmentalCategory': ['exact'],
            'directorFullname': ['exact'],
            'directorPhoneNumber': ['exact'],
            'directorEmail': ['exact'],
            'additionalContactPersonFullname': ['exact'],
            'additionalContactPersonPosition': ['exact'],
            'additionalContactPersonPhoneNumber': ['exact'],
            'additionalContactPersonEmail': ['exact'],
            'taxId': ['lt', 'gt'],
        }


class PfiProductFilter(FilterSet):

    class Meta:
        model = PfiProduct
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'amount': ['exact'],
            'term': ['exact'],
            'loanInterest': ['exact'],
            'gracePeriod': ['exact'],
            'sectionCriteria': ['exact'],
            'requirement': ['exact'],
            'pfi': ['exact'],
        }


class AgencyFilter(FilterSet):

    class Meta:
        model = Agency
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'legalAddress': ['exact'],
            'responsiblePersonFullname': ['exact'],
            'responsiblePersonPosition': ['exact'],
            'responsiblePersonPhoneNumber': ['exact'],
            'responsiblePersonEmail': ['exact'],
            'additionalContactPersonFullname': ['exact'],
            'additionalContactPersonPosition': ['exact'],
            'additionalContactPersonPhoneNumber': ['exact'],
            'additionalContactPersonEmail': ['exact'],
            'taxId': ['lt', 'gt'],
        }


class ConsultantFilter(FilterSet):

    class Meta:
        model = Consultant
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'legalAddress': ['exact'],
            'responsiblePersonFullname': ['exact'],
            'responsiblePersonPosition': ['exact'],
            'responsiblePersonPhoneNumber': ['exact'],
            'responsiblePersonEmail': ['exact'],
            'additionalContactPersonFullname': ['exact'],
            'additionalContactPersonPosition': ['exact'],
            'additionalContactPersonPhoneNumber': ['exact'],
            'additionalContactPersonEmail': ['exact'],
            'taxId': ['lt', 'gt'],
        }


class BeneficiaryFounderFilter(FilterSet):

    class Meta:
        model = BeneficiaryFounder
        fields = {
            'name': ['exact', 'startswith', 'contains'],
            'type': ['exact'],
            'gender': ['lt', 'gt'],
            'percentage': ['exact'],
            'beneficiary': ['exact'],
            'taxId': ['lt', 'gt'],
        }
