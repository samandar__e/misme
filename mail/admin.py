# from django.contrib import admin
# from .models import Inboxes, InboxUsers, InboxMessage
#
#
# # Register your models here.
#
# @admin.register(Inboxes)
# class InboxesAdmin(admin.ModelAdmin):
#     list_display = ('name', 'created_time')
#     fields = ('name',)
#     search_fields = ('name',)
#
#
# @admin.register(InboxUsers)
# class InboxUsersAdmin(admin.ModelAdmin):
#     list_display = ('inbox_id', 'user_id', 'starred', 'important', 'trashed', 'created_time')
#     fields = ('inbox_id', 'user_id', 'starred', 'important', 'trashed')
#     search_fields = ('inbox_id', 'user_id', 'starred', 'important', 'trashed')
#
#
# @admin.register(InboxMessage)
# class InboxMessageAdmin(admin.ModelAdmin):
#     list_display = ('inbox_id', 'from_id', 'to_id', 'subject', 'message', 'created_time', 'is_read')
#     fields = ('inbox_id', 'from_id', 'to_id', 'subject', 'message', 'files',)
#     search_fields = ('subject', 'message')