from django.db import models
from django.utils.translation import ugettext_lazy as _

from projects.models import Projects
from restapp.models import BaseModel, Attachment
from users.models import User


class EmailMessage(BaseModel):
    from_id = models.ForeignKey(User, related_name='sent_messages', null=True, on_delete=models.SET_NULL)
    to_id = models.ForeignKey(User, related_name='inbox_messages', null=True, on_delete=models.SET_NULL)
    subject = models.TextField(_('Message subject'))
    is_read = models.BooleanField(_('Is read?'), null=True, default=False)
    message = models.TextField(_('Message subject'))
    reply_id = models.ForeignKey('self', related_name='reply_messages', null=True, on_delete=models.SET_NULL)
    is_alert = models.BooleanField(_('Is alert?'), null=True)
    attachments = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        verbose_name = _('Email message')
        verbose_name_plural = _('Email messages')

    def __str__(self):
        return self.message


class UserMessageAttribute(BaseModel):
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    message = models.ForeignKey(EmailMessage, related_name='attributes', null=True, on_delete=models.SET_NULL)
    starred = models.BooleanField(_('Starred'), null=True, default=False)
    important = models.BooleanField(_('Important'), null=True, default=False)
    trashed = models.BooleanField(_('Trashed'), null=True, default=False)
    spam = models.BooleanField(_('Spam'), null=True, default=False)

    class Meta:
        verbose_name = _('Message attributes')
        verbose_name_plural = _('Message attributes list')

    def __str__(self):
        return self.user


class UserNotifications(BaseModel):
    STATUS = [
        ('New', 'New'),
        ('Approved', 'Approved'),
        ('Read', 'Read'),
    ]

    project = models.ForeignKey(Projects, related_name='notifications', on_delete=models.SET_NULL, blank=False, null=True)
    user_to = models.ForeignKey(User, related_name='user_to', null=True, on_delete=models.SET_NULL)
    user_from = models.ForeignKey(User, related_name='user_from', null=True, on_delete=models.SET_NULL)
    title = models.CharField(_('Title'), max_length=225, blank=True)
    description = models.TextField(_('Description'), max_length=225, blank=True)
    status = models.CharField(max_length=50, verbose_name=_('Status'), default='New', choices=STATUS, blank=True, null=True)

    class Meta:
        verbose_name = _('Notifications')
        verbose_name_plural = _('Notifications list')

    def __str__(self):
        return self.title
