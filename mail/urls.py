from django.urls import re_path, path

from .views import EmailMessageView, EmailMessageDetailView, EmailSentListView, EmailTrashedListView, \
    EmailStarredAddView, EmailImportantAddView, EmailTrashedAddView, UserListView, UserNotificationsView, \
    UserNotificationsDetailView

urlpatterns = [
    re_path(r'^mailbox$', EmailMessageView.as_view(), name='mailbox_post_list_view'),
    path('mailbox/detail/<int:pk>', EmailMessageDetailView.as_view(), name='mailbox_detail_view'),

    re_path(r'^mailbox/sent$', EmailSentListView.as_view(), name='mailbox_sent_list_view'),
    re_path(r'^mailbox/trashed$', EmailTrashedListView.as_view(), name='mailbox_trashed_view'),
    re_path(r'^mailbox/trashed-add$', EmailTrashedAddView.as_view(), name='mailbox_add_trashed_view'),
    re_path(r'^mailbox/user-list$', UserListView.as_view(), name='mailbox_user_list_view'),

    path('mailbox/starred/<int:pk>', EmailStarredAddView.as_view(), name='mailbox_starred_add_view'),
    path('mailbox/important/<int:pk>', EmailImportantAddView.as_view(), name='mailbox_important_add_view'),

    re_path(r'^notification/$', UserNotificationsView.as_view(), name='notification_view'),
    path('notification/<int:pk>', UserNotificationsDetailView.as_view(), name='notification_detail_view'),
    path('notification/user-list$>', UserListView.as_view(), name='notification_user_list_view'),
]
