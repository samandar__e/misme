from django_filters import rest_framework as filters

from mail.models import EmailMessage, UserNotifications


class EmailMessageFilter(filters.FilterSet):
    starred = filters.BooleanFilter(field_name="attributes", lookup_expr='starred')
    important = filters.BooleanFilter(field_name="attributes", lookup_expr='important')

    class Meta:
        model = EmailMessage
        fields = ['starred', 'important']


class UserNotificationsFilter(filters.FilterSet):

    class Meta:
        model = UserNotifications
        fields = {
            'title': ['exact'],
            'description': ['exact'],
        }
