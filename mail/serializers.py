from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from restapp.models import Attachment
from restapp.serializers import AttachmentSerializer
from .models import EmailMessage, UserMessageAttribute, UserNotifications
from users.models import User


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'avatar')


class ReplyMessageRelationSerializer(serializers.ModelSerializer):
    attachments = AttachmentSerializer(many=True)

    class Meta:
        model = EmailMessage
        fields = ('id', 'subject', 'is_read', 'message', 'is_alert', 'attachments')


class EmailMessageCreateSerializer(serializers.ModelSerializer):
    attachments = serializers.FileField(write_only=True)

    class Meta:
        model = EmailMessage
        fields = ('id', 'from_id', 'to_id', 'subject', 'is_read', 'message', 'reply_id', 'is_alert', 'attachments')

    def create(self, validated_data):
        file = validated_data.pop('attachments')
        message = EmailMessage.objects.create(**validated_data)
        if file:
            attachment = Attachment.objects.create(file_name=file.name, file=file)
            attachment.save()
            message.attachments.add(attachment)
            message.save()
        return message


class UserMessageAttributeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMessageAttribute
        fields = ('user', 'starred', 'important', 'trashed', 'spam')


class EmailMessageDetailSerializer(serializers.ModelSerializer):
    to_id = UserInfoSerializer()
    from_id = UserInfoSerializer()
    reply = SerializerMethodField(read_only=True)
    attributes = UserMessageAttributeSerializer(many=True, read_only=True)
    selected = serializers.BooleanField(default=False, read_only=True)
    userinfo = SerializerMethodField()
    attachments = AttachmentSerializer(many=True)

    class Meta:
        model = EmailMessage
        fields = ('id', 'from_id', 'to_id', 'subject', 'is_read', 'message',
                  'reply', 'is_alert', 'attachments', 'attributes', 'selected', 'created_time', 'userinfo')

    def get_reply(self, instance):
        reply = instance.reply_messages.order_by('id')
        return ReplyMessageRelationSerializer(reply, many=True).data

    def get_userinfo(self, instance):
        if instance.from_id == self.context['request'].user:
            return UserInfoSerializer(instance.to_id).data
        else:
            return UserInfoSerializer(instance.from_id).data


class MessageAttributeTrashedSerializer(serializers.ModelSerializer):
    trash_list = serializers.ListField()

    class Meta:
        model = UserMessageAttribute
        fields = ('trash_list',)


class UserMessageAttributeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserMessageAttribute
        fields = ('id', 'message', 'user', 'starred', 'important', 'trashed', 'spam')


class UserNotificationsSerializer(serializers.ModelSerializer):
    userinfo = UserInfoSerializer(source='user_from', read_only=True)
    created_time = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S", read_only=True)

    class Meta:
        model = UserNotifications
        fields = ('id', 'user_to', 'user_from', 'project', 'userinfo', 'title', 'description', 'status', 'created_time')

