from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework import status

from mail.filterset import EmailMessageFilter, UserNotificationsFilter
from mail.models import UserMessageAttribute, EmailMessage, UserNotifications
from mail.serializers import EmailMessageCreateSerializer, \
    EmailMessageDetailSerializer, UserMessageAttributeCreateSerializer, MessageAttributeTrashedSerializer, \
    UserInfoSerializer, UserNotificationsSerializer
from rest_framework.generics import ListCreateAPIView, get_object_or_404, ListAPIView, RetrieveAPIView, UpdateAPIView, \
    DestroyAPIView, RetrieveUpdateDestroyAPIView


from restapp.pagination import ResultsSetPagination
from restapp.utils.responses import nonContent
from users.models import User


class EmailMessageView(ListCreateAPIView):
    serializer_class = EmailMessageCreateSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = EmailMessageFilter
    ordering = ['pk']

    def get_serializer_context(self):
        context = super(EmailMessageView, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get(self, request, *args, **kwargs):
        self.serializer_class = EmailMessageDetailSerializer
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        return EmailMessage.objects.filter(to_id=self.request.user.id, reply_id=None).exclude(attributes__trashed=True)

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user, from_id=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class EmailMessageDetailView(RetrieveAPIView):
    serializer_class = EmailMessageDetailSerializer

    def get(self, request, pk):
        instance = get_object_or_404(EmailMessage, id=pk)
        EmailMessage.objects.filter(id=pk, to_id=request.user.id, is_read=False).update(is_read=True)
        serializer = self.serializer_class(instance, context={"request": self.request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class EmailSentListView(ListAPIView):
    serializer_class = EmailMessageDetailSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = EmailMessageFilter
    ordering = ['pk']

    def get_serializer_context(self):
        context = super(EmailSentListView, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        return EmailMessage.objects.filter(from_id=self.request.user.id, reply_id=None).exclude(
            attributes__trashed=True)


class EmailTrashedListView(ListAPIView):
    serializer_class = EmailMessageDetailSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = EmailMessageFilter
    pagination_class = ResultsSetPagination
    ordering = ['pk']

    def get_serializer_context(self):
        context = super(EmailTrashedListView, self).get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        return EmailMessage.objects.filter(attributes__user=self.request.user.id, attributes__trashed=True)


class EmailTrashedAddView(DestroyAPIView):

    def delete(self, request, *args, **kwargs):
        for item in request.data['trash_list']:
            instance, created = UserMessageAttribute.objects.get_or_create(
                message_id=item, user_id=request.user.id,
                defaults={"message_id": item, "user_id": request.user.id,
                          "trashed": True})
            if not created:
                UserMessageAttribute.objects.filter(pk=instance.pk).update(trashed=True)
        return Response(nonContent(), status=status.HTTP_204_NO_CONTENT)


class EmailStarredAddView(UpdateAPIView):
    serializer_class = UserMessageAttributeCreateSerializer
    http_method_names = ['put']

    def put(self, request, pk):
        instance, created = UserMessageAttribute.objects.get_or_create(
            message_id=pk, user_id=request.user.id,
            defaults={"message_id": pk, "user_id": request.user.id, "starred": False})

        UserMessageAttribute.objects.filter(pk=instance.pk).update(starred=not instance.starred)

        return Response(status=status.HTTP_202_ACCEPTED)


class EmailImportantAddView(UpdateAPIView):
    serializer_class = EmailMessageDetailSerializer
    http_method_names = ['put']

    def put(self, request, pk):
        instance, created = UserMessageAttribute.objects.get_or_create(
            message_id=pk, user_id=request.user.id,
            defaults={"message_id": pk, "user_id": request.user.id, "important": False})
        UserMessageAttribute.objects.filter(pk=instance.pk).update(important=not instance.important)

        return Response(status=status.HTTP_202_ACCEPTED)


class UserListView(ListAPIView):
    serializer_class = UserInfoSerializer
    pagination_class = ResultsSetPagination

    def get_queryset(self):
        return User.objects.filter(~Q(id=self.request.user.id)).all()


class UserNotificationsView(ListCreateAPIView):
    serializer_class = UserNotificationsSerializer
    pagination_class = ResultsSetPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = UserNotificationsFilter
    ordering = ['-pk']

    # def get_queryset(self):
    #     return UserNotifications.objects.all()

    def get_queryset(self):
        return UserNotifications.objects.filter(user_to=self.request.user.id).order_by('-created_time')

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status.HTTP_201_CREATED)


class UserNotificationsDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = UserNotificationsSerializer

    def get(self, request, pk):
        instance = get_object_or_404(UserNotifications, id=pk)
        serializer = self.serializer_class(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        instance = get_object_or_404(UserNotifications, id=pk)
        serializer = self.serializer_class(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(updated_by=self.request.user)
        return Response(serializer.data, status.HTTP_202_ACCEPTED)

    def delete(self, request, pk):
        instance = get_object_or_404(UserNotifications, id=pk)
        instance.delete()
        return Response(nonContent(), status.HTTP_204_NO_CONTENT)
