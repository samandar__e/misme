from rest_framework import serializers

from components.models import Component, ProcurementPlan, Contracts, Trainings
from projects.models import Projects
from subproject.models import Subproject, Visit
from crm.models import InternationalFinancialInstitute, Consultant, ParticipatingFinancialInstitution, Beneficiary
from directory.serializers import RelatedRegionSerializer
from subproject.serializers import TotalCostField, LoanAmountField


class ProjectsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Projects
        fields = ('id', 'name', 'created_time')


class ComponentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Component
        fields = ('id', 'name', 'created_time')


class PFISerializer(serializers.ModelSerializer):

    class Meta:
        model = ParticipatingFinancialInstitution
        fields = ('id', 'name', 'created_time')


class ProjectDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'name')


class SubprojectSerializer(serializers.ModelSerializer):
    total_cost = TotalCostField(source='*', required=False)
    loan_amount = LoanAmountField(source='*', required=False)
    region_detail = RelatedRegionSerializer(source='region', read_only=True)
    pfi_detail = PFISerializer(source='pfi', read_only=True)
    project_detail = ProjectDetailSerializer(source='project', read_only=True)

    class Meta:
        model = Subproject
        fields = ('id', 'name', 'loan_amount', 'total_cost', 'project', 'project_detail', 'pfi', 'pfi_detail', 'region',
                  'region_detail', 'created_time')


class ProcurementPlanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProcurementPlan
        fields = ('id', 'created_time')


class ContractsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contracts
        fields = ('id', 'created_time')


class TrainingsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trainings
        fields = ('id', 'created_time')


class VisitSerializer(serializers.ModelSerializer):

    class Meta:
        model = Visit
        fields = ('id', 'created_time')


class IFISerializer(serializers.ModelSerializer):

    class Meta:
        model = InternationalFinancialInstitute
        fields = ('id', 'created_time')


class BeneficiarySerializer(serializers.ModelSerializer):

    class Meta:
        model = Beneficiary
        fields = ('id', 'created_time')


class ConsultantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Consultant
        fields = ('id', 'created_time')
