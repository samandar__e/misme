from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json

from subproject.models import Subproject
from statistic.serializers import SubprojectSerializer


class ComparativeChartsList(ListAPIView):
    http_method_names = ['get']

    def get(self, request, include_self=True):
        date_from = request.GET['dateFrom']
        date_to = request.GET['dateTo']

        # Subproject count
        queryset_subproject = Subproject.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer = SubprojectSerializer(queryset_subproject, many=True)
        subproject_dict = json.loads(json.dumps(serializer.data))

        return Response(subproject_dict, status=status.HTTP_200_OK)
