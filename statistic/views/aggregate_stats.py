from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.utils import json
from datetime import timedelta


from components.models import Component, ProcurementPlan, Contracts, Trainings
from projects.models import Projects
from subproject.models import Subproject, Visit
from crm.models import InternationalFinancialInstitute, Consultant, ParticipatingFinancialInstitution, Beneficiary
from statistic.serializers import ProjectsSerializer, ComponentSerializer, SubprojectSerializer, \
    ProcurementPlanSerializer, ContractsSerializer, TrainingsSerializer, VisitSerializer, IFISerializer, PFISerializer, \
    BeneficiarySerializer, ConsultantSerializer


class AggregateStatsList(ListAPIView):
    http_method_names = ['get']

    def get(self, request, include_self=True):
        date_from = request.GET['dateFrom']
        date_to = request.GET['dateTo']
        
        # Projects count
        queryset_projects = Projects.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_projects = ProjectsSerializer(queryset_projects, many=True)
        projects_dict = json.loads(json.dumps(serializer_projects.data))

        # Component count
        queryset_component = Component.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_component = ComponentSerializer(queryset_component, many=True)
        component_dict = json.loads(json.dumps(serializer_component.data))

        # Subproject count
        queryset_subproject = Subproject.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer = SubprojectSerializer(queryset_subproject, many=True)
        subproject_dict = json.loads(json.dumps(serializer.data))

        # Procurement packages count
        queryset_procurement = ProcurementPlan.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_procurement = ProcurementPlanSerializer(queryset_procurement, many=True)
        procurement_dict = json.loads(json.dumps(serializer_procurement.data))

        # Contracts count
        queryset_contracts = Contracts.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_contracts = ContractsSerializer(queryset_contracts, many=True)
        contracts_dict = json.loads(json.dumps(serializer_contracts.data))

        # Trainings count
        queryset_trainings = Trainings.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_trainings = TrainingsSerializer(queryset_trainings, many=True)
        trainings_dict = json.loads(json.dumps(serializer_trainings.data))

        # Visit packages count
        queryset_visit = Visit.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_visit = VisitSerializer(queryset_visit, many=True)
        visit_dict = json.loads(json.dumps(serializer_visit.data))

        # IFIs count
        queryset_ifi = InternationalFinancialInstitute.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_ifi = IFISerializer(queryset_ifi, many=True)
        ifi_dict = json.loads(json.dumps(serializer_ifi.data))

        # PFIs count
        queryset_pfi = ParticipatingFinancialInstitution.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_pfi = PFISerializer(queryset_pfi, many=True)
        pfi_dict = json.loads(json.dumps(serializer_pfi.data))

        # Beneficiaries count
        queryset_beneficiary = Beneficiary.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_beneficiary = BeneficiarySerializer(queryset_beneficiary, many=True)
        beneficiary_dict = json.loads(json.dumps(serializer_beneficiary.data))

        # Consultants count
        queryset_consultant = Consultant.objects.filter(created_time__gte=date_from, created_time__lte=date_to)
        serializer_consultant = ConsultantSerializer(queryset_consultant, many=True)
        consultant_dict = json.loads(json.dumps(serializer_consultant.data))

        return Response({
            'projects_count': len(projects_dict),
            'component_count': len(component_dict),
            'subproject_count': len(subproject_dict),
            'procurement_count': len(procurement_dict),
            'contracts_count': len(contracts_dict),
            'training_count': len(trainings_dict),
            'visit_count': len(visit_dict),
            'ifi_count': len(ifi_dict),
            'pfi_count': len(pfi_dict),
            'beneficiary_count': len(beneficiary_dict),
            'consultant_count': len(consultant_dict),
        }, status=status.HTTP_200_OK)
