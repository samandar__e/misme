from django.urls import re_path
from statistic.views.aggregate_stats import AggregateStatsList
from statistic.views.comparative_charts import ComparativeChartsList

urlpatterns = [
    re_path(r'^aggregate-stats/$', AggregateStatsList.as_view(), name='aggregate_stats_view'),
    re_path(r'^comparative-charts/$', ComparativeChartsList.as_view(), name='comparative_charts_view'),
]